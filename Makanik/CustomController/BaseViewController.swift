//
//  BaseViewController.swift
//  HelpHandStore
//
//  Created by ahmed on 3/2/20.
//  Copyright © 2020 ahmed. All rights reserved.
//

import UIKit
import MXSegmentedPager

class BaseViewController:MXSegmentedPagerController,UIGestureRecognizerDelegate {
    
    var emptyView:EmptyView?
    
    //MXSegmentedPagerController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // enable swipe to pop
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        // swipe to Show side menu
        //        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        //        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        //        self.view.addGestureRecognizer(swipeRight)
        
    }
    
    //    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer){
    //        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
    //            switch swipeGesture.direction{
    //            case UISwipeGestureRecognizer.Direction.right:
    //                //write your logic for right swipe
    //                print("Swiped right")
    //                SlideMune.showSideMenuVC(self)
    //
    //            case UISwipeGestureRecognizer.Direction.left:
    //                //write your logic for left swipe
    //                print("Swiped left")
    //
    //            default:
    //                break
    //            }
    //        }
    //    }
    
    
    @IBAction func ShowMenuAction(_sender :UIViewController) {
        return
        
    }
    
    func didClickProfileButton(_sender :UIBarButtonItem) {
    }
    
    func didClickMenuButton(_sender :UIBarButtonItem) {
        print("sideMune")
        // SlideMune.showSideMenuVC(self)
    }
    
    @objc func didClickRightButton(_sender :UIBarButtonItem) {
    }
    
    
    
    func backButtonAction(_sender :UIBarButtonItem) {
        let isFromNotfication = UserDefaults.standard.bool(forKey: "isNotification")
        if isFromNotfication{
            let vc:TTabBarViewController = TTabBarViewController.loadFromNib()
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            UserDefaults.standard.set(false, forKey: "isNotification")
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func backButtonActionWithdismiss(_sender :UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func backRootButtonAction(_sender :UIBarButtonItem) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    func backButtonActionToRoot(_sender :UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    
    
    
}
