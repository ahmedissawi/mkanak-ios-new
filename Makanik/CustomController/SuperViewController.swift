//
//  SuperViewController.swift
//  Project
//  edit from git
//  Created by ahmed on 6/10/18.
//  Copyright © 2018 ahmed. All rights reserved.
//

import UIKit

class SuperViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    var emptyView:EmptyView?
    var emptyView2:EmptyView?

    var network = NetworkManager.sharedInstance
    var reachability = try! Reachability()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didClickProfileButton(_sender :UIBarButtonItem) {
    }
    
    func didClickRightButton(_sender :UIBarButtonItem) {
    }
    
    func backButtonAction(_sender :UIBarButtonItem) {
        let isFromNotfication = UserDefaults.standard.bool(forKey: "isNotification")
        if isFromNotfication{
            let vc:TTabBarViewController = TTabBarViewController.loadFromNib()
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            UserDefaults.standard.set(false, forKey: "isNotification")
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func didClickMenuButton(_sender :UIBarButtonItem) {
        // SlideMune.showSideMenuVC(self)
    }
    func backButtonActionWithdismiss(_sender :UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func backButtonActionToRoot(_sender :UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func backRootButtonAction(_sender :UIBarButtonItem) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    
    
} 
