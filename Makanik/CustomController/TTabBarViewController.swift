//
//  TTabBarViewController.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 11/10/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class TTabBarViewController: UITabBarController {
    
    /// check hight  tabbar device
    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStyleTabBar()
        setTabBarVCs()
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -10)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let newTabBarHeight = defaultTabBarHeight + 10.0
        
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newFrame
//        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal:0, vertical:-10 )
//        self.tabBar.frame = self.tabBar.frame.insetBy(dx: 0,dy: 3);

    }
    
    
    
    func setTabBarVCs(){
        
        
        let homeuserVC = HomeUserVC().navigationController()
        homeuserVC.tabBarItem = UITabBarItem(title: "Home".localized, image: UIImage(named: "ic_home"), tag: 0)
        homeuserVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        let homeproviderVC = HomeProviderVC().navigationController()
        homeproviderVC.tabBarItem = UITabBarItem(title: "Home".localized, image: UIImage(named: "ic_home"), tag: 0)
        homeproviderVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        
//        let servicesuserVC = ServicesUserVC().navigationController()
//        servicesuserVC.tabBarItem = UITabBarItem(title: "Categories".localized, image: UIImage(named: "ic_services"), tag: 1)
//        servicesuserVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        
        let ordersVC = OrdersVC().navigationController()
        ordersVC.tabBarItem = UITabBarItem(title: "Orders".localized, image: UIImage(named: "ic_orders"), tag: 2)
        ordersVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        
        let notificationVC = NotificationVC().navigationController()
        notificationVC.tabBarItem = UITabBarItem(title: "Notification".localized, image: UIImage(named: "ic_notification"), tag: 3)
        notificationVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        
        let moreVC = MoreVC().navigationController()
        moreVC.tabBarItem = UITabBarItem(title: "More".localized, image: UIImage(named: "ic_more"), tag: 4)
        moreVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        
        ///For Provider
        let navVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        let accountProviderVC:ShowProviderDeatilesVC = AppDelegate.sb_main.instanceVC()
        navVC.viewControllers = [accountProviderVC]
        navVC.modalPresentationStyle = .fullScreen
        accountProviderVC.tabBarItem = UITabBarItem(title: "Account".localized, image: UIImage(named: "ic_account"), tag: 5)
        accountProviderVC.tabBarItem.imageInsets = UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0)
        
        if CurrentUser.typeSelect == "client"{
            self.viewControllers = [homeuserVC,ordersVC,notificationVC,moreVC]
        }else if CurrentUser.typeSelect == "provider"{
            self.viewControllers = [homeproviderVC,ordersVC,notificationVC,navVC,moreVC]
        }
        
        if CurrentUser.userInfo == nil,CurrentProvider.providerInfo == nil{
            self.viewControllers = [homeuserVC,moreVC]
        }
        
    }
    
    
    func setStyleTabBar(){
        
        
        self.tabBar.tintColor = "586AF6".color
        
        let appearance = UITabBarItem.appearance(whenContainedInInstancesOf: [TTabBarViewController.self])
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:"151B44".color,NSAttributedString.Key.font:UIFont.NeoSansArabicMedium(ofSize: 12)], for: .normal)
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:"586AF6".color,NSAttributedString.Key.font:UIFont.NeoSansArabicMedium(ofSize: 12)], for: .selected)
        tabBar.backgroundImage = UIImage(color: "FFFFFF".color)
        tabBar.unselectedItemTintColor = "151B44".color
        tabBar.barTintColor = UIColor.white
        tabBar.isTranslucent = false
        
        
        if #available(iOS 13, *) {
            let appearance = UITabBarAppearance()
            
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: "151B44".color,NSAttributedString.Key.font : UIFont.NeoSansArabicMedium(ofSize: 12)]
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: "586AF6".color,NSAttributedString.Key.font : UIFont.NeoSansArabicMedium(ofSize: 12)]
            appearance.backgroundColor = UIColor.white
            appearance.stackedLayoutAppearance.normal.iconColor = "151B44".color
            tabBar.isTranslucent = false
            tabBar.standardAppearance = appearance
        }
    }
    
    
    
}
