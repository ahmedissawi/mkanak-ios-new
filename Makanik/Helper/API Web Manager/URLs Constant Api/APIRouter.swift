//
//  APIRouter.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 11/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

enum APIRouter {
    
    #if DEBUG
    static let networkEnviroment: NetworkEnvironment = .dev
    #else
    static let networkEnviroment: NetworkEnvironment = .production
    #endif
    
    //MARK: - APIRouter URL -
    public static var DOMAIN_URL :String{
        switch APIRouter.networkEnviroment {
        case .dev:
            return "https://new.mkanak.com.sa/api/v1/"
        case .production:
            return "https://new.mkanak.com.sa/api/v1/"
        case .stage:
            return "https://new.mkanak.com.sa/api/v1/"
            
        }
    }
    
    case loginUser
    case loginProvider
    case checkUser
    case checkProvider
    case registerProvider
    case registerUser
    case resetpasswordUser
    case resetpasswordProvider
    case homeProvider
    case orderProvider
    case orderUser
    case showorderUser
    case showorderProvider
    case rateUser
    case rateMechanic
    case acceptUser
    case rejectedUser
    case contactus
    case reasons
    case supportUser
    case supportProvider
    case updateProfileUser
    case updateProfileProvider
    case mechanicprofile
    case userwallet
    case providerwallet
    case depositwalletUser
    case depositwalletProvider
    case WithdrawalwalletProvider
    case updatepasswordUser
    case updatepasswordProvider
    case homeUser
    case subCategoryUser
    case categoryUser
    case settings
    case logoutUser
    case logoutProvider
    case showmyProvider
    case showservicesProvider
    case addservicesProvider
    case editeservicesProvider
    case createmechanicbank
    case banks
    case addBanks
    case getbanksDeatiles
    case editebanks
    case deletebank
    case mechanicservicesByType
    case shwoservicesProvider
    case deleteservices
    case makeOrder
    case addworkDays
    case editeworkDays
    case deleteworkDays
    case deatilesworkDays
    case subCategories
    case categories
    case days
    case requestSpeacialService
    case getnotificationsUser
    case getnotificationsProvider
    case updatenotificationsProvider
    case updatenotificationsUser
    case makeAcceptOrder
    case makeComplateOrder
    case makeRejectedOrder
    case makeAddofferOrder
    case makeediteofferOrder
    case pages



    public var values : (url: String ,reqeustType: HTTPMethod,key :String?){
        get{
            switch self {
            case .loginUser:
                return (APIRouter.DOMAIN_URL + "user/login",.post,nil)
            case .loginProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/login",.post,nil)
            case .checkUser:
                return (APIRouter.DOMAIN_URL + "user/check_mobile",.post,nil)
            case .checkProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/check_mobile",.post,nil)
            case .registerUser:
                return (APIRouter.DOMAIN_URL + "user/register",.post,nil)
            case .registerProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/register",.post,nil)
            case .resetpasswordUser:
                return (APIRouter.DOMAIN_URL + "user/rest_password",.post,nil)
            case .resetpasswordProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/rest_password",.post,nil)
            case .homeProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/home",.get,nil)
            case .orderProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/orders",.get,nil)
            case .orderUser:
                return (APIRouter.DOMAIN_URL + "user/orders",.get,nil)
            case .showorderUser:
                return (APIRouter.DOMAIN_URL + "user/order/",.get,"nested")
            case .rateUser:
                return (APIRouter.DOMAIN_URL + "user/rating",.post,nil)
            case .rateMechanic:
                return (APIRouter.DOMAIN_URL + "mechanic/rating",.post,nil)
            case .acceptUser:
                return (APIRouter.DOMAIN_URL + "user/offer/",.post,nil)
            case .rejectedUser:
                return (APIRouter.DOMAIN_URL + "user/offer/",.post,nil)
            case .showorderProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/order/",.get,"nested")
            case .addservicesProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/service",.post,nil)
            case .editeservicesProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/service/",.patch,nil)
            case .deleteservices:
                return (APIRouter.DOMAIN_URL + "mechanic/service/",.delete,"nested")
            case .shwoservicesProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/service/",.get,"nsested")
            case .contactus:
                return (APIRouter.DOMAIN_URL + "contact_us",.post,nil)
            case .reasons:
                return (APIRouter.DOMAIN_URL + "reasons",.get,nil)
            case .supportUser:
                return (APIRouter.DOMAIN_URL + "user/support",.post,nil)
            case .supportProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/support",.post,nil)
            case .updateProfileUser:
                return (APIRouter.DOMAIN_URL + "user/profile",.post,nil)
            case .updateProfileProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/profile",.post,nil)
            case .mechanicprofile:
                return (APIRouter.DOMAIN_URL + "mechanic_profile/",.get,"nested")
            case .userwallet:
                return (APIRouter.DOMAIN_URL + "user/wallet",.get,nil)
            case .providerwallet:
                return (APIRouter.DOMAIN_URL + "mechanic/wallet",.get,nil)
            case .depositwalletUser:
                return (APIRouter.DOMAIN_URL + "user/add_balance",.post,nil)
            case .depositwalletProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/add_balance",.post,nil)
            case .WithdrawalwalletProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/withdrawal_balance",.post,nil)
            case .updatepasswordUser:
                return (APIRouter.DOMAIN_URL + "user/update_password",.post,nil)
            case .updatepasswordProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/update_password",.post,nil)
            case .homeUser:
                return (APIRouter.DOMAIN_URL + "home",.get,nil)
            case .subCategoryUser:
                return (APIRouter.DOMAIN_URL + "services/",.get,"nested")
            case .categoryUser:
                return (APIRouter.DOMAIN_URL + "category_search",.get,"nested")
            case .settings:
                return (APIRouter.DOMAIN_URL + "settings",.get,nil)
            case .logoutUser:
                return (APIRouter.DOMAIN_URL + "user/logout",.post,nil)
            case .logoutProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/logout",.post,nil)
            case .showmyProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/profile",.get,nil)
            case .showservicesProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/service",.get,nil)
            case .createmechanicbank:
                return (APIRouter.DOMAIN_URL + "mechanic/mechanic_bank",.post,nil)
            case .banks:
                return (APIRouter.DOMAIN_URL + "banks",.get,nil)
            case .addBanks:
                return (APIRouter.DOMAIN_URL + "mechanic/mechanic_bank",.post,nil)
            case .getbanksDeatiles:
                return (APIRouter.DOMAIN_URL + "mechanic/mechanic_bank/",.get,"nested")
            case .editebanks:
                return (APIRouter.DOMAIN_URL + "mechanic/mechanic_bank/",.patch,"nested")
            case .deletebank:
                return (APIRouter.DOMAIN_URL + "mechanic/mechanic_bank/",.delete,"nested")
            case .mechanicservicesByType:
                return (APIRouter.DOMAIN_URL + "mechanic_services",.get,"nested")
            case .makeOrder:
                return (APIRouter.DOMAIN_URL + "user/order",.post,nil)
            case .addworkDays:
                return (APIRouter.DOMAIN_URL + "mechanic/work_day",.post,nil)
            case .categories:
                return (APIRouter.DOMAIN_URL + "categories",.get,nil)
            case .subCategories:
                return (APIRouter.DOMAIN_URL + "categories/",.get,"nested")
            case .days:
                return (APIRouter.DOMAIN_URL + "days",.get,nil)
            case .editeworkDays:
                return (APIRouter.DOMAIN_URL + "mechanic/work_day/",.patch,"nested")
            case .deatilesworkDays:
                return (APIRouter.DOMAIN_URL + "mechanic/work_day/",.get,"nested")
            case .deleteworkDays:
                return (APIRouter.DOMAIN_URL + "mechanic/work_day/",.delete,"nested")
            case .requestSpeacialService:
                return (APIRouter.DOMAIN_URL + "mechanic/service_distinction",.post,nil)
            case .getnotificationsUser:
                return (APIRouter.DOMAIN_URL + "user/notifications",.get,nil)
            case .getnotificationsProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/notifications",.get,nil)
            case .updatenotificationsProvider:
                return (APIRouter.DOMAIN_URL + "mechanic/update_notification",.post,nil)
            case .updatenotificationsUser:
                return (APIRouter.DOMAIN_URL + "user/update_notification",.post,nil)
                
            case .makeAcceptOrder:
                return (APIRouter.DOMAIN_URL + "mechanic/order/",.post,nil)
            
            case .makeComplateOrder:
                return (APIRouter.DOMAIN_URL + "mechanic/order/",.post,nil)

            case .makeRejectedOrder:
                return (APIRouter.DOMAIN_URL + "mechanic/order/",.post,nil)
                
            case .makeAddofferOrder:
                return (APIRouter.DOMAIN_URL + "mechanic/order_offer",.post,nil)

            case .makeediteofferOrder:
                return (APIRouter.DOMAIN_URL + "mechanic/order/update",.post,nil)

            case .pages:
                return (APIRouter.DOMAIN_URL + "page/",.get,"nested")

                
            }
        }
        
    }
}
