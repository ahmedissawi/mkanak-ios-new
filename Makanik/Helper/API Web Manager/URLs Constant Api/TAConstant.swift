//
//  TAConstant.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 11/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

struct StatusStruct : Codable{
    let status: Int?
    let code: Int?
    let success: Bool?
    let message: String?
}

enum DataType: String {
    case json
    case serialize
}
enum NetworkEnvironment{
    case dev
    case production
    case stage
}
