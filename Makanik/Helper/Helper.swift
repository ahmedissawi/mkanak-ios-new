//
//  Helper.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 11/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import IQKeyboardManagerSwift


class Helper: NSObject {
    
    class func EnableKeyboardManager(TintColor:UIColor,BarTintColor:UIColor,placeholderColor:UIColor){
        IQKeyboardManager.shared.enable = true
        /// label Color
        IQKeyboardManager.shared.toolbarTintColor = TintColor
        /// BackGround Bar
        IQKeyboardManager.shared.toolbarBarTintColor = BarTintColor
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = false
        IQKeyboardManager.shared.placeholderColor = placeholderColor
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.placeholderFont = UIFont.NeoSansArabicMedium(ofSize: 15)
        
    }
    
    class func routeToLogin(){
        CurrentUser.userInfo = nil
        CurrentProvider.providerInfo = nil
        let mainVC = LoginVC.loadFromNib()
        UIApplication.shared.keyWindow?.rootViewController = mainVC
        UIApplication.shared.keyWindow?.makeKeyAndVisible()

    }
    
    /// Will quit the application with animation
    class func quit() {
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        //Comment if you want to minimise app
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            exit(0)
        }
    }
    
    class func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    class func transformURLString(_ string: String) -> URLComponents? {
        guard let urlPath = string.components(separatedBy: "?").first else {
            return nil
        }
        var components = URLComponents(string: urlPath)
        if let queryString = string.components(separatedBy: "?").last {
            components?.queryItems = []
            let queryItems = queryString.components(separatedBy: "&")
            for queryItem in queryItems {
                guard let itemName = queryItem.components(separatedBy: "=").first,
                    let itemValue = queryItem.components(separatedBy: "=").last else {
                        continue
                }
                components?.queryItems?.append(URLQueryItem(name: itemName, value: itemValue))
            }
        }
        return components!
    }
    
    
}
