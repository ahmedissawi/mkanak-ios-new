//
//  FCM.swift
//
//
//  Created by  Ahmed’s MacBook Pro on 10/23/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import Firebase
import CoreFoundation
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications

class FCM: NSObject,MessagingDelegate {
    

    init(withApplication:UIApplication) {
        super.init()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { granted, error in
                    if let error = error {
                        print("D'oh: \(error.localizedDescription)")
                    } else {
                        //DispatchQueue.main.async {
                        //    withApplication.registerForRemoteNotifications()
                        //}
                    }})
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self


        } else {

            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            withApplication.registerUserNotificationSettings(settings)

        }

        withApplication.registerForRemoteNotifications()
        
        FCM.handelTopicNotification()

    }
    
  class func handelTopicNotification(){
        
        if CurrentUser.typeSelect == "client"{
            // For client user
            if CurrentUser.userInfo?.notification == 1{
                Messaging.messaging().subscribe(toTopic: "users")
                Messaging.messaging().subscribe(toTopic: "user_\(CurrentUser.userInfo?.id ?? 0)")
            }else{
                Messaging.messaging().unsubscribe(fromTopic: "users")
                Messaging.messaging().unsubscribe(fromTopic: "user_\(CurrentUser.userInfo?.id ?? 0)")
            }
        }else if CurrentUser.typeSelect == "provider"{
            // For provider user
            if CurrentProvider.providerInfo?.notification == 1{
                Messaging.messaging().subscribe(toTopic: "mechanics")
                Messaging.messaging().subscribe(toTopic: "mechanic_\(CurrentProvider.providerInfo?.id ?? 0)")
            }else{
                Messaging.messaging().unsubscribe(fromTopic: "mechanics")
                Messaging.messaging().unsubscribe(fromTopic: "mechanic_\(CurrentProvider.providerInfo?.id ?? 0)")
            }
        }
        // For public user
        Messaging.messaging().subscribe(toTopic: "mkanak")
        
    }



}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension FCM : UNUserNotificationCenterDelegate {

    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        var category = ""
        var orderid = ""
        let state = UIApplication.shared.applicationState
        if state == .active {
        if let aps = userInfo["aps"] as? NSDictionary {
            if let titleBody = aps["category"] as? NSString {
                category = titleBody as String
                print("category\(category)")
                
                if category == "withdraw_wallet" || category == "deposit_wallet"{
                    let wallet:WalletVC = WalletVC.loadFromNib()
                    let navigationController = CustomNavigationBar(rootViewController: wallet)
                    wallet.hidesBottomBarWhenPushed = true
                    navigationController.viewControllers = [wallet]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }
                
            }
        }
        
        if let orderID = userInfo["order_id"] as? NSString {
            orderid = orderID as String
            //check if category from order
            if category == "completed_order" || category == "accept_offer" || category == "accept_order" || category == "new_order" || category == "reject_offer" || category == "reject_order" || category == "send_offer"{
                if CurrentUser.typeSelect == "client"{
                    let orderDeatiles:UserDeatilesVC = UserDeatilesVC.loadFromNib()
                    let navigationController = CustomNavigationBar(rootViewController: orderDeatiles)
                    orderDeatiles.hidesBottomBarWhenPushed = true
                    orderDeatiles.id = Int.init(orderid ) ?? 0
                    navigationController.viewControllers = [orderDeatiles]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }else{
                    let orderDeatiles:ProviderDeatilesVC = ProviderDeatilesVC.loadFromNib()
                    let navigationController = CustomNavigationBar(rootViewController: orderDeatiles)
                    orderDeatiles.hidesBottomBarWhenPushed = true
                    orderDeatiles.id = Int.init(orderid ) ?? 0
                    navigationController.viewControllers = [orderDeatiles]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }

            }
        }
            print(userInfo)
            print(notification)
            completionHandler([.badge, .sound,.alert])
    
        }
        


    }

    //When tap  on the notification
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        var category = ""
        var orderid = ""

        if let aps = userInfo["aps"] as? NSDictionary {
            if let titleBody = aps["category"] as? NSString {
                category = titleBody as String
                print("category\(category)")
                
                if category == "withdraw_wallet" || category == "deposit_wallet"{
                    let wallet:WalletVC = WalletVC.loadFromNib()
                    let navigationController = CustomNavigationBar(rootViewController: wallet)
                    wallet.hidesBottomBarWhenPushed = true
                    navigationController.viewControllers = [wallet]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }
                
            }
        }
        
        if let orderID = userInfo["order_id"] as? NSString {
            orderid = orderID as String
            //check if category from order
            if category == "completed_order" || category == "accept_offer" || category == "accept_order" || category == "new_order" || category == "reject_offer" || category == "reject_order" || category == "send_offer"{
                if CurrentUser.typeSelect == "client"{
                    let orderDeatiles:UserDeatilesVC = UserDeatilesVC.loadFromNib()
                    let navigationController = CustomNavigationBar(rootViewController: orderDeatiles)
                    orderDeatiles.hidesBottomBarWhenPushed = true
                    orderDeatiles.id = Int.init(orderid ) ?? 0
                    navigationController.viewControllers = [orderDeatiles]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }else{
                    let orderDeatiles:ProviderDeatilesVC = ProviderDeatilesVC.loadFromNib()
                    let navigationController = CustomNavigationBar(rootViewController: orderDeatiles)
                    orderDeatiles.hidesBottomBarWhenPushed = true
                    orderDeatiles.id = Int.init(orderid ) ?? 0
                    navigationController.viewControllers = [orderDeatiles]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }
                
            }
        }
        
        
        print(userInfo)
        completionHandler()


    }

}

extension AppDelegate{

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //Messaging.messaging().shouldEstablishDirectChannel = true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //AppDelegate.fcm.connectToFcm()

    }


    //    func applicationWillTerminate(_ application: UIApplication) {
    //
    //        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.


    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // With swizzling disabled you must set the APNs token here.


        FCM.handelTopicNotification()
        
        Messaging.messaging().apnsToken = deviceToken
        
        Messaging.messaging()
            .setAPNSToken(deviceToken as Data, type: MessagingAPNSTokenType.unknown)

//        #if DEBUG
//        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
//        #else
//        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
//        #endif

    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        FCM.handelTopicNotification()
    }

    // [START receive_message] When tap  on the notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.

        //AppDelegate.fcm.recievedRemoteNotification(userInfo: userInfo)

    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("userinfo in background mode on-\(userInfo)")
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        //AppDelegate.fcm.recievedRemoteNotification(userInfo: userInfo)

        if application.applicationState == UIApplication.State.active {

            print("App already open")
        } else {
            print ("App opened from Notification")
        }
        print(userInfo)

        Messaging.messaging().appDidReceiveMessage(userInfo)


        completionHandler(UIBackgroundFetchResult.newData)
    }


}
