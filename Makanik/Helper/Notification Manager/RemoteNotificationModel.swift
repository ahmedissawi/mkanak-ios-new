//
//  RemoteNotificationModel.swift
//  Mshwar
//
//  Created by Omar Al tawashi on 10/28/19.
//  Copyright © 2019 Omar Al tawashi. All rights reserved.
//

import Foundation


class RemoteNotificationModel: NSObject {
    
    var body:String!
    var title:String!
    var message:String!
    var sound:String!
    var date:String!
    var type:String?
    var image:String?
    
    var payload:[AnyHashable: Any]?
    
    
    init(withUserInfo: [AnyHashable: Any]){
        super.init()
        self.payload = withUserInfo
        let notification = withUserInfo["notification"] as? [AnyHashable: Any]
        body        = withUserInfo["body"] as? String
        message     = withUserInfo["message"] as? String
        title       = withUserInfo["title"] as? String
        type        = withUserInfo["notification_type"] as? String
        image       = withUserInfo["image"] as? String
        //print("gcm.notification.typegcm.notification.type")
        if let aps = withUserInfo["aps"] as? [AnyHashable: Any] ,let alert = aps["alert"] as? [AnyHashable: Any] {
            body        = alert["body"] as? String
            title       = alert["title"] as? String
        }
        //print(self.type)
        sound       = notification?["sound2"] as? String
        date        = withUserInfo["date"] as? String ?? ""
        
    }
    
    override init() {
        super.init()
    }
    
    
}
