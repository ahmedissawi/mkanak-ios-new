//
//  NotifactionRouteViewModel.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 10/23/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation
import UIKit


enum NotificationType:String {
    
    case adminNotification = "admin_notification"
    case nearbyOrder = "nearby_order"
    case newOffer = "new_offer"

}


