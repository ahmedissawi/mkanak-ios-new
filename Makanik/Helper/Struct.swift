//
//  Struct.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 11/10/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//
import Foundation
import UIKit


struct CurrentUser  {

    static var userInfo : UserData?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentUser");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentUser")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentUser") as? Data {
                return try? PropertyListDecoder().decode(UserData.self, from:data)

            }
            return nil
        }

    }
    static var typeSelect : String?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "typeSelect");
                return  ;
            }
            UserDefaults.standard.set(newValue, forKey: "typeSelect") //String
            UserDefaults.standard.synchronize();
        }
        get {
                return UserDefaults.standard.string(forKey: "typeSelect")

        }
    }
}

struct CurrentProvider  {

    static var providerInfo : ProviderData?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentProvider");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentProvider")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentProvider") as? Data {
                return try? PropertyListDecoder().decode(ProviderData.self, from:data)

            }
            return nil
        }

    }
}


struct CurrentSettings  {

    static var settingsInfo : Settings?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentSettings");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentSettings")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentSettings") as? Data {
                return try? PropertyListDecoder().decode(Settings.self, from:data)

            }
            return nil
        }

    }
}

struct CurrentProviderProfile  {

    static var providerInfo : MyProviderData?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentProviderProfile");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentProviderProfile")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentProviderProfile") as? Data {
                return try? PropertyListDecoder().decode(MyProviderData.self, from:data)

            }
            return nil
        }

    }
}



/// App Link SocialNetwork

struct SocialNetworkUrl {
    let scheme: String
    let page: String

    func openPage() {
        let schemeUrl = NSURL(string: scheme)!
        if UIApplication.shared.canOpenURL(schemeUrl as URL) {
            UIApplication.shared.open(schemeUrl as URL)
        } else {
            UIApplication.shared.open(NSURL(string: page)! as URL)
        }
    }
}

enum SocialNetwork {
    case Facebook, Twitter, Instagram
    func url() -> SocialNetworkUrl {
        switch self {
        case .Facebook: return SocialNetworkUrl(scheme: "fb://profile?id=ZaadOman", page: "https://www.facebook.com/ZaadOman")
        case .Twitter: return SocialNetworkUrl(scheme: "twitter:///user?screen_name=ZaadOman", page: "https://twitter.com/ZaadOman")
        case .Instagram: return SocialNetworkUrl(scheme: "instagram://user?username=ZaadOman", page:"https://www.instagram.com/ZaadOman")

      }
    }
    func openPage() {
        self.url().openPage()
    }
 }

///SocialNetwork.Facebook.openPage()



extension DateFormatter {
    static var articleDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
}
