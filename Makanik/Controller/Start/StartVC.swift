//
//  StartVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit

class StartVC: SuperViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        start()
    }
    
    var myProviderdeatiles:MyProviderData?
    
    func start(){
        
        DispatchQueue.main.async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                if CurrentUser.userInfo != nil || CurrentProvider.providerInfo != nil{
                    let vc:TTabBarViewController = TTabBarViewController.loadFromNib()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                    self.getProfile()
                }else{
                    let mainVC = LoginVC.loadFromNib()
                    mainVC.modalPresentationStyle = .fullScreen
                    self.present(mainVC, animated: true, completion: nil)
                }
                self.getSettings()
            })
        }
        
        
    }
    
    func getSettings(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.settings).start(){ (response, error) in
                        
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(SettingsResponse.self, from: response.data!)
                CurrentSettings.settingsInfo = Status.data
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    func getProfile(){
        if CurrentUser.typeSelect == "client"{
            
            
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showmyProvider).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseMyProvider.self, from: response.data!)
                    CurrentProviderProfile.providerInfo = Status.data
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
    }

}
