//
//  EditePasswordVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit

class EditePasswordVC: SuperViewController {
    
    @IBOutlet weak var tfOldPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfComfiarmPassword: UITextField!

    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNav()
    }
    
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Edite Password".localized, sender: self, large: false)
    }

    @IBAction func didTab_Edite(_ sender: UIButton) {
        
        guard let oldpassword = self.tfOldPassword.text, !oldpassword.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your old password".localized)
            return
        }
        
        guard let newpassword = self.tfNewPassword.text, !newpassword.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your New password".localized)
            return
        }
        
        guard let connfirampassword = self.tfComfiarmPassword.text, !connfirampassword.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your Confiarm password".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["old_password"] = oldpassword
        parameters["password"] = newpassword
        parameters["password_confirmation"] = connfirampassword

        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.updatepasswordUser,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.updatepasswordProvider,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
 
        
    }
    
}
