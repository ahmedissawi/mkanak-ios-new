//
//  OrdersCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/17/21.
//

import UIKit

class OrdersCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblservices: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStartWork: UILabel!
    @IBOutlet weak var lblNamePrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgOrder: UIImageView!
    
    
    
    var currentorder:HomeProviderCurrentOrder!{
        didSet{
            self.lblName.text = currentorder.user?.name ?? ""
            self.lblservices.text = currentorder.service ?? ""
            self.lblTime.text = currentorder.createdAt ?? ""
            if currentorder.priority == "current"{
                self.lblStartWork.text  = "current priority".localized
            }else{
                self.lblStartWork.text = "later priority".localized
            }
            self.imgOrder.sd_custom(url: currentorder.user?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
            
            
            
            if self.currentorder?.type == "offer"{
                self.lblType.text = "Offer price".localized
                self.lblNamePrice.text = "Lowest Price".localized
                if self.currentorder?.minOffer == 0.0 || self.currentorder?.minOffer == 0 || self.currentorder?.minOffer == nil {
                    self.lblPrice.text = "No Price".localized
                }else{
                    self.lblPrice.text = "\(self.currentorder?.minOffer ?? 0.0)" + " " +  "SAR".localized
                }
            }else{
                self.lblType.text = "Fixed price".localized
                self.lblPrice.text = "\(self.currentorder?.price ?? "0.0")" + " " + "SAR".localized
                self.lblNamePrice.text = "Price".localized
            }
            if self.currentorder?.status == "completed"{
                self.lblPrice.text =  "\(self.currentorder?.price ?? "0.0")" + " " +  "SAR".localized
                self.lblNamePrice.text = "price".localized
            }
            
//
//            if currentorder.type == "offer"{
//                self.lblType.text = "Offer price".localized
//                self.lblNamePrice.text = "Lowest Price".localized
//                if currentorder.minOffer == 0.0 || currentorder.minOffer == 0 || currentorder.minOffer == nil {
//                    self.lblPrice.text = "No Price".localized
//                }else{
//                    self.lblPrice.text = "\(currentorder.minOffer ?? 0.0)" + " " +  "SAR".localized
//                }
//
//            }else{
//                self.lblType.text = "Fixed price".localized
//                self.lblPrice.text = "\(currentorder.price ?? "0")" + " " + "SAR".localized
//                self.lblNamePrice.text = "Price".localized
//
//            }
        }
    }
    
    
    var completedorder:HomeProviderCompletedOrder!{
        didSet{
            self.lblName.text = completedorder.user?.name ?? ""
            self.lblservices.text = completedorder.service ?? ""
            self.lblTime.text = completedorder.createdAt ?? ""
            if completedorder.priority == "current"{
                self.lblStartWork.text = "current priority".localized
            }else{
                self.lblStartWork.text = "later priority".localized
            }
            self.imgOrder.sd_custom(url: completedorder.user?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))

            
            if self.completedorder?.type == "offer"{
                self.lblType.text = "Offer price".localized
                self.lblNamePrice.text = "Lowest Price".localized
                if self.completedorder?.minOffer == 0.0 || self.completedorder?.minOffer == 0 || self.completedorder?.minOffer == nil {
                    self.lblPrice.text = "No Price".localized
                }else{
                    self.lblPrice.text = "\(self.completedorder?.minOffer ?? 0.0)" + " " +  "SAR".localized
                }
            }else{
                self.lblType.text = "Fixed price".localized
                self.lblPrice.text = "\(self.completedorder?.price ?? 0)" + " " + "SAR".localized
                self.lblNamePrice.text = "Price".localized
            }
            
            if self.completedorder?.status == "completed"{
                self.lblPrice.text =  "\(self.completedorder?.price ?? 0)" + " " +  "SAR".localized
                self.lblNamePrice.text = "price".localized
            }
        }
    }
    
    
    var order:OrderProviderItems!{
        didSet{
            
            self.lblTime.text = order.createdAt ?? ""
            if order.priority == "current"{
                self.lblStartWork.text = "current priority".localized
            }else{
                self.lblStartWork.text = "later priority".localized
            }
            
            if CurrentUser.typeSelect == "client"{
                
                self.imgOrder.sd_custom(url: order.serviceIcon ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                self.lblName.text = order.service ?? ""
                
                if order.type == "offer"{
//                    self.lblservices.text = "Offers made:".localized + " " +  "\(order.offers?.count ?? 0)"
                    self.lblservices.text = "\(order.offers?.count ?? 0)"
                }else{
                    self.lblservices.text = order.mechanic?.name ?? ""
                }
            }else{
                self.imgOrder.sd_custom(url: order.user?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                self.lblName.text = order.user?.name ?? ""
                self.lblservices.text = order.service ?? ""
                
            }
            
            
            if self.order?.type == "offer"{
                self.lblType.text = "Offer price".localized
                self.lblNamePrice.text = "Lowest Price".localized
                if self.order?.minOffer == 0.0 || self.order?.minOffer == 0 || self.order?.minOffer == nil {
                    self.lblPrice.text = "No Price".localized
                }else{
                    self.lblPrice.text = "\(self.order?.minOffer ?? 0.0)" + " " +  "SAR".localized
                }
            }else{
                self.lblType.text = "Fixed price".localized
                self.lblPrice.text = "\(self.order?.price ?? 0)" + " " + "SAR".localized
                self.lblNamePrice.text = "Price".localized
            }
            
            //Edite price after order is completed
            if self.order?.status == "completed"{
                self.lblPrice.text =  "\(self.order?.price ?? 0)" + " " +  "SAR".localized
                self.lblNamePrice.text = "price".localized
            }
        }
    }
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
}
