//
//  ProviderInformationVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit

class ProviderInformationVC: SuperViewController {
    
    @IBOutlet weak var lblmobile: UILabel!
    @IBOutlet weak var lblcompany: UILabel!
    @IBOutlet weak var lblworkID: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lbllicence: UILabel!
    @IBOutlet weak var lblGold: UILabel!
    @IBOutlet weak var imgGold: UIImageView!
    @IBOutlet weak var btEdite: UIButton!
    @IBOutlet weak var viewNumberWork: UIView!
    @IBOutlet weak var viewNumberlicence: UIView!

    
    var deatiles:DeatilesProviderData?
    var myProviderdeatiles:MyProviderData?
    var idProvider:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProviderDeatiles()
        if CurrentUser.typeSelect == "client"{
            self.btEdite.isHidden = true
        }
    }
    
    
    func getProviderDeatiles(){
        
        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.mechanicprofile,nestedParams: "\(idProvider ?? 0)").start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseDeatilesProvider.self, from: response.data!)
                    self.deatiles = Status.data
                    self.lblmobile.text = self.deatiles?.mobile ?? ""
                    self.lblEmail.text = self.deatiles?.email ?? ""
                    self.lblAddress.text = self.deatiles?.address ?? ""
                    self.lblcompany.text = self.deatiles?.type ?? ""
                    self.imgGold.sd_custom(url: self.deatiles?.medals?.icon ?? "")
                    self.lblGold.text = self.deatiles?.medals?.name ?? ""
                    if self.deatiles?.licence != nil{
                        self.viewNumberlicence.isHidden = false
                        self.lbllicence.text = self.deatiles?.licence ?? ""
                    }
                    if self.deatiles?.identificationNumber != nil{
                        self.viewNumberWork.isHidden = false
                        self.lblworkID.text = self.deatiles?.identificationNumber ?? ""
                    }
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showmyProvider).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseMyProvider.self, from: response.data!)
                    self.myProviderdeatiles = Status.data
                    self.lblmobile.text = self.myProviderdeatiles?.mobile ?? ""
                    self.lblEmail.text = self.myProviderdeatiles?.email ?? ""
                    self.lblAddress.text = self.myProviderdeatiles?.address ?? ""
                    self.lblcompany.text = self.myProviderdeatiles?.type ?? ""
                    self.imgGold.sd_custom(url: self.myProviderdeatiles?.medals?.icon ?? "")
                    self.lblworkID.text = self.myProviderdeatiles?.identificationNumber ?? ""
                    self.lblGold.text = self.myProviderdeatiles?.medals?.name ?? ""
                    if self.myProviderdeatiles?.licence != nil{
                        self.viewNumberlicence.isHidden = false
                        self.lbllicence.text = self.myProviderdeatiles?.licence ?? ""
                    }
                    if self.myProviderdeatiles?.identificationNumber != nil{
                        self.viewNumberWork.isHidden = false
                        self.lblworkID.text = self.myProviderdeatiles?.identificationNumber ?? ""
                    }

                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
        
    }
    
    @IBAction func didTab_Edite(_ sender: UIButton) {
        let vc = EditeProfileProviderVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
}

