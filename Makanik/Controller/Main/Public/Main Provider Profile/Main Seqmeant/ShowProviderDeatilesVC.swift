//
//  ShowProviderDeatilesVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit
import MXSegmentedPager
import Cosmos


class ShowProviderDeatilesVC: BaseViewController {
    
    
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet weak var imgProvider: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var viewRate: CosmosView!

    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    var deatiles:DeatilesProviderData?
    var id:Int?
    var rating:Double?
    
    
    var myProviderdeatiles:MyProviderData?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSeqment()
        getProviderDeatiles()

    }
    
    
    func setupSeqment(){

        if CurrentUser.typeSelect == "client"{
            arrayView = ["Account details","Services","Worker dates","Ratings"]
            arrayViewController = [ShowProviderInformationVC(),ShowServicesVC(),ShowWorkingDateVC(),ShowRatingsVC()]
        }else{
            arrayView = ["Account details","Services","Worker dates","Bank Account"]
            arrayViewController = [ShowProviderInformationVC(),ShowServicesVC(),ShowWorkingDateVC(),ShowBankVC()]
        }

        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }

            self.setUpSegmentation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateProfile(notification:)), name: Notification.Name("UpdateProfile"), object: nil)

    }
    
    @objc func UpdateProfile(notification: NSNotification)  {
        getProviderDeatiles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
            let navigation = self.navigationController as! CustomNavigationBar
            navigation.navigationBar.isTranslucent = true
            navigation.navigationBar.shadowImage = UIImage()
            navigation.navigationBar.setBackgroundImage(UIImage(), for: .default)
        if CurrentUser.typeSelect == "client"{
            navigation.setCustomBackButtonForViewController(sender: self)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
            let navigation = self.navigationController as! CustomNavigationBar
            navigation.navigationBar.isTranslucent = false
    }
    
    
    func getProviderDeatiles(){
        if CurrentUser.typeSelect == "client"{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.mechanicprofile,nestedParams: "\(id ?? 0)").start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseDeatilesProvider.self, from: response.data!)
                    self.deatiles = Status.data
                    self.lblname.text = self.deatiles?.name ?? ""
                    self.viewRate.rating = Double(self.deatiles?.ratingAverage ?? 0)
                    self.imgProvider.sd_custom(url: self.deatiles?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showmyProvider).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseMyProvider.self, from: response.data!)
                    self.myProviderdeatiles = Status.data
                    self.lblname.text = self.myProviderdeatiles?.name ?? ""
                    self.viewRate.rating = Double(self.myProviderdeatiles?.rating ?? 0)
                    self.imgProvider.sd_custom(url: self.myProviderdeatiles?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
                        
        }
    }
    
    
}


extension ShowProviderDeatilesVC {
    
    
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = "2476B8".color
        segmentedPager.segmentedControl.backgroundColor = "2476B8".color
        segmentedPager.parallaxHeader.view = HeaderView
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 195
        segmentedPager.parallaxHeader.minimumHeight = 110
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "EDAE20".color]
        
        
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.clear
        segmentedPager.segmentedControl.selectionIndicatorHeight = 2.5
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont.NeoSansArabicMedium(ofSize: 15), NSAttributedString.Key.foregroundColor :  UIColor.white]
    }
    
    
    
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
       return  arrayView[index].localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }

    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        return arrayViewController[index]
    }
    
    
    fileprivate  func ShowProviderInformationVC() ->ProviderInformationVC{
        let vc:ProviderInformationVC = ProviderInformationVC.loadFromNib()
        addChild(vc)
        vc.didMove(toParent: self)
        vc.idProvider = id
        return vc
        
    }
    
    fileprivate  func ShowServicesVC() ->ShowTableDataVC{
        let vc:ShowTableDataVC = ShowTableDataVC.loadFromNib()
        addChild(vc)
        vc.didMove(toParent: self)
        vc.isFromServices = true
        vc.idProvider = id
        return vc
    }
        
    fileprivate  func ShowWorkingDateVC() ->ShowTableDataVC{
        let vc:ShowTableDataVC = ShowTableDataVC.loadFromNib()
        addChild(vc)
        vc.didMove(toParent: self)
        vc.isFromWorking = true
        vc.idProvider = id
        return vc
    }
    
    
    fileprivate  func ShowBankVC() ->ShowTableDataVC{
        let vc:ShowTableDataVC = ShowTableDataVC.loadFromNib()
        addChild(vc)
        vc.didMove(toParent: self)
        vc.isFromBank = true
        vc.idProvider = id
        return vc
    }
    
    fileprivate  func ShowRatingsVC() ->ShowTableDataVC{
        let vc:ShowTableDataVC = ShowTableDataVC.loadFromNib()
        addChild(vc)
        vc.didMove(toParent: self)
        vc.isFromRating = true
        vc.idProvider = id
        return vc
    }
    
}
