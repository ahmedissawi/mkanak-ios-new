//
//  ServicesCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit

class ServicesCell: UITableViewCell {
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblSubCatgory: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var editeStack: UIStackView!
    @IBOutlet weak var btEdite: UIButton!
    @IBOutlet weak var btClose: UIButton!

    var Edite: (() -> ())?
    var Close: (() -> ())?
    
    
    var service:MechanicService!{
        didSet{
            self.lblCategory.text = service.service?.name
            self.imgCategory.sd_custom(url: service.service?.icon ?? "")
            self.lblSubCatgory.text = service.service?.subCategory?.name ?? ""
        }
    }
    
    var myservice:ServicesData!{
        didSet{
            self.lblCategory.text = myservice.service?.name
            self.imgCategory.sd_custom(url: myservice.service?.icon ?? "")
            self.lblSubCatgory.text = myservice.service?.subCategory?.name ?? ""
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    @IBAction func btEdite(_ sender: UIButton) {
        Edite?()
    }
    

    @IBAction func btClose(_ sender: UIButton) {
        Close?()
    }
    
    
    
}
