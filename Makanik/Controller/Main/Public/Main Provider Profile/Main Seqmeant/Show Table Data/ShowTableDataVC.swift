//
//  ShowTableDataVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit

class ShowTableDataVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btStatus: UIButton!
    
    
    var isFromServices = false
    var isFromWorking = false
    var isFromRating = false
    var isFromBank = false
    
    var services = [MechanicService]()
    var workdays = [WorkDaysDeatilesProvider]()
    var rating = [RatingDeatilesProvider]()
    var bank = [MechanicBanks]()
    var deatiles:DeatilesProviderData?
    var myProviderdeatiles:MyProviderData?
    var myProviderservices = [ServicesData]()
    var idProvider:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "RatingProviderCell")
        tableView.registerCell(id: "DaysWorkCell")
        tableView.registerCell(id: "ServicesCell")
        tableView.registerCell(id: "BanksCell")
        if CurrentUser.typeSelect == "provider"{
            getServices()
            if isFromServices{
                self.btStatus.setTitle("Add New Services".localized, for: .normal)
            }else if isFromWorking {
                self.btStatus.setTitle("Add New Work Days".localized, for: .normal)
            }else{
                self.btStatus.setTitle("Add a new bank account".localized, for: .normal)
            }
        }else{
            self.btStatus.isHidden = true
        }
        getProviderDeatiles()
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateBank(notification:)), name: Notification.Name("UpdateBank"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateWorkDays(notification:)), name: Notification.Name("UpdateWorkDays"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateService(notification:)), name: Notification.Name("UpdateService"), object: nil)
        
        
    }
    
    
    @objc func UpdateBank(notification: NSNotification)  {
        getProviderDeatiles()
    }
    
    @objc func UpdateWorkDays(notification: NSNotification)  {
        getProviderDeatiles()
    }
    
    @objc func UpdateService(notification: NSNotification)  {
        getServices()
    }
    
    func getProviderDeatiles(){
        
        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.mechanicprofile,nestedParams: "\(idProvider ?? 0)").start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseDeatilesProvider.self, from: response.data!)
                    self.deatiles = Status.data
                    self.services = self.deatiles?.mechanicService ?? []
                    self.workdays = self.deatiles?.workDays ?? []
                    self.rating = self.deatiles?.rating ?? []
                    
                    if self.isFromServices{
                        if self.deatiles?.mechanicService?.count == 0{
                            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                            self.emptyView?.firstLabel.text = "There are no services to display.".localized
                            
                            self.services.removeAll()
                            self.tableView.reloadData()
                        }else{
                            self.tableView.tableFooterView = nil
                        }
                    }
                    
                    if self.isFromWorking{
                        if self.deatiles?.workDays?.count == 0{
                            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                            self.emptyView?.firstLabel.text = "There are no days work to display.".localized
                            
                            self.workdays.removeAll()
                            self.tableView.reloadData()
                        }else{
                            self.tableView.tableFooterView = nil
                        }
                    }
                    
                    if self.isFromRating{
                        if self.deatiles?.rating?.count == 0{
                            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                            self.emptyView?.firstLabel.text = "There are no rating to display.".localized
                            
                            self.rating.removeAll()
                            self.tableView.reloadData()
                        }else{
                            self.tableView.tableFooterView = nil
                        }
                    }
                    
                    self.tableView.reloadData()
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showmyProvider).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseMyProvider.self, from: response.data!)
                    self.myProviderdeatiles = Status.data
                    
                    self.workdays = self.myProviderdeatiles?.workDays ?? []
                    self.bank = self.myProviderdeatiles?.mechanicBanks ?? []
                    
                    if self.isFromWorking{
                        if self.myProviderdeatiles?.workDays?.count == 0 || self.myProviderdeatiles?.workDays == nil {
                            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                            self.emptyView?.firstLabel.text = "There are no days work to display.".localized
                            
                            self.workdays.removeAll()
                            self.tableView.reloadData()
                        }else{
                            self.tableView.tableFooterView = nil
                        }
                    }
                    
                    if self.isFromBank{
                        if self.myProviderdeatiles?.mechanicBanks?.count == 0 || self.myProviderdeatiles?.mechanicBanks ==  nil {
                            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                            self.emptyView?.firstLabel.text = "There are no banks data to display.".localized
                            
                            self.bank.removeAll()
                            self.tableView.reloadData()
                        }else{
                            self.tableView.tableFooterView = nil
                        }
                    }
                    
                    self.tableView.reloadData()
                    
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
    }
    
    
    
    @objc func didRefersh(sender: UIButton) {
        self.getProviderDeatiles()
    }
    
    func getServices(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showservicesProvider).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseServices.self, from: response.data!)
                self.myProviderservices = Status.data ?? []
                
                if self.isFromServices{
                    if self.myProviderservices.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no services to display.".localized
                        
                        self.myProviderservices.removeAll()
                        self.tableView.reloadData()
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                }
                
                self.tableView.reloadData()
                
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    @IBAction func didTab_Add(_ sender: UIButton) {
        if isFromServices{
            let vc:AddServicesVC = AddServicesVC.loadFromNib()
            vc.hidesBottomBarWhenPushed  = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if isFromWorking {
            let vc:AddWorkDaysVC = AddWorkDaysVC.loadFromNib()
            vc.hidesBottomBarWhenPushed  = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc:AddBankVC = AddBankVC.loadFromNib()
            vc.hidesBottomBarWhenPushed  = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func makedeleteBank(idBank:Int){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.deletebank,nestedParams: "\(idBank)").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    CATransaction.begin()
                    self.navigationController?.popViewController(animated: true)
                    CATransaction.setCompletionBlock({ [weak self] in
                        NotificationCenter.default.post(name: Notification.Name("UpdateBank"), object: nil)
                        self?.showAlert(title: "", message: "Done successfully".localized)
                    })
                    CATransaction.commit()
                    
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    func makedeleteWorkDay(idDay:Int){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.deleteworkDays,nestedParams: "\(idDay)").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    CATransaction.begin()
                    self.navigationController?.popViewController(animated: true)
                    CATransaction.setCompletionBlock({ [weak self] in
                        NotificationCenter.default.post(name: Notification.Name("UpdateWorkDays"), object: nil)
                        self?.showAlert(title: "", message: "Done successfully".localized)
                    })
                    CATransaction.commit()
                    
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    func makedeleteService(idService:Int){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.deleteservices,nestedParams: "\(idService)").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    CATransaction.begin()
                    self.navigationController?.popViewController(animated: true)
                    CATransaction.setCompletionBlock({ [weak self] in
                        NotificationCenter.default.post(name: Notification.Name("UpdateService"), object: nil)
                        self?.showAlert(title: "", message: "Done successfully".localized)
                    })
                    CATransaction.commit()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    
}


extension ShowTableDataVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if CurrentUser.typeSelect == "client"{
            if isFromServices{
                if services.count != 0{
                    return services.count
                }else{
                    return 0
                }
            }else if isFromWorking{
                if workdays.count != 0{
                    return workdays.count
                }else{
                    return 0
                }
            }else{
                
                if rating.count != 0{
                    return rating.count
                }else{
                    return 0
                }
            }
        }else{
            if isFromServices{
                if myProviderservices.count != 0{
                    return myProviderservices.count
                }else{
                    return 0
                }
            }
            if isFromWorking{
                if workdays.count != 0{
                    return workdays.count
                }else{
                    return 0
                }
            }
            
            if isFromBank{
                if bank.count != 0{
                    return bank.count
                }else{
                    return 0
                }
            }
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if CurrentUser.typeSelect == "client"{
            if isFromServices{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell", for: indexPath) as! ServicesCell
                cell.service = services[indexPath.row]
                return cell
            }else if isFromWorking{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DaysWorkCell", for: indexPath) as! DaysWorkCell
                cell.days = workdays[indexPath.row]
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "RatingProviderCell", for: indexPath) as! RatingProviderCell
                tableView.separatorStyle = .singleLine
                cell.rating = rating[indexPath.row]
                return cell
            }
        }else{
            if isFromServices{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell", for: indexPath) as! ServicesCell
                cell.myservice = myProviderservices[indexPath.row]
                cell.editeStack.isHidden = false
                cell.Edite = { [weak self] in
                    let vc:AddServicesVC = AddServicesVC.loadFromNib()
                    vc.isFromEdite = true
                    vc.idService = self?.myProviderservices[indexPath.row].id
                    vc.hidesBottomBarWhenPushed = true
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                cell.Close = { [weak self] in
                    self?.makedeleteService(idService:self?.myProviderservices[indexPath.row].id ?? 0)
                }
                
                return cell
            }else if isFromWorking{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DaysWorkCell", for: indexPath) as! DaysWorkCell
                cell.days = workdays[indexPath.row]
                cell.editeStack.isHidden = false
                cell.Edite = { [weak self] in
                    let vc:AddWorkDaysVC = AddWorkDaysVC.loadFromNib()
                    vc.isFromEdite = true
                    vc.idDay = self?.workdays[indexPath.row].id
                    vc.hidesBottomBarWhenPushed = true
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                cell.Close = { [weak self] in
                    self?.makedeleteWorkDay(idDay:self?.workdays[indexPath.row].id ?? 0)
                }
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "BanksCell", for: indexPath) as! BanksCell
                cell.bank = bank[indexPath.row]
                cell.Edite = { [weak self] in
                    let vc:AddBankVC = AddBankVC.loadFromNib()
                    vc.idBank = self?.bank[indexPath.row].id
                    vc.isFromEdite = true
                    vc.hidesBottomBarWhenPushed = true
                    self?.navigationController?.pushViewController(vc, animated: true)
                    
                }
                cell.Close = { [weak self] in
                    self?.makedeleteBank(idBank:self?.bank[indexPath.row].id ?? 0)
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if CurrentUser.typeSelect == "client"{
            if isFromServices{
                return 80
            }else if isFromWorking{
                return 70
            }else{
                return UITableView.automaticDimension
            }
        }else{
            if isFromServices{
                return 80
            }else if isFromWorking{
                return 70
            }else{
                return 80
            }
        }
    }
}
