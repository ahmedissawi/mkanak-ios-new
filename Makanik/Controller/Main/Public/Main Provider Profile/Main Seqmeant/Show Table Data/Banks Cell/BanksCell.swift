//
//  BanksCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21.
//

import UIKit

class BanksCell: UITableViewCell {
    
    @IBOutlet weak var lblbankname: UILabel!
    @IBOutlet weak var lblbanknumber: UILabel!
    @IBOutlet weak var btEdite: UIButton!
    @IBOutlet weak var btClose: UIButton!

    var Edite: (() -> ())?
    var Close: (() -> ())?

    
    var bank:MechanicBanks!{
        didSet{
            self.lblbankname.text = bank.ownerName ?? ""
            self.lblbanknumber.text = bank.accountNumber ?? ""
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btEdite(_ sender: UIButton) {
        Edite?()
    }
    

    @IBAction func btClose(_ sender: UIButton) {
        Close?()
    }
    
}
