//
//  RatingProviderCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit
import Cosmos

class RatingProviderCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var lblDescrbtion: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var rating:RatingDeatilesProvider!{
        didSet{
            self.lblName.text = rating.user?.name ?? ""
            self.lblDescrbtion.text = rating.comment ?? ""
            self.viewRate.rating = Double(rating.rating ?? 0)
            self.lblDate.text = rating.createdAt ?? ""
        }
    }
    

}
