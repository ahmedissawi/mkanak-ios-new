//
//  DaysWorkCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit

class DaysWorkCell: UITableViewCell {
    
    @IBOutlet weak var lblNameDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var editeStack: UIStackView!
    @IBOutlet weak var btEdite: UIButton!
    @IBOutlet weak var btClose: UIButton!

    var Edite: (() -> ())?
    var Close: (() -> ())?



    override func awakeFromNib() {
        super.awakeFromNib()
    }


    var days:WorkDaysDeatilesProvider!{
        didSet{
            self.lblNameDay.text = days.dayName ?? ""
            self.lblDate.text =  "\(days.fromTime ?? "")" + " " + "To".localized + " " + "\(days.toTime ?? "")"
        }
    }
    
    @IBAction func btEdite(_ sender: UIButton) {
        Edite?()
    }
    

    @IBAction func btClose(_ sender: UIButton) {
        Close?()
    }
    
    
    
}
