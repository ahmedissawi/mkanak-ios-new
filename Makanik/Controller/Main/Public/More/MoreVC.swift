//
//  MoreVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit
import SwiftEntryKit
import Firebase
import FirebaseMessaging

class MoreVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "MuneMoreCell")
        setupnav()
    }
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("More".localized, sender: self, large: false)
    }
    
    func makeLogOut(){
        
        if CurrentUser.typeSelect == "client"{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.logoutUser,isAuthRequired: true).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: true) { () -> Void in
                            CurrentUser.userInfo = nil
                            //unsubscribe client
                            Messaging.messaging().unsubscribe(fromTopic: "users")
                            Messaging.messaging().unsubscribe(fromTopic: "user_\(CurrentUser.userInfo?.id ?? 0)")

                            let mainVC = LoginVC()
                            UIApplication.shared.keyWindow?.rootViewController = mainVC
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.logoutProvider,isAuthRequired: true).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: true) { () -> Void in
                            CurrentProvider.providerInfo = nil
                            //unsubscribe provider
                            Messaging.messaging().unsubscribe(fromTopic: "mechanics")
                            Messaging.messaging().unsubscribe(fromTopic: "mechanic_\(CurrentProvider.providerInfo?.id ?? 0)")

                            let mainVC = LoginVC()
                            UIApplication.shared.keyWindow?.rootViewController = mainVC
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            
        }
        
    }
    
    
    
    
}

extension MoreVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch CurrentUser.typeSelect {
        case "client":
            let data  = MenuOptionsUser.allValues.map { $0.description }
            return data.count
        case "provider":
            let data  = MenuOptionsProvider.allValues.map { $0.description }
            return data.count
        default:
            let data  = MenuOptionsVistore.allValues.map { $0.description }
            return data.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MuneMoreCell", for: indexPath) as! MuneMoreCell
        switch CurrentUser.typeSelect {
        case "client":
            let obj = MenuOptionsUser.init(rawValue: indexPath.row)
            cell.lblmunename.text = obj?.description
            cell.imgmune.image = obj?.image
        case "provider":
            let obj = MenuOptionsProvider.init(rawValue: indexPath.row)
            cell.lblmunename.text = obj?.description
            cell.imgmune.image = obj?.image
        default:
            let obj = MenuOptionsVistore.init(rawValue: indexPath.row)
            cell.lblmunename.text = obj?.description
            cell.imgmune.image = obj?.image
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if CurrentUser.typeSelect == "client"{
            let obj = MenuOptionsUser.init(rawValue: indexPath.row)
            switch obj {
            case .wallet:
                let vc = WalletVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .profile:
                let vc = EditeProfileVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .password:
                let vc = EditePasswordVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .share:
                self.showShareActivity(msg: CurrentSettings.settingsInfo?.iosUrl ?? "", image: nil, url: nil, sourceRect: nil)
            case .contactUs:
                let vc = ContactUs.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .aboutUs:
                let vc = InfoVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.keyInfo = "about_us"
                self.navigationController?.pushViewController(vc, animated: true)
            case .terms:
                let vc = InfoVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.keyInfo = "usage_policy"
                self.navigationController?.pushViewController(vc, animated: true)
            case .support:
                let vc = SupportVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .settings:
                let vc = SettingsVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .logout:
                self.showAlertDialog(attributes: EKAttributes.centerFloat,titleButon:"Log Out".localized, title: "Log Out!".localized, message: "Are you sure you want to logout ?".localized) {
                    self.makeLogOut()
                }

            default:
                print("default")
            }
        }else if CurrentUser.typeSelect == "provider" {
            let obj = MenuOptionsProvider.init(rawValue: indexPath.row)
            switch obj {
            case .wallet:
                let vc = WalletVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .service:
                let vc = MakeFeatureServiceVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .password:
                let vc = EditePasswordVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .share:
                self.showShareActivity(msg: CurrentSettings.settingsInfo?.iosUrl ?? "", image: nil, url: nil, sourceRect: nil)
            case .contactUs:
                let vc = ContactUs.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .aboutUs:
                let vc = InfoVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.keyInfo = "about_us"
                self.navigationController?.pushViewController(vc, animated: true)
            case .terms:
                let vc = InfoVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.keyInfo = "usage_policy"
                self.navigationController?.pushViewController(vc, animated: true)
            case .support:
                let vc = SupportVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .settings:
                let vc = SettingsVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .logout:
                self.showAlertDialog(attributes: EKAttributes.centerFloat,titleButon:"Log Out".localized, title: "Log Out!".localized, message: "Are you sure you want to logout ?".localized) {
                    self.makeLogOut()
                }
            default:
                print("default")
            }
        }else{
            let obj = MenuOptionsVistore.init(rawValue: indexPath.row)
            switch obj {
            case .share:
                self.showShareActivity(msg: CurrentSettings.settingsInfo?.iosUrl ?? "", image: nil, url: nil, sourceRect: nil)
            case .aboutUs:
                let vc = InfoVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.keyInfo = "about_us"
                self.navigationController?.pushViewController(vc, animated: true)
            case .terms:
                let vc = InfoVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                vc.keyInfo = "usage_policy"
                self.navigationController?.pushViewController(vc, animated: true)
            case .settings:
                let vc = SettingsVC.loadFromNib()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case .login:
                self.showAlertDialog(attributes: EKAttributes.centerFloat,titleButon:"Login App".localized + "\n" , title: "Alert!".localized, message: "You are browsing the application without logging in.".localized + "\n\n" + "Please login to use the application better".localized) {
                    let mainVC = LoginVC.loadFromNib()
                    mainVC.modalPresentationStyle = .fullScreen
                    self.present(mainVC, animated: true, completion: nil)
                }
            default:
                print("default")
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
