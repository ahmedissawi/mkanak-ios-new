//
//  MenuOptionsUser.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit


enum MenuOptionsUser: Int, CustomStringConvertible {
    
    case wallet
    case profile
    case password
    case share
    case contactUs
    case aboutUs
    case terms
    case support
    case settings
    case logout

    
    static let allValues = [wallet,profile,password,share,contactUs,aboutUs,terms,support,settings,logout]
    
    var description: String {
        switch self {
        case .wallet: return "Wallet".localized
        case .profile: return "Edite Profile".localized
        case .password: return "Edite Password".localized
        case .share: return "Share App".localized
        case .contactUs: return "Contact Us".localized
        case .aboutUs: return "About Us".localized
        case .terms: return "Terms and Conditions".localized
        case .support: return "Support".localized
        case .settings: return "Settings".localized
        case .logout: return "Log out".localized
        }
    }
    
    var image: UIImage {
        switch self {
        case .wallet: return UIImage(named: "wallet") ?? UIImage()
        case .profile: return UIImage(named: "user-alt") ?? UIImage()
        case .password: return UIImage(named: "lock-alt") ?? UIImage()
        case .share: return UIImage(named: "share-alt") ?? UIImage()
        case .contactUs: return UIImage(named: "envelope-open") ?? UIImage()
        case .aboutUs: return UIImage(named: "ic_aboutus") ?? UIImage()
        case .terms: return UIImage(named: "terms-and-conditions") ?? UIImage()
        case .support: return UIImage(named: "user-headset") ?? UIImage()
        case .settings: return UIImage(named: "cog") ?? UIImage()
        case .logout: return UIImage(named: "sign-out") ?? UIImage()
        }
    }
    
}

enum MenuOptionsProvider: Int, CustomStringConvertible {
    
    case wallet
    case service
    case password
    case share
    case contactUs
    case aboutUs
    case terms
    case support
    case settings
    case logout

    
    static let allValues = [wallet,service,password,share,contactUs,aboutUs,terms,support,settings,logout]
    
    var description: String {
        switch self {
        case .wallet: return "Wallet".localized
        case .service: return "Service distinction".localized
        case .password: return "Edite Password".localized
        case .share: return "Share App".localized
        case .contactUs: return "Contact Us".localized
        case .aboutUs: return "About Us".localized
        case .terms: return "Terms and Conditions".localized
        case .support: return "Support".localized
        case .settings: return "Settings".localized
        case .logout: return "Log out".localized
        }
    }
    
    var image: UIImage {
        switch self {
        case .wallet: return UIImage(named: "wallet") ?? UIImage()
        case .service: return UIImage(named: "stars") ?? UIImage()
        case .password: return UIImage(named: "lock-alt") ?? UIImage()
        case .share: return UIImage(named: "share-alt") ?? UIImage()
        case .contactUs: return UIImage(named: "envelope-open") ?? UIImage()
        case .aboutUs: return UIImage(named: "ic_aboutus") ?? UIImage()
        case .terms: return UIImage(named: "terms-and-conditions") ?? UIImage()
        case .support: return UIImage(named: "user-headset") ?? UIImage()
        case .settings: return UIImage(named: "cog") ?? UIImage()
        case .logout: return UIImage(named: "sign-out") ?? UIImage()
        }
    }
    
    
    
}

enum MenuOptionsVistore: Int, CustomStringConvertible {
    
    case share
    case aboutUs
    case terms
    case settings
    case login

    
    static let allValues = [share,settings,aboutUs,terms,login]
    
    var description: String {
        switch self {
        case .share: return "Share App".localized
        case .aboutUs: return "About Us".localized
        case .terms: return "Terms and Conditions".localized
        case .settings: return "Settings".localized
        case .login: return "Sign In".localized
        }
    }
    
    var image: UIImage {
        switch self {
        case .share: return UIImage(named: "share-alt") ?? UIImage()
        case .aboutUs: return UIImage(named: "ic_aboutus") ?? UIImage()
        case .terms: return UIImage(named: "terms-and-conditions") ?? UIImage()
        case .settings: return UIImage(named: "cog") ?? UIImage()
        case .login: return UIImage(named: "sign-out") ?? UIImage()
        }
    }
    
    
    
}
