//
//  OrderPriceCard.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit

class OrderPriceCard: UIView {

    @IBOutlet weak var lblPrice: UILabel!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    init() {
        super.init(frame: .zero)
        fromNib()
    }
    

}
