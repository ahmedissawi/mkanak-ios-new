//
//  OrderQuantitiesCard.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit

class OrderQuantitiesCard: UIView {

    @IBOutlet weak var lblQuantities: UILabel!
    @IBOutlet weak var btMinus: UIButton!
    @IBOutlet weak var btPlus: UIButton!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    init() {
        super.init(frame: .zero)
        fromNib()
    }
    
    
    
}
