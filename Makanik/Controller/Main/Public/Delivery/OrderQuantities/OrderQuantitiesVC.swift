//
//  OrderQuantitiesVC.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit
import CollectionKit

class OrderQuantitiesVC: SuperViewController {
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var OrderItems: CollectionView!
    @IBOutlet weak var OrderPriceProvider: CollectionView!

    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    
    // MARK: Properties
    
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNav()
        SetupQuantities()
        SetupOrderPrice()
        self.txtNote.placeholders = "Write notes here on order".localized
    }
    
    //MARK: - Factory Functions
    func setupNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Order Deatiles".localized, sender: self, large: false)
        navigation.setCustomBackButtonForViewController(sender: self)
    }
    
    
    func SetupOrderPrice() {
        
        let selectionAction = { (context: BasicProvider<String, OrderPriceCard>.TapContext) -> Void in
            
        }
        let data  = ["dssd","ds","aa","ds","www","qqq"]
        
        
        let cellUpdater = { (cell: OrderPriceCard, data: String, index: Int) in
            // cell.camel = self.homeObj?.offlineAuctions?[index]
            
            if MOLHLanguage.isRTLLanguage() {
                cell.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
        let viewSource = ClosureViewSource(viewUpdater: cellUpdater)
        let sizeSource = { (index: Int, data: String, collectionSize: CGSize) -> CGSize in
            return CGSize(width: 120, height: 65)
        }
        
        let provider = BasicProvider<String, OrderPriceCard>(dataSource: ArrayDataSource(data: data),
                                                                  viewSource: viewSource,
                                                                  sizeSource: sizeSource,
                                                                  layout:RowLayout(spacing: 10, justifyContent: .start).inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)),
                                                                  tapHandler: selectionAction)
        OrderPriceProvider.delegate = self
        OrderPriceProvider.showsVerticalScrollIndicator = false
        OrderPriceProvider.showsHorizontalScrollIndicator = false
        OrderPriceProvider.provider = provider
        if MOLHLanguage.isRTLLanguage() {
            
            OrderPriceProvider.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func SetupQuantities() {
        
        let selectionAction = { (context: BasicProvider<String, OrderQuantitiesCard>.TapContext) -> Void in
            
        }
        let data  = ["dssd","ds","aa","ds","www","qqq"]
        
        
        let cellUpdater = { (cell: OrderQuantitiesCard, data: String, index: Int) in
            // cell.camel = self.homeObj?.offlineAuctions?[index]
            
            if MOLHLanguage.isRTLLanguage() {
                cell.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
        let viewSource = ClosureViewSource(viewUpdater: cellUpdater)
        let sizeSource = { (index: Int, data: String, collectionSize: CGSize) -> CGSize in
            return CGSize(width: collectionSize.width, height: 50)
        }
        
        let provider = BasicProvider<String, OrderQuantitiesCard>(dataSource: ArrayDataSource(data: data),
                                                                  viewSource: viewSource,
                                                                  sizeSource: sizeSource,
                                                                  layout: FlowLayout(lineSpacing: 10).inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)),
                                                                  tapHandler: selectionAction)
        OrderItems.delegate = self
        OrderItems.showsVerticalScrollIndicator = false
        OrderItems.showsHorizontalScrollIndicator = false
        OrderItems.provider = provider
        if MOLHLanguage.isRTLLanguage() {
            
            OrderItems.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
    //MARK: - IBAction

    @IBAction func didTab_Add(_ sender: UIButton) {
        
    }
    
    @IBAction func didTab_Next(_ sender: UIButton) {
        let vc = MakeOrderVC.loadFromNib()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    
}


//MARK: - UIScrollViewDelegate

extension OrderQuantitiesVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //    layoutSearchbar()
        
    }
    
    
}
