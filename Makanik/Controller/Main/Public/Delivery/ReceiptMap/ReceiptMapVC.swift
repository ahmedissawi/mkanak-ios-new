//
//  ReceiptMapVC.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit
import GoogleMaps


class ReceiptMapVC: SuperViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblsenderStreet: UILabel!
    @IBOutlet weak var lblsenderAddress: UILabel!
    @IBOutlet weak var lblrecipientStreet: UILabel!

    // MARK: Properties
    
    
    // MARK: - Life Cycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNav()
        
    }

    
    //MARK: - Factory Functions
    func setupNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Receiving location".localized, sender: self, large: false)
        navigation.setCustomBackButtonForViewController(sender: self)
    }
    
    
    //MARK: - IBAction
    @IBAction func didTab_Next(_ sender: UIButton) {
        let vc = OrderDeatilesVC.loadFromNib()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}


//MARK:- GMSMapViewDelegate

extension ReceiptMapVC:GMSMapViewDelegate{
    
    
    
}
