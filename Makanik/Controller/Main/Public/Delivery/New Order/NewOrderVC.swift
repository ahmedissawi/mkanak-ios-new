//
//  NewOrderVC.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit

class NewOrderVC: SuperViewController {
    
    // MARK: - IBOutlet

    @IBOutlet weak var lbltitleSend: UILabel!
    @IBOutlet weak var lblsubtitleSend: UILabel!
    @IBOutlet weak var lbldescSend: UILabel!

    @IBOutlet weak var lbltitleOrder: UILabel!
    @IBOutlet weak var lblsubtitleOrder: UILabel!
    @IBOutlet weak var lbldescOrder: UILabel!

    
    @IBOutlet weak var lbltitleContact: UILabel!
    @IBOutlet weak var lblsubtitleContact: UILabel!
    @IBOutlet weak var lbldescContact: UILabel!

    
    @IBOutlet weak var stackSend: UIStackView!
    @IBOutlet weak var stackOrder: UIStackView!
    @IBOutlet weak var stackContact: UIStackView!
    

    @IBOutlet weak var viewkSend: UIView!
    @IBOutlet weak var viewkOrder: UIView!
    @IBOutlet weak var viewkContact: UIView!

    
    
    // MARK: Properties
    
    
    // MARK: - Life Cycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNav()
    }

    //MARK: - Factory Functions
    func setupNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("New Order".localized, sender: self, large: false)
        navigation.setCustomBackButtonForViewController(sender: self)
    }
    
    //MARK: - IBAction

    @IBAction func didTab_Service(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.lbldescSend.showAnimated(in: self.stackSend)
            self.lbldescOrder.hideAnimated(in: self.stackOrder)
            self.lbldescContact.hideAnimated(in: self.stackContact)
            
            self.viewkSend.borderWidth = 1
            self.viewkSend.borderColor = "586AF6".color

            self.viewkOrder.borderWidth = 0
            self.viewkContact.borderWidth = 0

            
        case 2:
            self.lbldescSend.hideAnimated(in: self.stackSend)
            self.lbldescOrder.showAnimated(in: self.stackOrder)
            self.lbldescContact.hideAnimated(in: self.stackContact)
            
            
            self.viewkOrder.borderWidth = 1
            self.viewkOrder.borderColor = "586AF6".color

            self.viewkSend.borderWidth = 0
            self.viewkContact.borderWidth = 0
            
        case 3:
            self.lbldescSend.hideAnimated(in: self.stackSend)
            self.lbldescOrder.hideAnimated(in: self.stackOrder)
            self.lbldescContact.showAnimated(in: self.stackContact)
            
            self.viewkContact.borderWidth = 1
            self.viewkContact.borderColor = "586AF6".color

            self.viewkSend.borderWidth = 0
            self.viewkOrder.borderWidth = 0

        default:
            break
        }
        
    }
        
    @IBAction func didTab_Next(_ sender: UIButton) {
        let vc = ReceiptMapVC.loadFromNib()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    
    
}
