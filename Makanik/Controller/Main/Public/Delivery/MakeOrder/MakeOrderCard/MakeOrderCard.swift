//
//  MakeOrderCard.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit

class MakeOrderCard: UIView {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btTrash: UIButton!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    init() {
        super.init(frame: .zero)
        fromNib()
    }

}
