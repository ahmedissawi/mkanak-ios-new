//
//  MakeOrderVC.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit
import CollectionKit

class MakeOrderVC: SuperViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var ReviewOrderProvider: CollectionView!
    @IBOutlet weak var PaymentCardProvider: CollectionView!
    @IBOutlet weak var lblAddress: UILabel!


    // MARK: Properties
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNav()
        SetupOrderPrice()
        SetupPaymentCard()
    }

    //MARK: - Factory Functions
    func setupNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Order Deatiles".localized, sender: self, large: false)
        navigation.setCustomBackButtonForViewController(sender: self)
    }
    
    func SetupOrderPrice() {
        
        let selectionAction = { (context: BasicProvider<String, MakeOrderCard>.TapContext) -> Void in
            
        }
        let data  = ["dssd","ds","aa","ds","www","qqq"]
        
        
        let cellUpdater = { (cell: MakeOrderCard, data: String, index: Int) in
            // cell.camel = self.homeObj?.offlineAuctions?[index]
            
            if MOLHLanguage.isRTLLanguage() {
                cell.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
        let viewSource = ClosureViewSource(viewUpdater: cellUpdater)
        let sizeSource = { (index: Int, data: String, collectionSize: CGSize) -> CGSize in
            return CGSize(width: collectionSize.width, height: 75)
        }
        
        let provider = BasicProvider<String, MakeOrderCard>(dataSource: ArrayDataSource(data: data),
                                                                  viewSource: viewSource,
                                                                  sizeSource: sizeSource,
                                                                  layout:FlowLayout(lineSpacing: 0).inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)),
                                                                  tapHandler: selectionAction)
        ReviewOrderProvider.delegate = self
        ReviewOrderProvider.showsVerticalScrollIndicator = false
        ReviewOrderProvider.showsHorizontalScrollIndicator = false
        ReviewOrderProvider.provider = provider
        if MOLHLanguage.isRTLLanguage() {
            
            ReviewOrderProvider.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func SetupPaymentCard() {
        
        let selectionAction = { (context: BasicProvider<String, PaymentCard>.TapContext) -> Void in
            
        }
        let data  = ["dssd","ds","aa","ds","www","qqq","dssd","ds"]
        
        
        let cellUpdater = { (cell: PaymentCard, data: String, index: Int) in
            // cell.camel = self.homeObj?.offlineAuctions?[index]
            
            if MOLHLanguage.isRTLLanguage() {
                cell.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
        let viewSource = ClosureViewSource(viewUpdater: cellUpdater)
        let sizeSource = { (index: Int, data: String, collectionSize: CGSize) -> CGSize in
            return CGSize(width: collectionSize.width/4  - 10, height: collectionSize.width/4 )
        }
        
        let provider = BasicProvider<String, PaymentCard>(dataSource: ArrayDataSource(data: data),
                                                                  viewSource: viewSource,
                                                                  sizeSource: sizeSource,
                                                                  layout:FlowLayout(lineSpacing: 10).inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)),
                                                                  tapHandler: selectionAction)
        PaymentCardProvider.delegate = self
        PaymentCardProvider.showsVerticalScrollIndicator = false
        PaymentCardProvider.showsHorizontalScrollIndicator = false
        PaymentCardProvider.provider = provider
        if MOLHLanguage.isRTLLanguage() {
            
            PaymentCardProvider.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    
    //MARK: - IBAction

    @IBAction func didTab_Next(_ sender: UIButton) {

    }
    
}

//MARK: - UIScrollViewDelegate

extension MakeOrderVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //    layoutSearchbar()
        
    }
    
    
}
