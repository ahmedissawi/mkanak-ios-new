//
//  PaymentCard.swift
//  Makanik
//
//  Created by Ahmed Issawi on 18/08/2021.
//

import UIKit

class PaymentCard: UIView {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var imgPayment: UIImageView!

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    init() {
        super.init(frame: .zero)
        fromNib()
    }
    
}
