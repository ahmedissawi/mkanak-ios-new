//
//  OrderDeatilesVC.swift
//  Makanik
//
//  Created by Ahmed Issawi on 17/08/2021.
//

import UIKit
import CollectionKit

class OrderDeatilesVC: SuperViewController {
    
    
    // MARK: - IBOutlet

    
    // MARK: Properties
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNav()
    }
    
    
    //MARK: - Factory Functions
    func setupNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Order Deatiles".localized, sender: self, large: false)
        navigation.setCustomBackButtonForViewController(sender: self)
    }
    
    func setupOrder(){
//        let selectionAction = { (context: BasicProvider<String, NewOffer>.TapContext) -> Void in
//
//        }
    }
    
    
    //MARK: - IBAction

    @IBAction func didTab_Next(_ sender: UIButton) {
        let vc = OrderQuantitiesVC.loadFromNib()
        self.navigationController?.pushViewController(vc, animated: true)

    }


   
}
