//
//  NotificationVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit

class NotificationVC: SuperViewController {

    @IBOutlet weak var tableView: UITableView!

    var notificationdata:NotificationData?
    var notificationitems = [NotificationItmes]()
    var currentpage = 1
    var paginate:Paginate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "NotificationCell")
        setupnav()
        getNotification()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.notificationitems.removeAll()
            self.getNotification()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getNotification() // next page
            self.hideIndicator()
        }
    }

    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Notification".localized, sender: self, large: false)
    }

    func getNotification(){
        
        if CurrentUser.typeSelect == "client"{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.getnotificationsUser,nestedParams: "?page=\(currentpage)").start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()

                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(NotificationResponse.self, from: response.data!)
                    self.notificationdata = Status.data
                    self.notificationitems += self.notificationdata!.items ?? []
                    self.paginate = self.notificationdata?.paginate
                    if self.paginate?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.notificationitems.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no Notification to display.".localized
                        
                        self.notificationitems.removeAll()
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    
                    self.tableView.reloadData()
                    
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.getnotificationsProvider,nestedParams: "?page=\(currentpage)").start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()

                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(NotificationResponse.self, from: response.data!)
                    self.notificationdata = Status.data
                    self.notificationitems += self.notificationdata!.items ?? []
                    self.paginate = self.notificationdata?.paginate
                    if self.paginate?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.notificationitems.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no Notification to display.".localized
                        
                        self.notificationitems.removeAll()
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    
                    self.tableView.reloadData()
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }
        
    }
    
    
    @objc func didRefersh(sender: UIButton) {
        self.notificationitems.removeAll()
        self.getNotification()
    }
    
    
    
}


extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationitems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.notification = notificationitems[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
