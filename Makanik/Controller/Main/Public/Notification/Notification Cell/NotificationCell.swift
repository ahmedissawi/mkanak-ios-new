//
//  NotificationCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDate: UILabel!

     
    var notification:NotificationItmes!{
        didSet{
            self.lblTitle.text = self.notification.title ?? ""
            self.lblDesc.text = self.notification.message ?? ""
            self.lblDate.text = self.notification.createdAt ?? ""
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
    
}
