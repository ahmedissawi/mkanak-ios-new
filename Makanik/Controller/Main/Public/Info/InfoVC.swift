//
//  InfoVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 2/9/21.
//

import UIKit

class InfoVC: SuperViewController {
    
    @IBOutlet weak var lblInfo: UILabel!
    
    
    var info:Pages?
    var titleInfo:String?
    var keyInfo:String?
    var isFromRegister = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getInfo()
        setupnav()
    }
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        if isFromRegister{
            navigation.setCustomBackButtonWithdismiss(sender: self)
        }else{
            navigation.setCustomBackButtonForViewController(sender: self)
        }
        navigation.setTitle(titleInfo ?? "", sender: self, large: false)
    }
    
    func getInfo(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.pages,nestedParams: keyInfo ?? "").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(PagesResponse.self, from: response.data!)
                self.info = Status.data
                self.lblInfo.text = self.info?.content?.htmlToString ?? ""
                let navigation = self.navigationController as! CustomNavigationBar
                self.titleInfo = self.info?.name ?? ""
                navigation.setTitle(self.titleInfo ?? "", sender: self, large: false)
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    
}
