//
//  FilterResultVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/30/21.
//

import UIKit

class FilterResultVC: SuperViewController {
    
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet weak var lblsearchResult: UILabel!
    @IBOutlet weak var tableView: UITableView!


    var currentpage = 1
    var ordersproviderdata:OrderProvider?
    var ordersprovideritem  =  [OrderProviderItems]()
    var paginate:Paginate?
    
    
    var parameters: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        setupOrder()
        self.tableView.tableHeaderView = HeaderView
        tableView.registerCell(id: "OrdersCell")
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.ordersprovideritem.removeAll()
            self.setupOrder()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.setupOrder() // next page
            self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
    }
    
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Search".localized, sender: self, large: false)
    }
    
    
    func setupOrder(){
        
        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.orderUser,nestedParams: "?page=\(currentpage)",parameters: parameters).start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()

                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(OrderProviderResponse.self, from: response.data!)
                    self.ordersproviderdata = Status.data
                    self.lblsearchResult.text = "Filter Result:".localized + " " + "\(self.ordersproviderdata?.items?.count ?? 0)"
                    self.ordersprovideritem += self.ordersproviderdata!.items ?? []
                    self.paginate = self.ordersproviderdata?.paginate
                    if self.paginate?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.ordersprovideritem.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no Orders to display.".localized
                        
                        self.ordersprovideritem.removeAll()
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    
                    self.tableView.reloadData()
                    
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
            
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.orderProvider,nestedParams: "?page=\(currentpage)",parameters: parameters).start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(OrderProviderResponse.self, from: response.data!)
                    self.ordersproviderdata = Status.data
                    self.lblsearchResult.text = "Filter Result:".localized + " " + "\(self.ordersproviderdata?.items?.count ?? 0)"
                    self.ordersprovideritem += self.ordersproviderdata!.items ?? []
                    self.paginate = self.ordersproviderdata?.paginate
                    if self.paginate?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.ordersprovideritem.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no Orders to display.".localized
                        
                        self.ordersprovideritem.removeAll()
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    
                    self.tableView.reloadData()
                    
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }
        
        
        
    }
    
    @objc func didRefersh(sender: UIButton) {
        self.ordersprovideritem.removeAll()
        self.setupOrder()
    }
    
    
    


}

extension FilterResultVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ordersprovideritem.count != 0{
            return ordersprovideritem.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! OrdersCell
        cell.order = ordersprovideritem[indexPath.row]
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if CurrentUser.typeSelect == "client"{
            let vc:UserDeatilesVC = UserDeatilesVC.loadFromNib()
            vc.creatiedat = ordersprovideritem[indexPath.row].createdAt ?? ""
            vc.id = ordersprovideritem[indexPath.row].id ?? 0
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            let vc:ProviderDeatilesVC = ProviderDeatilesVC.loadFromNib()
            vc.id = ordersprovideritem[indexPath.row].id ?? 0
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
}
