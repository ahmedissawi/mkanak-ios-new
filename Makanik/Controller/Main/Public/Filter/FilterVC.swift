//
//  FilterVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/30/21.
//

import UIKit
import ActionSheetPicker_3_0

class FilterVC: SuperViewController {
    
    @IBOutlet weak var tfServices: UITextField!
    @IBOutlet weak var tftypeOrder: UITextField!
    @IBOutlet weak var tftypeStatus: UITextField!
    @IBOutlet weak var tffromDate: UITextField!
    @IBOutlet weak var tftoDate: UITextField!

    var type = ""
    var status = ""
    var categories = [Categories]()
    var selectedcategorie:Int?
    var FromDate:Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        getCategories()
    }
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Search".localized, sender: self, large: false)
    }
    
    func getCategories(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.categories).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesResponse.self, from: response.data!)
                if Status.data?.count != 0{
                    self.categories = Status.data ?? []
                }else if Status.data?.count == 0{
                    self.showAlert(title: "Alert!".localized, message: "No Categories Add.".localized)
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    
    
    
    @IBAction func didTab_Services(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Categories".localized, rows: self.categories.compactMap({$0.name}), initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tftypeOrder.text = Value as? String
                self.selectedcategorie = self.categories[value].id ?? 0

            }
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    
    @IBAction func didTab_typeOrder(_ sender: UIButton) {
        
        let type = ["Fixed Price","Offer"]
        ActionSheetStringPicker.show(withTitle: "Type".localized, rows: type, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tftypeOrder.text = Value as? String
            }
            if self.tftypeOrder.text == "Fixed Price"{
                self.type = "s_price"
            }else{
                self.type = "offer"
            }
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    
    @IBAction func didTab_typeStatus(_ sender: UIButton) {
        view.endEditing(true)
        let type = ["pending","accepted","completed","canceled","rejected"]
        ActionSheetStringPicker.show(withTitle: "Type".localized, rows: type, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tftypeStatus.text = Value as? String
                self.status = self.tftypeStatus.text ?? ""

            }
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

        
    }
    
    @IBAction func didTab_FromDate(_ sender: UIButton) {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            self.FromDate = value as? Date
            
            self.tffromDate.text = dateString
            self.tftoDate.text = ""
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.show()
        
    }
    
    @IBAction func didTab_ToDate(_ sender: UIButton) {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tftoDate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.minimumDate = FromDate
        datePicker?.show()
    }
    
    
    
    @IBAction func didTab_Search(_ sender: UIButton) {
        
        
        guard let startdate = self.tffromDate.text, !startdate.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter startdate".localized)
            return
        }
        
        guard let enddate = self.tftoDate.text, !enddate.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter enddate".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        if self.status != ""{
            parameters["status"] =  self.status
        }
        if self.type != ""{
            parameters["type"] =  self.type
        }
        if self.selectedcategorie != nil{
            parameters["service_id"] =  selectedcategorie?.description
        }
        
        parameters["form_date"] =  startdate
        parameters["to_date"] =  enddate

        let vc:FilterResultVC = FilterResultVC.loadFromNib()
        vc.parameters = parameters
        self.navigationController?.pushViewController(vc, animated: true)

        

    }
    
    
    
    
    
    
    
}
