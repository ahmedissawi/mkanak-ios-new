//
//  SettingsVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit
import Firebase
import FirebaseMessaging

class SettingsVC: SuperViewController {
    
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var NotificationSwitch: UISwitch!


    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNav()
        handelSettingsToipc()
        setupLanguage()
    }

    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Settings".localized, sender: self, large: false)
    }
    
    func setupLanguage(){
        if MOLHLanguage.currentAppleLanguage() == "ar"{
            self.lblLanguage.text = "العربية"
        }
        if MOLHLanguage.currentAppleLanguage() == "en"{
            self.lblLanguage.text = "English"
        }
    }
    
    func handelSettingsToipc(){
        if CurrentUser.typeSelect == "client"{
            if CurrentUser.userInfo?.notification == 1{
                self.NotificationSwitch.setOn(true, animated: true)
            }else{
                self.NotificationSwitch.setOn(false, animated: true)
            }
        }else{
            if CurrentProvider.providerInfo?.notification == 1{
                self.NotificationSwitch.setOn(true, animated: true)
            }else{
                self.NotificationSwitch.setOn(false, animated: true)
            }

        }
    }
    
    func updateNotification(isNotification:Int?){
            
        var parameters: [String: Any] = [:]
        parameters["notification"] = isNotification?.description

        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.updatenotificationsUser,parameters: parameters).start(){ (response, error) in
                            
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        print("done notification")
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.updatenotificationsProvider,parameters: parameters).start(){ (response, error) in
                            
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        print("done notification")
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }

    }

    @IBAction func didTab_language(_ sender: Any) {
        
        
        var actions: [(String, UIAlertAction.Style)] = []
        actions.append(("العربية", UIAlertAction.Style.default))
        actions.append(("English", UIAlertAction.Style.default))
        actions.append(("Cancel", UIAlertAction.Style.cancel))
        
        showActionsheet(viewController: self, title: "Alert!".localized, message: "The application will close to change the language settings of the application.".localized, actions: actions) { (index) in
            print("call action \(index)")
            
            switch index{
            case 0:
                self.askForQuit(title: "Alert!!".localized, message:"The application will close to change the language settings of the application.".localized ) { (canQuit) in
                    if canQuit {
                        MOLH.setLanguageTo("ar")
                        MOLH.reset(transition: .transitionCrossDissolve)
                        Helper.quit()
                    }
                }
                
            case 1:
                self.askForQuit(title: "Alert!".localized, message:"The application will close to change the language settings of the application.".localized ) { (canQuit) in
                    if canQuit {
                        MOLH.setLanguageTo("en")
                        MOLH.reset(transition: .transitionCrossDissolve)
                        Helper.quit()
                    }
                }
            default:
                break
            }
            
        }
    }
    
    
    @IBAction func NotificationAction(_ sender: UISwitch) {
        if NotificationSwitch.isOn{
            if CurrentUser.typeSelect == "client"{
                Messaging.messaging().subscribe(toTopic: "users")
                Messaging.messaging().subscribe(toTopic: "user_\(CurrentUser.userInfo?.id ?? 0)")
                CurrentUser.userInfo?.notification = 1
            }else{
                Messaging.messaging().subscribe(toTopic: "mechanics")
                Messaging.messaging().subscribe(toTopic: "mechanic_\(CurrentProvider.providerInfo?.id ?? 0)")
                CurrentProvider.providerInfo?.notification = 1
            }
            self.updateNotification(isNotification: 1)

        }else{
            if CurrentUser.typeSelect == "client"{
                Messaging.messaging().unsubscribe(fromTopic: "users")
                Messaging.messaging().unsubscribe(fromTopic: "user_\(CurrentUser.userInfo?.id ?? 0)")
                CurrentUser.userInfo?.notification = 0
            }else{
                Messaging.messaging().unsubscribe(fromTopic: "mechanics")
                Messaging.messaging().unsubscribe(fromTopic: "mechanic_\(CurrentProvider.providerInfo?.id ?? 0)")
                CurrentProvider.providerInfo?.notification = 0
            }
            self.updateNotification(isNotification: 0)
        }
    }
  

}
