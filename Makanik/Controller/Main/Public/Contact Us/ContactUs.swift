//
//  ContactUs.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit
import IQKeyboardManagerSwift
import FlagPhoneNumber


class ContactUs: SuperViewController {

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var txtContact: IQTextView!
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var tfCodeMobile: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtContact.placeholder = "Text message".localized
        setUpNav()
        setupMobileFileds()
        setupdate()
    }
    
    var dialCode = ""
    var Code = ""
    var MobileVaild = false
    var Mobile = ""
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    func setupdate(){
        if CurrentUser.userInfo != nil{
            if CurrentUser.typeSelect == "client"{
                self.tfName.text = CurrentUser.userInfo?.name ?? ""
                self.tfEmail.text = CurrentUser.userInfo?.email ?? ""
                self.tfMobile.text = "\(CurrentUser.userInfo?.mobile?.dropFirst(4) ?? "")"
                self.tfCodeMobile.text  = CurrentUser.userInfo?.countryCode ?? ""
            }else{
                self.tfName.text = CurrentProvider.providerInfo?.name ?? ""
                self.tfEmail.text = CurrentProvider.providerInfo?.email ?? ""
                self.tfMobile.text = "\(CurrentProvider.providerInfo?.mobile?.dropFirst(4) ?? "")"
                self.tfCodeMobile.text  = CurrentProvider.providerInfo?.countryCode ?? ""
            }
            self.MobileVaild = true
        }
    }

    
    func setupMobileFileds(){
        tfMobile.keyboardType = .asciiCapableNumberPad
        phoneNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneNumberTextField.countryRepository)
        phoneNumberTextField.setFlag(key: .SA)
        dialCode = phoneNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = (phoneNumberTextField.selectedCountry?.code.rawValue)!
        listController.didSelect = { [weak self] country in
            self?.phoneNumberTextField.setFlag(countryCode: country.code)
        }
        phoneNumberTextField.flagLbl = tfCodeMobile
        phoneNumberTextField.delegate = self
        tfCodeMobile.text = phoneNumberTextField.selectedCountry?.phoneCode ?? ""
        tfMobile.addTarget(self, action: #selector(didEditText), for: .editingChanged)
    }
    
    @objc private func didEditText(_ textField: UITextField) {
        phoneNumberTextField.text = textField.text
        phoneNumberTextField.setupMyTEXT()

    }
    
    @IBAction func didTab_MobileCode(_ sender: UIButton) {
        phoneNumberTextField.selectCountries()

    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Contact Us".localized, sender: self, large: false)
    }
    
    
    @IBAction func didTab_Send(_ sender: UIButton) {
        
        guard let name = self.tfName.text, !name.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your name".localized)
            return
        }
        
        guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your mobile number".localized)
            return
        }
        
        if !MobileVaild {
            self.showAlert(title: "Alert!".localized, message: "Mobile Not Vaild".localized)
            
            return
        }
        
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your email".localized)
            
            return
        }
        
        if !email.isValidEamil{
            self.showAlert(title: "Alert!".localized, message: "Email Not Vaild".localized)
            
        }
        
        guard let txt = self.txtContact.text, !txt.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your message".localized)
            return
        }
        
        
        
        var parameters: [String: String] = [:]
        parameters["name"] = name
        parameters["email"] = email
        parameters["mobile"] = "+" + dialCode + mobile
        parameters["message"] = txt

        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.contactus,parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else{
                    
                    CATransaction.begin()
                    self.navigationController?.popViewController(animated: true)
                    CATransaction.setCompletionBlock({ [weak self] in
                        self?.showAlert(title: "", message: "Done successfully".localized)
                    })
                    CATransaction.commit()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
                        
        }
        
    }

}


extension ContactUs: FPNTextFieldDelegate {
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        print("isValid: \(isValid)")
        MobileVaild = isValid
        Mobile = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCode = dialCode.replacingOccurrences(of: "+", with: "")
        self.Code = code
        print(name, dialCode, code)
    }
    
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        listController.title = "Countries".localized
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        
        self.present(navigationViewController, animated: true, completion: nil)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}
