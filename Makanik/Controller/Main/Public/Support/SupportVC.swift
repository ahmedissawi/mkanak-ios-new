//
//  SupportVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/20/21.
//

import UIKit
import IQKeyboardManagerSwift
import ActionSheetPicker_3_0

class SupportVC: SuperViewController {
    
    @IBOutlet weak var txtMessage: IQTextView!
    @IBOutlet weak var tfResones: UITextField!
    
    var resones = [Resones]()
    var idResones:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNav()
        getResones()
    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Technical Support".localized, sender: self, large: false)
    }
    
    func getResones(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.reasons).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseResones.self, from: response.data!)
                self.resones = Status.data ?? []
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    
    @IBAction func didTab_Resones(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Resones".localized, rows: self.resones.map { $0.reason as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.tfResones.text = Value as? String
                                            self.idResones = self.resones[value].id ?? 0
                                        }
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_Send(_ sender: UIButton) {
        
        guard let txt = self.txtMessage.text, !txt.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your message".localized)
            
            return
        }
        guard let resones = self.tfResones.text, !resones.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your resone".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["message"] = txt
        parameters["reason_id"] = idResones?.description
        
        
        if CurrentUser.typeSelect == "client"{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.supportUser,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.supportProvider,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }
        
    }
    
    
}
