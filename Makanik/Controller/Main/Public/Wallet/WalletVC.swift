//
//  WalletVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit

class WalletVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblStatusBalance: UILabel!
    @IBOutlet weak var lblnameStatusBalance: UILabel!
    @IBOutlet weak var viewWithdrawal: UIView!


    var currentpage = 1
    var walletdata:WalletData?
    var walletitem  =  [WalletItems]()
    var paginate:Paginate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        getWallet()
        setupview()
        handelpaginate()
    }
    
    func setupview(){
        tableView.registerCell(id: "WalletCell")
        tableView.tableHeaderView = HeaderView
        if CurrentUser.typeSelect == "client"{
            self.viewWithdrawal.isHidden = true
        }else{
            self.viewWithdrawal.isHidden = false
        }
    }
    
    func handelpaginate(){
        
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.walletitem.removeAll()
            self.getWallet()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getWallet() // next page
            self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
    }

    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Wallet".localized, sender: self, large: false)
    }
    
    
    func getWallet(){
        
        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.userwallet,nestedParams: "?page=\(currentpage)").start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()

                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseWallet.self, from: response.data!)
                    self.walletdata = Status.data
                    self.lblBalance.text = "\(self.walletdata?.availableBalance ?? 0)"
                    if self.walletdata?.outstandingBalance ?? 0 < 0{
                        self.lblStatusBalance.text = "\(self.walletdata?.outstandingBalance ?? 0)"
                        self.lblnameStatusBalance.text = "on you".localized
                    }else{
                        self.lblStatusBalance.text = "\(self.walletdata?.outstandingBalance ?? 0)"
                        self.lblnameStatusBalance.text = "to you".localized
                    }
                    self.walletitem += self.walletdata!.items ?? []
                    self.paginate = self.walletdata?.paginate
                    if self.paginate?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.walletitem.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no data to display.".localized
                        
                        self.walletitem.removeAll()
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    
                    self.tableView.reloadData()
                    
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.providerwallet,nestedParams: "?page=\(currentpage)").start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()

                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseWallet.self, from: response.data!)
                    self.walletdata = Status.data
                    self.lblBalance.text = "\(self.walletdata?.availableBalance ?? 0)"
                    if self.walletdata?.outstandingBalance ?? 0 < 0{
                        self.lblStatusBalance.text = "\(self.walletdata?.outstandingBalance ?? 0)"
                        self.lblnameStatusBalance.text = "on you".localized
                    }else{
                        self.lblStatusBalance.text = "\(self.walletdata?.outstandingBalance ?? 0)"
                        self.lblnameStatusBalance.text = "to you".localized
                    }
                    self.walletitem += self.walletdata!.items ?? []
                    self.paginate = self.walletdata?.paginate
                    if self.paginate?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.walletitem.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no data to display.".localized
                        
                        self.walletitem.removeAll()
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    
                    self.tableView.reloadData()
                    
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }
    }
    
    
    @objc func didRefersh(sender: UIButton) {
        self.walletitem.removeAll()
        self.getWallet()
    }
    

    @IBAction func didTab_Withdrawal(_ sender: UIButton) {
        let vc:DepositWalletVC = DepositWalletVC.loadFromNib()
        vc.typeSend = "Withdrawal"
        navigationController?.pushViewController(vc, animated: true)
    }
 
    @IBAction func didTab_Deposit(_ sender: UIButton) {
        let vc:DepositWalletVC = DepositWalletVC.loadFromNib()
        vc.typeSend = "Deposit"
        navigationController?.pushViewController(vc, animated: true)

    }
 

}

extension WalletVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletitem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
        cell.wallet = walletitem[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc:DepositWalletVC = DepositWalletVC.loadFromNib()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86
    }
}
