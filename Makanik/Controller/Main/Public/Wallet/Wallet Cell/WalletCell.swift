//
//  WalletCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit

class WalletCell: UITableViewCell {
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgBalance: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    var wallet:WalletItems!{
        didSet{
            self.lblPrice.text = "\(wallet.amount ?? 0)" + " "  + "SAR".localized
            self.lblDate.text = "\(wallet.createdAt ?? "")"
            self.lblname.text = wallet.depositType ?? ""
            if wallet.type == "deposit"{
                self.imgBalance.image = UIImage(named: "ic_top")
            }else{
                self.imgBalance.image = UIImage(named: "ic_bottom")
            }

        }
    }
    
 
    
}
