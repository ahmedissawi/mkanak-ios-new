//
//  DepositWalletVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit

class DepositWalletVC: SuperViewController {
    
    @IBOutlet weak var tfammount: UITextField!
    @IBOutlet weak var tfonline: UILabel!
    @IBOutlet weak var tfcash: UILabel!
    @IBOutlet weak var imgcash: UIImageView!
    @IBOutlet weak var imgonline: UIImageView!
    @IBOutlet weak var viewType: UIView!

    @IBOutlet weak var viewCash: UIView!
    @IBOutlet weak var viewonline: UIView!

    
    var type:String?
    var typeSend:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        setupview()
        
    }
    
    func setupview(){
        if typeSend == "Withdrawal"{
            self.viewType.isHidden = true
        }else{
            self.viewType.isHidden = false
        }

        if CurrentSettings.settingsInfo?.cachePay == 1{
            self.viewCash.isHidden = false
        }
        
        if CurrentSettings.settingsInfo?.onlinePay == 1{
            self.viewonline.isHidden = false
        }
        
    }
    
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Deposit to wallet".localized, sender: self, large: false)
    }
    
    @IBAction func didTab_Deposit(_ sender: UIButton) {
        if sender.tag == 1{
            self.imgcash.image = UIImage(named: "ic_unselect")
            self.imgonline.image = UIImage(named: "ic_selectedbutton")
            self.type = "1" // online
        }else{
            self.imgcash.image = UIImage(named: "ic_selectedbutton")
            self.imgonline.image = UIImage(named: "ic_unselect")
            self.type = "2" // cash
        }
    }
    
    @IBAction func didTab_Send(_ sender: UIButton) {
        
        guard let ammount = self.tfammount.text, !ammount.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your amount".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["balance"] =  ammount
        
        if typeSend == "Deposit"{
            parameters["type"] =  self.type
            if CurrentUser.typeSelect == "client"{
                _ = WebRequests.setup(controller: self).prepare(api: APIRouter.depositwalletUser,parameters: parameters).start(){ (response, error) in
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200{
                            self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                            return
                        }else if Status.status == 200{
                            CATransaction.begin()
                            self.navigationController?.popViewController(animated: true)
                            CATransaction.setCompletionBlock({ [weak self] in
                                self?.showAlert(title: "", message: "Done successfully".localized)
                            })
                            CATransaction.commit()

                        }
                        
                    }catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                    
                }
            }else{
                _ = WebRequests.setup(controller: self).prepare(api: APIRouter.depositwalletProvider,parameters: parameters).start(){ (response, error) in
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200{
                            self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                            return
                        }else if Status.status == 200{
                            CATransaction.begin()
                            self.navigationController?.popViewController(animated: true)
                            CATransaction.setCompletionBlock({ [weak self] in
                                self?.showAlert(title: "", message: "Done successfully".localized)
                            })
                            CATransaction.commit()

                        }
                        
                    }catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                    
                }
            }
            
        }else{
            
            if CurrentUser.typeSelect == "provider"{
                _ = WebRequests.setup(controller: self).prepare(api: APIRouter.WithdrawalwalletProvider,parameters: parameters).start(){ (response, error) in
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200{
                            self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                            return
                        }else if Status.status == 200{
                            CATransaction.begin()
                            self.navigationController?.popViewController(animated: true)
                            CATransaction.setCompletionBlock({ [weak self] in
                                self?.showAlert(title: "", message: "Done successfully".localized)
                            })
                            CATransaction.commit()
                        }
                        
                    }catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                }
            }
        }
    }
    
    
}
