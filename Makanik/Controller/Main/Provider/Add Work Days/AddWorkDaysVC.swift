//
//  AddWorkDaysVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21.
//

import UIKit
import ActionSheetPicker_3_0

class AddWorkDaysVC: UIViewController {
    
    
    @IBOutlet weak var tfFromdate: UITextField!
    @IBOutlet weak var tfTodate: UITextField!
    @IBOutlet weak var btAddEdite: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewDays: UIView!

    
    var isFromEdite = false
    var idDay:Int?
    var days = [Days]()
    var daysDeatiles:DaysDeatilesData?
    //var selectedDay:Int?
    var listSelectd = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNav()
        getdays()
        if isFromEdite{
            getWordDays()
            self.viewDays.isHidden = true
            
        }
    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        if isFromEdite{
            navigation.setTitle("Edite a new Day Work".localized, sender: self, large: false)
            self.btAddEdite.setTitle("Edite".localized, for: .normal)
        }else{
            navigation.setTitle("Add a new Day Work".localized, sender: self, large: false)
            self.btAddEdite.setTitle("Add".localized, for: .normal)
        }
        collectionView.registerCell(id: "DaysCell")
        
        
    }
    
    func getWordDays(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.deatilesworkDays,nestedParams: "\(idDay ?? 0)").start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(DaysDeatilesResponse.self, from: response.data!)
                self.daysDeatiles = Status.data
                self.tfTodate.text = self.daysDeatiles?.toTime ?? ""
                self.tfFromdate.text = self.daysDeatiles?.fromTime ?? ""
               // self.selectedDay = self.daysDeatiles?.day
                self.listSelectd.removeAll()
                self.listSelectd.append(self.daysDeatiles?.day ?? 0)
                self.collectionView.reloadData()

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    func getdays(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.days).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(DaysResponse.self, from: response.data!)
                self.days = Status.data ?? []
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    
    @IBAction func didTab_Fromdate(_ sender: UIButton) {
        view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Start At".localized, datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfFromdate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        datePicker?.show()
    }
    
    
    @IBAction func didTab_Todate(_ sender: UIButton) {
        view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "End At".localized, datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfTodate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        datePicker?.show()
    }
    
    @IBAction func didTab_AddEdite(_ sender: UIButton) {
        
        guard let fromdate = self.tfFromdate.text, !fromdate.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your From Date".localized)
            return
        }
        
        
        guard let todate = self.tfTodate.text, !todate.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your To Date".localized)
            return
        }
        
        guard  self.listSelectd.count != 0 else{
            self.showAlert(title: "Alert!".localized, message: "Please choses selected day".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["from_time"] =  fromdate.description
        parameters["to_time"] =  todate.description
        if !isFromEdite{
            var i = 0
            for item in listSelectd{
                parameters["days[\(i)]"] = item
                i = i + 1
            }
        }

        if isFromEdite{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.editeworkDays,nestedParams: "\(idDay ?? 0)",parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                        NotificationCenter.default.post(name: Notification.Name("UpdateWorkDays"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                            
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.addworkDays,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                        NotificationCenter.default.post(name: Notification.Name("UpdateWorkDays"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                            
            }
        }
        
    }
    
    
}

extension AddWorkDaysVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DaysCell", for: indexPath) as! DaysCell
        
        cell.imgselected.image = UIImage(named: "ic_unselectedDay")
        cell.lblday.text = days[indexPath.row].day
        if (listSelectd.contains(days[indexPath.row].num!)){
            cell.imgselected.image = UIImage(named: "ic_selectedDay")
        }else{
            cell.imgselected.image = UIImage(named: "ic_unselectedDay")
        }
//        if selectedDay == days[indexPath.row].num{
//            cell.imgselected.image = UIImage(named: "ic_selectedDay")
//        }else{
//            cell.imgselected.image = UIImage(named: "ic_unselectedDay")
//        }
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = days[indexPath.row]
        if listSelectd.contains(obj.num!){
            let index = listSelectd.firstIndex(of: obj.num!)
            listSelectd.remove(at: index!)
        }else{
            listSelectd.append(obj.num!)
        }
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size - 20 , height: 60)
    }
    
    
    
    
    
    
    
}
