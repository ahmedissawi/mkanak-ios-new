//
//  EditeProfileProviderVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/23/21.
//

import UIKit
import IQKeyboardManagerSwift
import FlagPhoneNumber


class EditeProfileProviderVC: SuperViewController,MapCoordinatesDelgate{
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var tfCodeMobile: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tfType: UITextField!

    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var dialCode = ""
    var Code = ""
    var MobileVaild = false
    var Mobile = ""
    var selectedimage = false
    var imagePicker: ImagePicker!
    var user:ProviderData?
    var type = ""
    var lat:Double?
    var Lon:Double?
    var address:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMobileFileds()
        setupdata()
        setUpNav()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)

    }
    
    
    func SelectedDone(Lat: Double, Lon: Double, address: String) {
        self.lat = Lat
        self.Lon = Lon
        self.address = address
        self.tfAddress.text = address
    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Edite Profile".localized, sender: self, large: false)
    }
    
    func setupdata(){
        self.tfName.text = CurrentProviderProfile.providerInfo?.name ?? ""
        self.tfEmail.text = CurrentProviderProfile.providerInfo?.email ?? ""
        self.tfMobile.text = CurrentProviderProfile.providerInfo?.mobile?.dropFirst(4).description ?? ""
        self.tfCodeMobile.text = CurrentProviderProfile.providerInfo?.countryCode ?? ""
        self.tfType.text = CurrentProviderProfile.providerInfo?.type ?? ""
        self.tfAddress.text = CurrentProviderProfile.providerInfo?.address ?? ""
        self.lat = Double(CurrentProviderProfile.providerInfo?.lat ?? "0.0")
        self.Lon = Double(CurrentProviderProfile.providerInfo?.lng ?? "0.0")
        self.imgProfile.sd_custom(url: CurrentProviderProfile.providerInfo?.image ?? "",placeholderImage: UIImage(named: "ic_uplaodprofile"))
        if CurrentProviderProfile.providerInfo?.image != nil{
            self.selectedimage = true
        }
    }
    
    func setupMobileFileds(){
        tfMobile.keyboardType = .asciiCapableNumberPad
        phoneNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneNumberTextField.countryRepository)
        phoneNumberTextField.setFlag(key: .SA)
        dialCode = phoneNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = phoneNumberTextField.selectedCountry?.code.rawValue ?? ""
        
        listController.didSelect = { [weak self] country in
            self?.phoneNumberTextField.setFlag(countryCode: country.code)
        }
        phoneNumberTextField.flagLbl = tfCodeMobile
        phoneNumberTextField.delegate = self

        tfMobile.text = phoneNumberTextField.selectedCountry?.phoneCode ?? ""
        
    }
    
    @IBAction func didTab_Map(_ sender: UIButton) {
        let vc:MapVC = MapVC.loadFromNib()
        vc.MapDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }


    @IBAction func didTab_Edite(_ sender: UIButton) {
        
        guard let fullName = self.tfName.text, !fullName.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your full name".localized)
            
            return
        }
        
        guard let address = self.tfAddress.text, !address.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your address".localized)
            
            return
        }
        
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your email".localized)
            
            return
        }
        
        if !email.isValidEamil{
            self.showAlert(title: "Alert!".localized, message: "Email Not Vaild".localized)
            
        }
        
        guard let type = self.tfType.text, !type.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter type".localized)
            return
        }
        
        guard  self.selectedimage != false else{
            self.showAlert(title: "Alert!".localized, message: "Please choose your image".localized)
            
            return
        }
        
        var parameters: [String: String] = [:]
        
        parameters["name"] =  fullName
        parameters["email"] =  email
        parameters["type"] =  type
        parameters["address"] =   self.tfAddress.text ?? ""
        parameters["lat"] =    self.lat?.description
        parameters["lng"] = self.Lon?.description

        
        self.showIndicator()
        let imageData = (imgProfile.image) ?? UIImage()
        
        WebRequests.sendPostMultipartRequestWithImgParam(api:APIRouter.updateProfileProvider,parameters: parameters, img: imageData, withName: "image", completion: { (response, error) in
            self.hideIndicator()

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseProvider.self, from: response.data!)
                self.user = Status.data
                CurrentProviderProfile.providerInfo?.name = self.user?.name ?? ""
                CurrentProviderProfile.providerInfo?.address = self.user?.address ?? ""
                CurrentProviderProfile.providerInfo?.email = self.user?.email ?? ""
                CurrentProviderProfile.providerInfo?.image = self.user?.image ?? ""
                CurrentProviderProfile.providerInfo?.lat = "\(self.user?.lat  ?? 0.0)"
                CurrentProviderProfile.providerInfo?.lng = "\(self.user?.lng  ?? 0.0)"
                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: Notification.Name("UpdateProfile"), object: nil)

                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        )        
    }
    

}

extension EditeProfileProviderVC: FPNTextFieldDelegate {
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        print("isValid: \(isValid)")
        MobileVaild = isValid
        Mobile = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCode = dialCode.replacingOccurrences(of: "+", with: "")
        self.Code = code
        print(name, dialCode, code)
    }
    
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        listController.title = "Countries".localized
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        
        self.present(navigationViewController, animated: true, completion: nil)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}


extension EditeProfileProviderVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    func didSelect(image: UIImage?, picker: ImagePicker) {
        if picker == imagePicker{
            self.imgProfile.image = image
            self.selectedimage = true
        }
    }
    
}
