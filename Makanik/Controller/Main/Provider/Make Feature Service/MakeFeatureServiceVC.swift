//
//  MakeFeatureServiceVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/28/21.
//

import UIKit
import ActionSheetPicker_3_0

class MakeFeatureServiceVC: SuperViewController {
    
    
    @IBOutlet weak var tfFromdate: UITextField!
    @IBOutlet weak var tfenddate: UITextField!
    @IBOutlet weak var tfService: UITextField!
    @IBOutlet weak var imgCash: UIImageView!
    @IBOutlet weak var imgOnline: UIImageView!
    @IBOutlet weak var imgWallet: UIImageView!
    @IBOutlet weak var lblprice: UILabel!

    @IBOutlet weak var viewCash: UIView!
    @IBOutlet weak var viewonline: UIView!
    @IBOutlet weak var viewwallet: UIView!


    var myProviderservices = [ServicesData]()
    var selectedServices:Int?
    var type  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getServices()
        setupnav()
        setupview()
        self.lblprice.text = "\(CurrentSettings.settingsInfo?.distinctionPrice ?? 0)"
    }
    
    func setupview(){
        if CurrentSettings.settingsInfo?.cachePay == 1{
            self.viewCash.isHidden = false
        }
        if CurrentSettings.settingsInfo?.onlinePay == 1{
            self.viewonline.isHidden = false
        }
        
        if CurrentSettings.settingsInfo?.walletPay == 1{
            self.viewwallet.isHidden = false
        }
    
        
    }
    
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Make Feature Service".localized, sender: self, large: false)
    }
    
    
    func getServices(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showservicesProvider).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseServices.self, from: response.data!)
                self.myProviderservices = Status.data ?? []
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    @IBAction func didTab_Fromdate(_ sender: UIButton) {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfFromdate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.show()
    }
    
    
    @IBAction func didTab_Todate(_ sender: UIButton) {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfenddate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.show()
    }
    
    
    @IBAction func didTab_Services(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Services".localized, rows: self.myProviderservices.map { $0.service?.name as Any }
                , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfService.text = Value as? String
                    self.selectedServices = self.myProviderservices[value].id ?? 0
                }
                return
                }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func didTab_Payment(_ sender: UIButton) {

        switch sender.tag {
        case 1:
            self.imgCash.image = UIImage(named: "ic_selectedbutton")
            self.imgWallet.image = UIImage(named: "ic_unselect")
            self.imgOnline.image = UIImage(named: "ic_unselect")
            self.type = "cash"//cash
        case 2:
            self.imgCash.image = UIImage(named: "ic_unselect")
            self.imgWallet.image = UIImage(named: "ic_selectedbutton")
            self.imgOnline.image = UIImage(named: "ic_unselect")
            self.type = "wallet"//wallet
        case 3:
            self.imgCash.image = UIImage(named: "ic_unselect")
            self.imgWallet.image = UIImage(named: "ic_unselect")
            self.imgOnline.image = UIImage(named: "ic_selectedbutton")
            self.type = "online"// online
        default:
            print("no type")
        }

    }
    
    @IBAction func didTab_Add(_ sender: UIButton) {

        
        guard let startdate = self.tfFromdate.text, !startdate.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter startdate".localized)
            return
        }
        
        guard let enddate = self.tfenddate.text, !enddate.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter enddate".localized)
            return
        }
        
        
        guard self.selectedServices != nil else{
            self.showAlert(title: "Alert!".localized, message: "Please select a service".localized)
            return
        }
        
        
        
        guard self.type != "" else{
            self.showAlert(title: "Alert!".localized, message: "Please choose a payment method".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        parameters["mechanic_service_id"] =  self.selectedServices?.description
        parameters["start_date"] =  startdate
        parameters["end_date"] =  enddate
        parameters["pay_type"] =  self.type

        
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.requestSpeacialService,parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    CATransaction.begin()
                    self.navigationController?.popViewController(animated: true)
                    CATransaction.setCompletionBlock({ [weak self] in
                        self?.showAlert(title: "", message: "Done successfully".localized)
                    })
                    CATransaction.commit()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        

    }
    
    
    
}
