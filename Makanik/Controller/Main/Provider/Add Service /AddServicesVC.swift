//
//  AddServicesVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21.
//

import UIKit
import ActionSheetPicker_3_0


class AddServicesVC: SuperViewController {
    
    @IBOutlet weak var tfCategories: UITextField!
    @IBOutlet weak var tfSubCategories: UITextField!
    @IBOutlet weak var tfServices: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var btAddEdite: UIButton!
    @IBOutlet weak var viewSubCategory: UIView!
    @IBOutlet weak var viewService: UIView!
    @IBOutlet weak var imgboth: UIImageView!
    @IBOutlet weak var imgfixedPrice: UIImageView!
    @IBOutlet weak var imgoffers: UIImageView!
    @IBOutlet weak var viewPrice: UIView!


   var categories = [Categories]()
   var subCategory = [subCategoryData]()
   var services = [subCategoryData]()
   var selectedcategorie:Int?
   var selectedSubcategorie:Int?
   var selectedServices:Int?
    
    var servicesdeatiles:ServicesDeatilesData?

    
   var isFromEdite = false
   var type = ""

    var idService:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategories()
        setUpNav()
        if isFromEdite{
            getserviceDeatiles()
        }
    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        if isFromEdite{
            navigation.setTitle("Edite Service".localized, sender: self, large: false)
            self.btAddEdite.setTitle("Edite Service".localized, for: .normal)
        }else{
            navigation.setTitle("Add Service".localized, sender: self, large: false)
            self.btAddEdite.setTitle("Add Service".localized, for: .normal)
        }
    }
    
    
    func getserviceDeatiles(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.shwoservicesProvider,nestedParams: "\(idService ?? 0)").start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ServicesDeatilesResponse.self, from: response.data!)
                self.servicesdeatiles = Status.data
                self.viewSubCategory.isHidden = false
                self.viewService.isHidden = false
                self.tfPrice.text = "\(self.servicesdeatiles?.price ?? 0)"
                
                self.tfServices.text = "\(self.servicesdeatiles?.service?.name ?? "")"
                self.selectedServices = self.servicesdeatiles?.service?.id ?? 0
                
                self.tfCategories.text = "\(self.servicesdeatiles?.service?.subCategory?.category?.name ?? "")"
                self.selectedcategorie = self.servicesdeatiles?.service?.subCategory?.category?.id ?? 0

                self.tfSubCategories.text = "\(self.servicesdeatiles?.service?.subCategory?.name ?? "")"
                self.selectedSubcategorie = self.servicesdeatiles?.service?.subCategory?.id ?? 0

                if self.servicesdeatiles?.type == "1"{
                    self.imgfixedPrice.image = UIImage(named: "ic_selectedbutton")
                    self.type = "1"//fixedprice
                }
                if self.servicesdeatiles?.type == "2"{
                    self.imgoffers.image = UIImage(named: "ic_selectedbutton")
                    self.type = "2"//offer
                }
                if self.servicesdeatiles?.type == "3"{
                    self.imgboth.image = UIImage(named: "ic_selectedbutton")
                    self.type = "3"//both
                }

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    func getCategories(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.categories).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesResponse.self, from: response.data!)
                if Status.data?.count != 0{
                    self.categories = Status.data ?? []
                }else if Status.data?.count == 0{
                    self.showAlert(title: "Alert!".localized, message: "No Categories Add.".localized)
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    
    func getsubCategories(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.subCategories,nestedParams: "\(selectedcategorie ?? 0)").start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(subCategoryResponse.self, from: response.data!)
                if Status.data?.count != 0{
                    self.subCategory = Status.data ?? []
                    self.viewSubCategory.isHidden = false
                }else if Status.data?.count == 0{
                    self.showAlert(title: "Alert!".localized, message: "No Sub Categories Add.".localized)
                }

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    func getServices(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.subCategoryUser,nestedParams: "\(selectedSubcategorie ?? 0)").start(){ (response, error) in
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponsesubCategory.self, from: response.data!)
                if Status.data?.count != 0{
                    self.services = Status.data ?? []
                    self.viewService.isHidden = false
                }else if Status.data?.count == 0{
                    self.showAlert(title: "Alert!".localized, message: "No Sub Services Add.".localized)
                }
                                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
           
        }
        
    }

    
    @IBAction func didTab_Categories(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Categories".localized, rows: self.categories.map { $0.name as Any }
                , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfCategories.text = Value as? String
                    self.selectedcategorie = self.categories[value].id ?? 0
                    self.getsubCategories()
                }
                    self.tfSubCategories.text = ""
                    self.selectedSubcategorie = nil
                    self.viewService.isHidden = true
                return
                }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    @IBAction func didTab_SubCategories(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "SubCategories".localized, rows: self.subCategory.map { $0.name as Any }
                , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfSubCategories.text = Value as? String
                    self.selectedSubcategorie = self.subCategory[value].id ?? 0
                    self.getServices()
                }
                    self.tfServices.text = ""
                    self.selectedServices = nil
                return
                }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    
    @IBAction func didTab_Services(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Services".localized, rows: self.services.map { $0.name as Any }
                , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfServices.text = Value as? String
                    self.selectedServices = self.services[value].id ?? 0
                }
                return
                }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }

    @IBAction func didTab_TypeServices(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            self.imgfixedPrice.image = UIImage(named: "ic_selectedbutton")
            self.imgoffers.image = UIImage(named: "ic_unselect")
            self.imgboth.image = UIImage(named: "ic_unselect")
            self.viewPrice.isHidden = false
            self.type = "1"//fixedprice
        case 2:
            self.imgfixedPrice.image = UIImage(named: "ic_unselect")
            self.imgoffers.image = UIImage(named: "ic_selectedbutton")
            self.imgboth.image = UIImage(named: "ic_unselect")
            self.viewPrice.isHidden = true
            self.type = "2"//offer
        case 3:
            self.imgfixedPrice.image = UIImage(named: "ic_unselect")
            self.imgoffers.image = UIImage(named: "ic_unselect")
            self.imgboth.image = UIImage(named: "ic_selectedbutton")
            self.viewPrice.isHidden = false
            self.type = "3"// both
        default:
            print("no type")
        }
    }
    
    
    
    @IBAction func didTab_AddEdite(_ sender: UIButton) {
        var parameters: [String: Any] = [:]

        
        guard  self.selectedServices != nil else{
            self.showAlert(title: "Alert!".localized, message: "Please select a service".localized)
            return
        }
        
        guard  self.type != "" else{
            self.showAlert(title: "Alert!".localized, message: "Please choose the type of service ".localized)
            return
        }
        
        if type == "1" || type == "3"{
            guard let price = self.tfPrice.text, !price.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your price".localized)
                return
            }
            parameters["price"] =  price
        }

        
        parameters["service_id"] =  self.selectedServices?.description
        parameters["type"] =  self.type

        if isFromEdite{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.editeservicesProvider,nestedParams: "\(idService ?? 0)",parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                        NotificationCenter.default.post(name: Notification.Name("UpdateService"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            
            
            
            
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.addservicesProvider,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                        NotificationCenter.default.post(name: Notification.Name("UpdateService"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            
        }
 
    }

    


}
