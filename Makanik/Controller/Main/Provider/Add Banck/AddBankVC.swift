//
//  AddBankVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/26/21.
//

import UIKit
import ActionSheetPicker_3_0


class AddBankVC: SuperViewController {
    
    
    @IBOutlet weak var tfNamebank: UITextField!
    @IBOutlet weak var tfMatchData: UITextField!
    @IBOutlet weak var tfNameNumber: UITextField!
    @IBOutlet weak var tfAccountNumber: UITextField!
    @IBOutlet weak var btAddEdite: UIButton!
    
    
    var banksdata = [BankData]()
    var bankdeatiles:BankDeatiles?
    var idbankName:Int?
    var isFromEdite = false
    var idBank:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNav()
        getBanks()
        if isFromEdite{
            getBanksDeatiles()
            self.btAddEdite.setTitle("Edite".localized, for: .normal)
        }else{
            self.btAddEdite.setTitle("Add".localized, for: .normal)
        }
    }
    
    
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        if isFromEdite{
            navigation.setTitle("Edite bank account".localized, sender: self, large: false)
        }else{
            navigation.setTitle("Add a new bank account".localized, sender: self, large: false)
        }
    }
    
    
    func getBanksDeatiles(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.getbanksDeatiles,nestedParams: "\(idBank ?? 0)").start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(BankDeatilesResponse.self, from: response.data!)
                self.bankdeatiles = Status.data
                self.idbankName = self.bankdeatiles?.bank?.id ?? 0
                self.tfNamebank.text = self.bankdeatiles?.bank?.name ?? ""
                self.tfMatchData.text = self.bankdeatiles?.ownerName ?? ""
                self.tfNameNumber.text = self.bankdeatiles?.iBan ?? ""
                self.tfAccountNumber.text = self.bankdeatiles?.accountNumber ?? ""
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    func getBanks(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.banks).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseBank.self, from: response.data!)
                self.banksdata = Status.data ?? []
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
        
    }
    
    @IBAction func didTab_Banks(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Banks".localized, rows: self.banksdata.map { $0.name as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.tfNamebank.text = Value as? String
                                            self.idbankName = self.banksdata[value].id ?? 0
                                        }
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    @IBAction func didTab_Add(_ sender: UIButton) {
        
        guard let namebank = self.tfNamebank.text, !namebank.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your name bank".localized)
            return
        }
        
        guard let mtchData = self.tfMatchData.text, !mtchData.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your MatchData".localized)
            return
        }
        guard let nameNumber = self.tfNameNumber.text, !nameNumber.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your name number".localized)
            return
        }
        guard let accountnumber = self.tfAccountNumber.text, !accountnumber.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your Account Number".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["bank_id"] =  idbankName?.description
        parameters["owner_name"] =  mtchData
        parameters["i_ban"] =  nameNumber
        parameters["account_number"] =  accountnumber
        
        
        if isFromEdite{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.editebanks,nestedParams: "\(idBank ?? 0)",parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                        NotificationCenter.default.post(name: Notification.Name("UpdateBank"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.addBanks,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        CATransaction.begin()
                        self.navigationController?.popViewController(animated: true)
                        CATransaction.setCompletionBlock({ [weak self] in
                            self?.showAlert(title: "", message: "Done successfully".localized)
                        })
                        CATransaction.commit()
                        NotificationCenter.default.post(name: Notification.Name("UpdateBank"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
            
        }
        
        
        
    }
    
    
}
