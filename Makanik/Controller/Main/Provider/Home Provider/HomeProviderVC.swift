//
//  HomeProviderVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/17/21.
//

import UIKit

class HomeProviderVC: SuperViewController {
    
    
    @IBOutlet weak var tableViewCurrent: UITableView!
    @IBOutlet weak var tableViewcompleted: UITableView!
    @IBOutlet weak var HeighttableViewcompleted: NSLayoutConstraint!
    @IBOutlet weak var HeighttableViewCurrent: NSLayoutConstraint!
    @IBOutlet weak var lblcompletedorder: UILabel!
    @IBOutlet weak var lblcurrentorder: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    
    var homeData:HomeProviderData?
    var homecurrentorder = [HomeProviderCurrentOrder]()
    var homecompletedorder = [HomeProviderCompletedOrder]()

    var nameprovider = CurrentProviderProfile.providerInfo?.name ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        getHome()
        tableViewCurrent.registerCell(id: "OrdersCell")
        tableViewcompleted.registerCell(id: "OrdersCell")

        self.tableViewcompleted.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.tableViewCurrent.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        UserDefaults.standard.removeObject(forKey: "isNotification")
        self.scrollView.es.addPullToRefresh {
            self.getHome()
            self.hideIndicator()
        }
        
    }
    
    // refrech order
    override func viewWillAppear(_ animated: Bool) {
        self.getHome()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.homecurrentorder.count != 0{
            HeighttableViewCurrent.constant = tableViewCurrent.contentSize.height
        }
        if self.homecompletedorder.count != 0{
            HeighttableViewcompleted.constant = tableViewcompleted.contentSize.height
        }
        
    }
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle(nameprovider, sender: self, large: false)
    }
    
    func getHome(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.homeProvider).start(){ (response, error) in
            
            self.scrollView.es.stopLoadingMore()
            self.scrollView.es.stopPullToRefresh()

            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(HomeProviderResponse.self, from: response.data!)
                self.homeData = Status.data
                self.lblcurrentorder.text = "\(self.homeData?.currentOrderCount ?? 0)"
                self.lblcompletedorder.text = "\(self.homeData?.completedOrderCount ?? 0)"
                self.homecurrentorder = self.homeData?.currentOrder ?? []
                self.homecompletedorder = self.homeData?.completedOrder ?? []
                
                                
                if self.homecurrentorder.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableViewCurrent, refershSelector: #selector(self.didRefersh))

                    self.emptyView?.firstLabel.text = "There are no Orders to display.".localized
                    
                    self.tableViewCurrent.reloadData()
                }else{
                    self.tableViewCurrent.tableFooterView = nil
                }
                
                if self.homecompletedorder.count == 0{
                    self.emptyView2 = self.showEmptyView(emptyView: self.emptyView2, parentView: self.tableViewcompleted, refershSelector: #selector(self.didRefersh))
                    self.emptyView2?.firstLabel.text = "There are no Orders to display.".localized

                    self.tableViewcompleted.reloadData()

                }else{
                    self.tableViewcompleted.tableFooterView = nil
                }
           
                
                self.tableViewCurrent.reloadData()
                self.tableViewcompleted.reloadData()
                
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    
    @objc func didRefersh(sender: UIButton) {
        self.getHome()
    }
    
    
    @IBAction func didTab_More(_ sender: UIButton) {
        if sender.tag == 1{
            let vc:StatusOrdersVC = StatusOrdersVC.loadFromNib()
            vc.status = "accepted"
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc:StatusOrdersVC = StatusOrdersVC.loadFromNib()
            vc.status = "completed"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
}


extension HomeProviderVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == tableViewCurrent{
            if homecurrentorder.count != 0{
                return homecurrentorder.count
            }
        }else{
            if homecompletedorder.count != 0{
                return homecompletedorder.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! OrdersCell
        if tableView == tableViewCurrent{
            cell.currentorder = homecurrentorder[indexPath.row]
        }else{
            cell.completedorder = homecompletedorder[indexPath.row]
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewCurrent{
            let vc:ProviderDeatilesVC = ProviderDeatilesVC.loadFromNib()
            vc.id = homecurrentorder[indexPath.row].id ?? 0
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc:ProviderDeatilesVC = ProviderDeatilesVC.loadFromNib()
            vc.id = homecompletedorder[indexPath.row].id ?? 0
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
}
