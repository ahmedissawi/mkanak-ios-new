//
//  ProviderDeatilesVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/22/21.
//

import UIKit
import Cosmos
import IQKeyboardManagerSwift
import SKPhotoBrowser
import GoogleMaps
import GooglePlaces

class ProviderDeatilesVC: SuperViewController,DeatilesOrderDelgate,GMSMapViewDelegate{
    
    //update order deatiles after add or edite offer if type is [offer]
    func SelectedDone() {
        self.getOrderDeatiles()
    }
    
    @IBOutlet weak var lbldescrbtion: UILabel!
    @IBOutlet weak var lblAddressName: UILabel!
    @IBOutlet weak var lblservices: UILabel!
    @IBOutlet weak var lblnameuser: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStartWork: UILabel!
    @IBOutlet weak var lblpriceoffer: UILabel!
    @IBOutlet weak var lbldescriptionoffer: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNamePrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblStatusOrder: UILabel!
    @IBOutlet weak var imgProvider: UIImageView!
    @IBOutlet weak var imgMap: UIImageView!
    @IBOutlet weak var viewGallery: UIView!
    
    @IBOutlet weak var StackviewComments: UIStackView!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var viewCommentsRate: CosmosView!
    @IBOutlet weak var viewCommentsDescrebtion: UIView!
    @IBOutlet weak var txtcomment: IQTextView!
    @IBOutlet weak var stackOrderStatus: UIStackView!
    @IBOutlet weak var stacklblOrderStatus: UIStackView!
    
    @IBOutlet weak var btStatus: UIButton!
    @IBOutlet weak var btCancel: UIButton!
    
    
    @IBOutlet weak var viewRatingDeatiles: UIView!
    @IBOutlet weak var RatingDeatiles: CosmosView!
    @IBOutlet weak var lblDeatilesDescrebtion: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewStatus: UIView!
    
    @IBOutlet weak var viewMyoffer: UIView!
    
    @IBOutlet weak var mapView: GMSMapView!

    
    var orderDeatiles:OrderDeatilesUserData?
    var id = 0
    var offers = [Offers]()
    var galleyitems = [Images]()
    var images = [SKPhoto]()
    var typestatus = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrderDeatiles()
        setupnav()
        setupview()
        handeltable()
        setRate()
    }
    
    func setupview(){
        
        //hide Form Rate & and Rate Deatiles to show if status is complated
        self.viewComment.isHidden = true
        self.viewRatingDeatiles.isHidden = true
        
        self.viewMyoffer.isHidden = true
        self.viewStatus.isHidden  = true
        
        collectionView.registerCell(id: "GalleryCell")
        self.txtcomment.placeholder = "Leave a comment for client".localized
    }
    
    func handeltable(){
        self.scrollView.es.addPullToRefresh {
            guard NetworkManager.isConnectedToNetwork() else {
                self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            self.getOrderDeatiles()
            self.hideIndicator()
        }
        
    }
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Deatiles Order".localized, sender: self, large: false)
    }
    
    func setRate(){
        viewCommentsRate.didTouchCosmos = { rating in
            if rating <= 3{
                self.viewCommentsDescrebtion.showAnimated(in: self.StackviewComments)
            }else{
                self.viewCommentsDescrebtion.hideAnimated(in: self.StackviewComments)
            }
        }
        
    }
    
    func getOrderDeatiles(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showorderProvider,nestedParams: "\(id)").start(){ (response, error) in
            
            self.scrollView.es.stopLoadingMore()
            self.scrollView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(OrderDeatilesUserResponse.self, from: response.data!)
                self.orderDeatiles = Status.data
                self.lblservices.text = self.orderDeatiles?.service ?? ""
                self.typestatus = self.orderDeatiles?.status ?? ""
                self.lblTime.text = self.orderDeatiles?.createdAt ?? ""
                self.lbldescrbtion.text = self.orderDeatiles?.descriptionValue ?? ""
                if self.orderDeatiles?.priority == "current"{
                    self.lblStartWork.text = "current priority".localized
                }else{
                    self.lblStartWork.text = "later priority".localized
                }
                self.imgProvider.sd_custom(url: self.orderDeatiles?.mechanic?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                self.lblAddressName.text = self.orderDeatiles?.address ?? ""
                self.lblnameuser.text = self.orderDeatiles?.user?.name ?? ""
                
                //show images
                if self.orderDeatiles?.images?.count != 0{
                    self.galleyitems = self.orderDeatiles?.images ?? []
                    for item in self.galleyitems{
                        let photo = SKPhoto.photoWithImageURL(item.image!)
                        photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                        self.images.append(photo)
                    }
                }else{
                    self.viewGallery.isHidden = true
                }
                self.collectionView.reloadData()
                
                //type status
                //pending
                //accepted
                //completed
                //reject
                
                //  Handel Offer and fixedPrice show
                if self.orderDeatiles?.type == "offer"{
                    self.lblType.text = "Offer price".localized
                    self.lblNamePrice.text = "Lowest Price".localized
                    
                    // check if type == offer & status == pending
                    if self.orderDeatiles?.status == "pending"{
                        self.viewStatus.isHidden  = false
                        self.btCancel.isHidden = true
                        self.btStatus.setTitle("Send Offer".localized, for: .normal)
                    }
                    
                    //                    // check if my offer is not nill to change status title button
                    //                    if self.orderDeatiles?.myOffer  != nil{
                    //                        self.btStatus.setTitle("Edite Offer".localized, for: .normal)
                    //                    }
                    
                    // check if type == offer & status == accepted
                    if self.orderDeatiles?.status == "accepted"{
                        if self.orderDeatiles?.myOffer != nil{
                            self.btStatus.setTitle("Edite Offer".localized, for: .normal)
                            self.lblpriceoffer.text =  "\(self.orderDeatiles?.myOffer?.price ?? 0)" + " "  + "SAR".localized
                            self.lbldescriptionoffer.text =  self.orderDeatiles?.myOffer?.descriptionValue ?? ""
                            self.viewMyoffer.isHidden = false
                        }
                    }
                    
                    // check if minoffer == nil show no price else show minoffer
                    if self.orderDeatiles?.minOffer == 0.0 || self.orderDeatiles?.minOffer == 0 || self.orderDeatiles?.minOffer == nil {
                        self.lblPrice.text = "No Price".localized
                    }else{
                        self.lblPrice.text = "\(self.orderDeatiles?.minOffer ?? 0.0)" + " " +  "SAR".localized
                    }
                }else{
                    self.lblType.text = "Fixed price".localized
                    self.lblPrice.text = "\(self.orderDeatiles?.price ?? "0.0")" + " " + "SAR".localized
                    self.lblNamePrice.text = "Price".localized
                    
                    // check if type == Fixed price & status == pending
                    if self.orderDeatiles?.status == "pending"{
                        self.viewStatus.isHidden  = false
                        self.btCancel.isHidden = false
                        self.btStatus.setTitle("Accept Order".localized, for: .normal)
                    }
                    
                    // check if type == Fixed price & status == accepted
                    if self.orderDeatiles?.status == "accepted"{
                        self.viewStatus.isHidden  = false
                        self.btCancel.isHidden = true
                        self.btStatus.setTitle("Complate Order".localized, for: .normal)
                    }
                }
                
                /// - Note: for Offer & Fixed Price
                //Edite price after order is completed
                if self.orderDeatiles?.status == "completed"{
                    self.viewStatus.isHidden = true
                    self.lblPrice.text =  "\(self.orderDeatiles?.price ?? "0.0")" + " " +  "SAR".localized
                    self.lblNamePrice.text = "price".localized
                    /// - Note :  this All show if status order is completed
                    // show rating deatiles if rating data != nil
                    if self.orderDeatiles?.rating != nil{
                        self.viewRatingDeatiles.isHidden = false
                        self.lblDeatilesDescrebtion.text = self.orderDeatiles?.rating?.comment ?? ""
                        self.RatingDeatiles.rating = Double(self.orderDeatiles?.rating?.rating ?? 0)
                    }else{
                        self.viewRatingDeatiles.isHidden = true
                    }
                    
                    // if isRating == false this meain provider is not submit rate for clinet
                    if self.orderDeatiles?.isRating == false{
                        self.viewComment.isHidden = false
                    }else{
                        self.viewComment.isHidden = true
                    }
                }
              
                
                //show Map
                let lat  = Double(self.orderDeatiles?.lat ?? 0.0)
                let lng  = Double(self.orderDeatiles?.lng ?? 0.0)
                
//                let testurlStr = "http://maps.google.com/maps/api/staticmap?markers=color:red|\(lat ?? "0.0"),\(lng ?? "0.0")&zoom=10&size=400x300&sensor=true&key=AIzaSyAgLfq54h5DuzBwBYYg0V5QkVXq4m36yYQ"
//                let components = Helper.transformURLString(testurlStr)
//
//                if let url = components?.url {
//                    self.imgMap.sd_setImage(with:url)
//                    print(url)
//                    print("valid")
//                } else {
//                    print("invalid")
//                }
//
                self.mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16)
                
                let myLocationCamera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 14)
                self.mapView.animate(to: myLocationCamera)
                let position = CLLocationCoordinate2DMake(lat,lng)
                let marker = GMSMarker(position: position)
                marker.map = self.mapView

                self.mapView.delegate = self
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        
    }
    @IBAction func didTab_Rate(_ sender: UIButton) {
        
        if self.viewCommentsRate.rating <= 3{
            guard let txtrate = self.txtcomment.text, !txtrate.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your comment".localized)
                return
            }
        }
        
        if self.viewCommentsRate.rating <= 3{
            guard self.viewCommentsRate.rating != 0.0 else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your comment".localized)
                return
            }
        }
        
        var parameters: [String: Any] = [:]
        parameters["order_id"] =  self.orderDeatiles?.id ?? 0
        parameters["rating"] = "\(self.viewCommentsRate.rating)"
        parameters["comment"] =  self.txtcomment.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.rateMechanic,parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    self.showAlert(title: "", message: Status.message ?? "")
                    self.txtcomment.text = ""
                    self.getOrderDeatiles()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    func makeAcceptOrder(){
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.makeAcceptOrder,nestedParams: "\(self.id)/accept").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    self.getOrderDeatiles()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    func makeCompalteOrder(){
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.makeComplateOrder,nestedParams: "\(self.id)/complete").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    self.getOrderDeatiles()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    func makeRejectedOrder(){
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.makeComplateOrder,nestedParams: "\(self.id)/reject").start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    self.viewStatus.isHidden = true
                    self.getOrderDeatiles()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    func makeOfferOrder(){
        let vc:AddOfferVC = AddOfferVC.loadFromNib()
        vc.deatilesOrderDelegate = self
        vc.offer = self.orderDeatiles?.myOffer
        vc.myOfferid = self.orderDeatiles?.myOffer?.id ?? 0
        vc.orderid = self.orderDeatiles?.id ?? 0
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false)
    }
    
    
    
    @IBAction func didTab_orderStatus(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            // check if type == offer or fixedprice
            if self.orderDeatiles?.type == "offer"{
                if self.typestatus == "pending"{
                    self.makeOfferOrder()
                }else if self.typestatus == "accepted"{

                }
            }else{
                if self.typestatus == "pending"{
                    self.makeAcceptOrder()
                }else if self.typestatus == "accepted"{
                    self.makeCompalteOrder()
                }
            }
        case 2:
            self.makeRejectedOrder()
        default:
            print("no button")
        }
        
    }
    
    
}

extension ProviderDeatilesVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if galleyitems.count != 0{
            return galleyitems.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        cell.imggallery = galleyitems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(indexPath.row)
        SKPhotoBrowserOptions.displayAction = false    // action button will be hidden
        self.present(browser, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 110)
    }
    
    
    
}
