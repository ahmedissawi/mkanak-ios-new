//
//  GalleryCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/22/21.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var btClose:UIButton!

    
    var Close: (() -> ())?

    
    var imggallery:Images!{
        didSet{
            self.imgGallery.sd_custom(url: imggallery.image ?? "")
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func btClose(_ sender: UIButton) {
        Close?()
     }


}
