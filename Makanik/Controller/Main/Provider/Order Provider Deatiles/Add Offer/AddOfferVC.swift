//
//  AddOfferVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/30/21.
//

import UIKit
import IQKeyboardManagerSwift

protocol DeatilesOrderDelgate : class {
    
    func SelectedDone()
}

class AddOfferVC: SuperViewController {

    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var txtDescription: IQTextView!
    @IBOutlet weak var btAddEdite: UIButton!


    var offer:MyOffer?
    var deatilesOrderDelegate:DeatilesOrderDelgate?
    var orderid:Int?
    var myOfferid:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupview()
    }
    
    func setupview(){
        self.txtDescription.placeholder = "Description".localized
        if offer != nil{
            setupdata()
            if offer?.canEdit == true{
                self.btAddEdite.isHidden = false

            }else{
                self.btAddEdite.isHidden = true
            }
         
        }else{
            self.btAddEdite.setTitle("Send".localized, for: .normal)
        }
    }
    
    func setupdata(){
        self.tfPrice.text = "\(self.offer?.price ?? 0)"
        self.txtDescription.text = "\(self.offer?.descriptionValue ?? "")"
    }

    @IBAction func didTab_close(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func didTab_send(_ sender: UIButton) {
        
        guard let price = self.tfPrice.text, !price.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your price".localized)
            return
        }
        
        let description = self.txtDescription.text ?? ""
        
        
        var parameters: [String: Any] = [:]
        parameters["description"] = description
        parameters["price"] = price
        if offer  == nil{
            parameters["order_id"] = orderid?.description

            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.makeAddofferOrder,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            self.deatilesOrderDelegate?.SelectedDone()
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }else{
            
            parameters["offer_id"] = offer?.id

            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.makeediteofferOrder,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            self.deatilesOrderDelegate?.SelectedDone()
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }

        
        
    }
  

}
