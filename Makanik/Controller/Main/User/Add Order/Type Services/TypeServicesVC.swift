//
//  TypeServicesVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21.
//

import UIKit

class TypeServicesVC: SuperViewController {
    
    @IBOutlet weak var lbltitle: UILabel!
    
    var titlecatogray:String?
    var type:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        let titleServices = UserDefaults.standard.string(forKey: "servicename")
        self.lbltitle.text  =  titleServices
    }
    
    
    @IBAction func didTab_close(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
  

    @IBAction func didTab_type(_ sender: UIButton) {
        if sender.tag == 1{
            let navVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc = MapVC.loadFromNib()
            vc.isFromAddService = true
            UserDefaults.standard.set("2", forKey: "type") // 2 == offer
            navVC.viewControllers = [vc]
            navVC.modalPresentationStyle = .fullScreen
            self.present(navVC, animated: true, completion: nil)
        }else{
            let navVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            let vc = FixedPriceVC.loadFromNib()
            UserDefaults.standard.set("1", forKey: "type") // 2 == fixedprice
            navVC.viewControllers = [vc]
            navVC.modalPresentationStyle = .fullScreen
            self.present(navVC, animated: true, completion: nil)
        }
        
    }



}
