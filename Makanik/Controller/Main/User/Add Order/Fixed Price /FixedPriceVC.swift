//
//  FixedPriceVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21.
//

import UIKit

class FixedPriceVC: SuperViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblservice: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    var selectServic = -99

    var providerdata = [ProvidersByServiceData]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "FixedPriceCell")
        setupnav()
        getProviders()
        self.tableView.tableHeaderView = headerView
        let title = UserDefaults.standard.string(forKey: "servicename")
        self.lblservice.text = title
        self.tableView.es.addPullToRefresh {
            self.getProviders()
            self.hideIndicator()
        }
        btnNext.isEnabled = false
        btnNext.backgroundColor = "999999".color
    }
    
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonWithdismiss(sender: self,isWhite: true)
        navigation.setTitle("Fixed Price".localized, sender: self, large: false)
    }
    
    func getProviders(){
        
        let id = UserDefaults.standard.integer(forKey: "serviceID")
      //  let type = UserDefaults.standard.string(forKey: "type")
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.mechanicservicesByType,nestedParams: "?service_id=\(id)").start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()

            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseProvidersByService.self, from: response.data!)
                if Status.data?.count != 0{
                    self.providerdata = Status.data ?? []
                }
                
                    if self.providerdata.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "There are no data to display.".localized
                        
                        self.providerdata.removeAll()
                        self.tableView.reloadData()
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @objc func didRefersh(sender: UIButton) {
        self.getProviders()
    }
    

}


extension FixedPriceVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return providerdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FixedPriceCell", for: indexPath) as! FixedPriceCell
        cell.providersByService = providerdata[indexPath.row]
        if selectServic == indexPath.row {
        cell.viewFull.borderWidth = 1
        cell.viewFull.borderColor = "586AF6".color
            cell.imgCheck.isHidden = false
        }else{
            cell.viewFull.borderWidth = 0
            cell.imgCheck.isHidden = true

        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectServic == indexPath.row {
            selectServic = -99
        }else
        {
            selectServic = indexPath.row
        }
        tableView.reloadData()
        
        btnNext.isEnabled = selectServic == -99 ? false :true
        btnNext.backgroundColor =  selectServic == -99 ? "999999".color : "586AF6".color

//        let vc:MapVC = MapVC.loadFromNib()
//        vc.isFromAddService = true
//        UserDefaults.standard.set(providerdata[indexPath.row].mechanic?.id, forKey: "mechanicID")
//        UserDefaults.standard.set(providerdata[indexPath.row].price, forKey: "price")
//
//
//        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
