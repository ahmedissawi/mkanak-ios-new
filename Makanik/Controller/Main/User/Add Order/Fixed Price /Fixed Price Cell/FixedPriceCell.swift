//
//  FixedPriceCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21.
//

import UIKit
import Cosmos

class FixedPriceCell: UITableViewCell {
    
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var viewFull: UIView!

    var providersByService:ProvidersByServiceData!{
        didSet{
            self.lblService.text = providersByService.mechanic?.name ?? ""
            self.lblPrice.text = "\(providersByService.price ?? 0)" + " " + "SAR".localized
            self.imgService.sd_custom(url: providersByService.mechanic?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
            self.viewRate.rating = Double(providersByService.mechanic?.rating ?? 0)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
