//
//  AddNewOrder.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21.
//

import UIKit
import IQKeyboardManagerSwift
import ActionSheetPicker_3_0
import Gallery
import SwiftEntryKit


class AddNewOrder: SuperViewController {
    
    @IBOutlet weak var imgcash: UIImageView!
    @IBOutlet weak var imgwallet: UIImageView!
    @IBOutlet weak var imgonline: UIImageView!
    @IBOutlet weak var imgnow: UIImageView!
    @IBOutlet weak var imglater: UIImageView!
    @IBOutlet weak var tfdate: UITextField!
    @IBOutlet weak var tftime: UITextField!
    @IBOutlet weak var txtdescrbtion: IQTextView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewCollection: UIView!
    
    @IBOutlet weak var viewCash: UIView!
    @IBOutlet weak var viewonline: UIView!
    @IBOutlet weak var viewwallet: UIView!
    @IBOutlet weak var viewdate: UIView!
    @IBOutlet weak var viewtime: UIView!

    
    var typepay:String?
    var typetime:String?
    var lat:Double?
    var Lon:Double?
    var address:String?
    
    var selectedImages: [UIImage] = []
    var previousGallery = GalleryController()
    
    var orderDeatiles:OrderDeatilesUserData?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNav()
        setupview()
        txtdescrbtion.placeholder = "Enter Descrbtion".localized
        collectionView.registerCell(id: "GalleryCell")
    }
    
    func setupview(){
        if CurrentSettings.settingsInfo?.cachePay == 1{
            self.viewCash.isHidden = false
        }
        if CurrentSettings.settingsInfo?.onlinePay == 1{
            self.viewonline.isHidden = false
        }
        
        if CurrentSettings.settingsInfo?.walletPay == 1{
            self.viewwallet.isHidden = false
        }
    
    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("New Order".localized, sender: self, large: false)
    }
    
    @IBAction func didTab_pay(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            self.imgcash.image = UIImage(named: "ic_selectedbutton")
            self.imgwallet.image = UIImage(named: "ic_unselect")
            self.imgonline.image = UIImage(named: "ic_unselect")
            self.typepay = "cash" // cash
        case 2:
            self.imgcash.image = UIImage(named: "ic_unselect")
            self.imgwallet.image = UIImage(named: "ic_selectedbutton")
            self.imgonline.image = UIImage(named: "ic_unselect")
            self.typepay = "wallet" // wallet
        case 3:
            self.imgcash.image = UIImage(named: "ic_unselect")
            self.imgwallet.image = UIImage(named: "ic_unselect")
            self.imgonline.image = UIImage(named: "ic_selectedbutton")
            self.typepay = "online" // online
        default:
            print("no type payment")
        }

    }
    
    @IBAction func didTab_time(_ sender: UIButton) {
        if sender.tag == 1{
            
            self.imgnow.image = UIImage(named: "ic_selectedbutton")
            self.imglater.image = UIImage(named: "ic_unselect")
            self.typetime = "2" // now
            self.viewdate.isHidden = true
        }else{
            
            self.imgnow.image = UIImage(named: "ic_unselect")
            self.imglater.image = UIImage(named: "ic_selectedbutton")
            self.typetime = "1" // later
            self.viewdate.isHidden = false
        }
    }
    
    @IBAction func didTab_date(_ sender: UIButton) {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
        
            self.tfdate.text = dateString
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func didTab_Pickertime(_ sender: UIButton) {
        view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized, datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "hh:mm:ss"
            let dateString = formatter.string(from: (value as? Date)!)
        
            self.tftime.text = dateString
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        datePicker?.show()
    }
    
    @IBAction func didTab_Gallery(_ sender: UIButton) {
        previousGallery = GalleryController()
        self.viewCollection.isHidden = false
        Config.tabsToShow = [.imageTab]
        previousGallery.delegate = self
        Config.Camera.imageLimit = 9
        present(previousGallery, animated: true)
        
    }
    
    func showLoginAlert(){
        self.showAlertDialog(attributes: EKAttributes.centerFloat,titleButon:"Login App".localized + "\n" , title: "Alert!".localized, message: "You are browsing the application without logging in.".localized + "\n\n" + "Please login to use the application better".localized) {
              let mainVC = LoginVC.loadFromNib()
              mainVC.isFromSkip = true
              self.present(mainVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTab_Send(_ sender: UIButton) {
        
        guard CurrentUser.userInfo != nil else{
            self.showLoginAlert()
            return
        }
        
        guard self.typepay != nil  else{
            self.showAlert(title: "Alert!".localized, message: "Please choose a payment method".localized)
            return
        }
        
        guard self.typetime != nil  else{
            self.showAlert(title: "Alert!".localized, message: "Please choose a time".localized)
            return
        }
        
        guard self.lat != nil  else{
            self.showAlert(title: "Alert!".localized, message: "Please choose address ".localized)
            return
        }
        
        guard self.Lon != nil  else{
            self.showAlert(title: "Alert!".localized, message: "Please choose address".localized)
            return
        }
        
        var parameters: [String: String] = [:]
        
        let id = UserDefaults.standard.integer(forKey: "serviceID")
        let type = UserDefaults.standard.string(forKey: "type")
        let price = UserDefaults.standard.integer(forKey: "price")
        let mechanicID = UserDefaults.standard.integer(forKey: "mechanicID")
        
        parameters["service_id"] = id.description
        
        if type == "1"{
            parameters["mechanic_id"] = mechanicID.description
            parameters["type"] = "s_price"
            parameters["price"] = price.description
            
        }else{
            parameters["type"] = "offer"
        }
        
        if typetime ==  "2"{
            parameters["priority"] = "current"
        }else{
            parameters["priority"] = "later"
            parameters["scheduling_date"] = self.tfdate.text ?? ""
            parameters["scheduling_time"] = self.tftime.text ?? ""

        }
        
        parameters["payment_type"] = self.typepay

        
        parameters["description"] = self.txtdescrbtion.text ?? ""
        parameters["lat"] = self.lat?.description
        
        parameters["lng"] = self.Lon?.description
        
        parameters["address"] = self.address ?? ""
        
        
        self.showIndicator()
        WebRequests.sendPostMultipartRequestWithMultiImgs(api:APIRouter.makeOrder,parameters: parameters, imges:selectedImages , withName: "images", completion: { (response, error) in
            self.hideIndicator()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(OrderDeatilesUserResponse.self, from: response.data!)
                self.orderDeatiles = Status.data
                let vc:ShowPopupSuccessVC = ShowPopupSuccessVC.loadFromNib()
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.ordernumber = self.orderDeatiles?.uuid ?? ""
                vc.orderID = self.orderDeatiles?.id
                self.present(vc, animated: false)

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
          }
        )
                
        
    }
    
    
    
}

extension AddNewOrder:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        let img = selectedImages[indexPath.row]
        cell.imgGallery.cornerRadius = 10
        cell.btClose.isHidden = false
        cell.imgGallery.clipsToBounds = true
        cell.imgGallery.image = img
        cell.Close = { [weak self] in
            self?.selectedImages.remove(at: indexPath.row)
            self?.collectionView.reloadData()
            if self?.selectedImages.count == 0{
                self?.viewCollection.isHidden = true
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    
    
    
}



extension AddNewOrder:GalleryControllerDelegate{
    
    // MARK: - Gallery Controller
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        if images.count > 0 {
            self.selectedImages.removeAll()
            for i in images {
                i.resolve { (_i) in
                    self.selectedImages.append(_i!)
                    if self.selectedImages.count == images.count {
                        self.collectionView.reloadData()
                    }
                }
            }
            self.collectionView.reloadData()
            controller.dismiss(animated: true)
        }else{
            controller.showAlert(title: "Error".localized, message: "chose one photo at lest".localized)
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
}
