//
//  ShowPopupSuccessVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 2/24/21.
//

import UIKit

class ShowPopupSuccessVC: UIViewController {
    
    @IBOutlet weak var lbordernumber: UILabel!

    var orderID:Int?
    var ordernumber:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbordernumber.text = ordernumber
    }
    
    
    @IBAction func didTab_goOrders(_ sender: UIButton) {
        let orderDeatiles:UserDeatilesVC = UserDeatilesVC.loadFromNib()
        let navigationController = CustomNavigationBar(rootViewController: orderDeatiles)
        orderDeatiles.hidesBottomBarWhenPushed = true
        orderDeatiles.id = self.orderID ?? 0
        navigationController.viewControllers = [orderDeatiles]
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        UserDefaults.standard.set(true, forKey: "isNotification")
    }


   

}
