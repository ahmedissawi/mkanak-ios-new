//
//  CategoryTCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/16/21.
//

import UIKit

class CategoryTCell: UITableViewCell {

    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    
    
    
    var categoriesAll:Categories!{
        didSet{
            self.imgCategory.sd_custom(url: categoriesAll.icon ?? "")
            self.lblCategory.text = categoriesAll.name  ?? ""
        }
    }
    
    var subcategoriesAll:subCategoryData!{
        didSet{
            self.imgCategory.sd_custom(url: subcategoriesAll.icon ?? "")
            self.lblCategory.text = subcategoriesAll.name  ?? ""
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
