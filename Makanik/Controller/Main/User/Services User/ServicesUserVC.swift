//
//  ServicesUserVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/17/21.
//

import UIKit

class ServicesUserVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tfSearch: UITextField!

    var isFromHome = false
    var isGrid = true
    
    var categoriesdata:CatagoryData?
    var subCategorydata = [subCategoryData]()
    var paginate:Paginate?
    var currentpage = 1
    
    
    
    var categories = [Categories]()
    var services = [subCategoryData]()

    
    var search = ""
    
    var namesubCategorie:String?
    var idsubCategory:Int?
    
    var isFromsubCategory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        if isFromHome{
            getsubCategories()
        }else if isFromsubCategory{
            getServices()
        }else{
            getAllCategories()
        }
        collectionView.registerCell(id: "CategoryCell")
        tableView.registerCell(id: "CategoryTCell")
        self.collectionView.isHidden = false
        tfSearch.returnKeyType = UIReturnKeyType.search
        tfSearch.delegate = self
        if isFromHome{

            self.tableView.es.addPullToRefresh {
                self.getsubCategories()
                self.hideIndicator()
            }
            
            self.collectionView.es.addPullToRefresh {
                self.getsubCategories()
                self.hideIndicator()
            }
            
            
        }else if isFromsubCategory{
            
            self.tableView.es.addPullToRefresh {
                self.getServices()
                self.hideIndicator()
            }
            
            self.collectionView.es.addPullToRefresh {
                self.getServices()
                self.hideIndicator()
            }
               
        }else{
            self.tableView.es.addPullToRefresh {
                self.getAllCategories()
                self.hideIndicator()
            }
            
            self.collectionView.es.addPullToRefresh {
                self.getAllCategories()
                self.hideIndicator()
            }
        }
        
    }
    
    func setupnav(){
        self.collectionView.alwaysBounceVertical = true
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setRightButtons([navigation.CollectionBtn!], sender: self)
        if isFromHome{
            navigation.setCustomBackButtonForViewController(sender: self)
            navigation.setTitle(namesubCategorie ?? "", sender: self, large: false)
        }else if isFromsubCategory{
            navigation.setCustomBackButtonForViewController(sender: self)
            navigation.setTitle(namesubCategorie ?? "", sender: self, large: false)
        }else{
            navigation.setTitle("Categories".localized, sender: self, large: false)
        }
        
    }
    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        if _sender.tag == 22{
            let navigation = self.navigationController as! CustomNavigationBar
            isGrid = !isGrid
            if isGrid{
                navigation.setRightButtons([navigation.TableBtn!], sender: self)
                collectionView.isHidden = false
                tableView.isHidden = true
                
            }else{
                navigation.setRightButtons([navigation.CollectionBtn!], sender: self)
                collectionView.isHidden = true
                tableView.isHidden = false
            }
            
        }
    }
    
    func getAllCategories(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.categories,nestedParams: "?search=\(search)").start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()

            self.collectionView.es.stopLoadingMore()
            self.collectionView.es.stopPullToRefresh()
            

            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesResponse.self, from: response.data!)
                if Status.data?.count != 0{
                    self.categories = Status.data ?? []
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }
                if Status.data?.count == 0{
                    self.tableView.showEmptyListMessage("There are no data to display.".localized)
                    
                    self.collectionView.showEmptyListMessage("There are no data to display.".localized)

                    
                    self.tableView.reloadData()
                    self.collectionView.reloadData()

                }else{
                    self.tableView.showEmptyListMessage("")
                    
                    self.collectionView.showEmptyListMessage("")
                    self.tableView.reloadData()
                    self.collectionView.reloadData()

                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    
    //Get Services
    func getServices(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.subCategoryUser,nestedParams: "\(idsubCategory ?? 0)?search=\(search)").start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()

            self.collectionView.es.stopLoadingMore()
            self.collectionView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponsesubCategory.self, from: response.data!)
                if Status.data?.count != 0{
                    self.services = Status.data ?? []
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }
                if Status.data?.count == 0{
                    self.tableView.showEmptyListMessage("There are no data to display.".localized)
                    
                    self.collectionView.showEmptyListMessage("There are no data to display.".localized)

                    
                    self.tableView.reloadData()
                    self.collectionView.reloadData()

                }else{
                    self.tableView.showEmptyListMessage("")
                    
                    self.collectionView.showEmptyListMessage("")
                    self.tableView.reloadData()
                    self.collectionView.reloadData()

                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }

    //Get sub Categories
    func getsubCategories(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.subCategories,nestedParams: "\(idsubCategory ?? 0)?search=\(search)").start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()

            self.collectionView.es.stopLoadingMore()
            self.collectionView.es.stopPullToRefresh()


            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CategoriesResponse.self, from: response.data!)
                if Status.data?.count != 0{
                    self.categories = Status.data ?? []
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }
                if Status.data?.count == 0{
                    
                    self.tableView.showEmptyListMessage("There are no data to display.".localized)
                    
                    self.collectionView.showEmptyListMessage("There are no data to display.".localized)
                    
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }else{
                    self.tableView.showEmptyListMessage("")
                    
                    self.collectionView.showEmptyListMessage("")
                    self.tableView.reloadData()
                    self.collectionView.reloadData()

                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }

    @objc func didRefersh(sender: UIButton) {
        if isFromHome{
            self.getsubCategories()
        }else if isFromsubCategory{
            self.getServices()
        }else{
            self.getAllCategories()
        }
    }
}

extension ServicesUserVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromHome{
            return self.categories.count
        }else if isFromsubCategory{
            return self.services.count
        }else{
            return self.categories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        if isFromHome{
            cell.categories =  categories[indexPath.row]
        }else if isFromsubCategory{
            cell.subcategoriesAll =  services[indexPath.row]
        }else{
            cell.categories =  categories[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isFromHome{
            let vc:ServicesUserVC = ServicesUserVC.loadFromNib()
            vc.idsubCategory = categories[indexPath.row].id
            vc.namesubCategorie = categories[indexPath.row].name
            vc.isFromsubCategory  = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if isFromsubCategory{
            let vc:TypeServicesVC = TypeServicesVC.loadFromNib()
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            UserDefaults.standard.set(services[indexPath.row].name ?? "", forKey: "servicename")
            UserDefaults.standard.set(services[indexPath.row].id  ?? "", forKey: "serviceID")
            self.present(vc, animated: false)
        }else{
            let vc:ServicesUserVC = ServicesUserVC.loadFromNib()
            vc.isFromHome = true
            vc.idsubCategory = categories[indexPath.row].id
            vc.namesubCategorie = categories[indexPath.row].name
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 150)
    }
    
    
    
}


extension ServicesUserVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFromHome{
            return self.categories.count
        }else if isFromsubCategory{
            return self.services.count
        }else{
            return self.categories.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTCell", for: indexPath) as! CategoryTCell
        if isFromHome{
            cell.categoriesAll =  categories[indexPath.row]
        }else if isFromsubCategory{
            cell.subcategoriesAll =  services[indexPath.row]
        }else{
            cell.categoriesAll =  categories[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromHome{
            let vc:ServicesUserVC = ServicesUserVC.loadFromNib()
            vc.idsubCategory = categories[indexPath.row].id
            vc.namesubCategorie = categories[indexPath.row].name
            vc.isFromsubCategory  = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if isFromsubCategory{
            let vc:TypeServicesVC = TypeServicesVC.loadFromNib()
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            UserDefaults.standard.set(services[indexPath.row].name ?? "", forKey: "servicename")
            UserDefaults.standard.set(services[indexPath.row].id  ?? "", forKey: "serviceID")
            self.present(vc, animated: false)
        }else{
            let vc:ServicesUserVC = ServicesUserVC.loadFromNib()
            vc.isFromHome = true
            vc.idsubCategory = categories[indexPath.row].id
            vc.namesubCategorie = categories[indexPath.row].name
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}



extension ServicesUserVC :UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.search  =  self.tfSearch.text ?? ""
        if isFromHome{
            if self.search != ""{
                self.categories.removeAll()
                self.getsubCategories()
            }else{
                self.showAlert(title: "Alert!".localized, message: "Search is Empty!".localized)
            }
        }else if isFromsubCategory{
            if self.search != ""{
                self.services.removeAll()
                self.getServices()
            }else{
                self.showAlert(title: "Alert!".localized, message: "Search is Empty!".localized)
            }
        }else{
            if self.search != ""{
                self.categories.removeAll()
                self.getAllCategories()
            }else{
                self.showAlert(title: "Alert!".localized, message: "Search is Empty!".localized)
            }
        }
        
        self.tfSearch.text = ""
        self.search = ""
        self.view.endEditing(true)
        
        return true
        
    }
    
    
}
