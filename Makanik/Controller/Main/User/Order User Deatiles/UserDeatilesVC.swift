//
//  UserDeatilesVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit
import Cosmos
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

class UserDeatilesVC: SuperViewController,GMSMapViewDelegate{
    
    
    @IBOutlet weak var btOffers: UIButton!
    @IBOutlet weak var btDeatiles: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbldescrbtion: UILabel!
    @IBOutlet weak var lblAddressName: UILabel!
    @IBOutlet weak var lblservices: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStartWork: UILabel!
    @IBOutlet weak var lblNamePrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgOrder: UIImageView!
    @IBOutlet weak var imgProvider: UIImageView!
    @IBOutlet weak var imgMap: UIImageView!
    @IBOutlet weak var stackMain: UIStackView!
    @IBOutlet weak var stackDeatiles: UIStackView!
    @IBOutlet weak var stackTable: UIStackView!
    @IBOutlet weak var HieghtstackTable: NSLayoutConstraint!
    
    @IBOutlet weak var viewProviderName: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    
    @IBOutlet weak var lblTypeWork: UILabel!
    
    @IBOutlet weak var StackviewComments: UIStackView!
    @IBOutlet weak var viewRatingDeatiles: UIView!
    @IBOutlet weak var RatingDeatiles: CosmosView!
    @IBOutlet weak var lblDeatilesDescrebtion: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!

    
    
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var viewCommentsRate: CosmosView!
    @IBOutlet weak var txtcomment: IQTextView!
    @IBOutlet weak var viewSegment: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    

    
    var orderDeatiles:OrderDeatilesUserData?
    var creatiedat = ""
    var id = 0
    var offers = [Offers]()
    var isFromStatusOrder = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDeatiles()
        setupnav()
        setupview()
        handeltable()
    }
    
    
    
    func setupview(){
        self.txtcomment.placeholder = "Leave a comment for your service provider".localized
        
        self.btDeatiles.backgroundColor = "FFFFFF".color
        self.btDeatiles.setTitle("Deatiles Order".localized, for: .normal)
        self.btDeatiles.setTitleColor("2476B8".color, for: .normal)
        
        self.btOffers.backgroundColor = UIColor.clear
        self.btOffers.setTitleColor("FFFFFF".color, for: .normal)
        self.stackTable.isHidden = true
        self.tableView.registerCell(id: "OffersCell")
        self.tableView.tableFooterView = UIView()
        
        self.HieghtstackTable.constant = 0
        //hide Form Rate & and Rate Deatiles to show if status is complated
        self.viewComment.isHidden = true
        self.viewRatingDeatiles.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateOffers(notification:)), name: Notification.Name("UpdateOffers"), object: nil)
        
    }
    
    
    func setRate(){
        viewCommentsRate.didTouchCosmos = { rating in
            if rating <= 3{
                self.viewRatingDeatiles.showAnimated(in: self.StackviewComments)
            }else{
                self.viewRatingDeatiles.hideAnimated(in: self.StackviewComments)
            }
        }
        
    }
    
    func handeltable(){
        
        self.tableView.es.addPullToRefresh {
            guard NetworkManager.isConnectedToNetwork() else {
                self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            self.getDeatiles()
            self.hideIndicator()
        }
        
        self.scrollView.es.addPullToRefresh {
            guard NetworkManager.isConnectedToNetwork() else {
                self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            self.getDeatiles()
            self.hideIndicator()
        }
        
    }
    
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Deatiles Order".localized, sender: self, large: false)
    }
    
    @objc func UpdateOffers(notification: NSNotification)  {
        getDeatiles()
    }
    
    @IBAction func didTab_ShowDeatilesProvider(_ sender: UIButton) {
        let vc:ShowProviderDeatilesVC = AppDelegate.sb_main.instanceVC()
        vc.hidesBottomBarWhenPushed = true
        vc.id = self.orderDeatiles?.mechanic?.id  ?? 0
        vc.rating = Double(self.orderDeatiles?.mechanic?.rating ?? 0)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTab_Status(_ sender: UIButton) {
        
        if sender.tag == 1{
            self.stackDeatiles.showAnimated(in: self.stackMain)
            self.stackTable.hideAnimated(in: self.stackMain)
            
            self.btDeatiles.backgroundColor = "FFFFFF".color
            self.btDeatiles.setTitle("Deatiles Order".localized, for: .normal)
            self.btDeatiles.setTitleColor("2476B8".color, for: .normal)
            
            self.btOffers.backgroundColor = UIColor.clear
            
            self.btOffers.setTitleColor("FFFFFF".color, for: .normal)
            
        }else{
            
            self.stackTable.showAnimated(in: self.stackMain)
            self.stackDeatiles.hideAnimated(in: self.stackMain)
            self.btOffers.backgroundColor = "FFFFFF".color
            self.btOffers.setTitleColor("2476B8".color, for: .normal)
            self.btDeatiles.backgroundColor = UIColor.clear
            self.btDeatiles.setTitle("Deatiles Order".localized, for: .normal)
            self.btDeatiles.setTitleColor("FFFFFF".color, for: .normal)
            
        }
        
    }
    
    
    func getDeatiles(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showorderUser,nestedParams: "\(id)").start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            self.scrollView.es.stopLoadingMore()
            self.scrollView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(OrderDeatilesUserResponse.self, from: response.data!)
                self.orderDeatiles = Status.data
                self.lblservices.text = self.orderDeatiles?.service ?? ""
                self.lblTime.text = self.creatiedat
                self.lbldescrbtion.text = self.orderDeatiles?.descriptionValue ?? ""
                if self.orderDeatiles?.priority == "current"{
                    self.lblStartWork.text = "current priority".localized
                }else{
                    self.lblStartWork.text = "later priority".localized
                }
                self.imgProvider.sd_custom(url: self.orderDeatiles?.mechanic?.image ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                self.imgOrder.sd_custom(url: self.orderDeatiles?.serviceIcon ?? "",placeholderImage: UIImage(named: "img_placeholder"))
                self.lblOrderNumber.text = self.orderDeatiles?.uuid ?? ""
                self.lblName.text  = self.orderDeatiles?.mechanic?.name ?? ""
                self.lblTypeWork.text = self.orderDeatiles?.service ?? ""
                self.viewRate.rating = Double(self.orderDeatiles?.mechanic?.rating ?? 0)
                self.lblAddressName.text = self.orderDeatiles?.address ?? ""
                if self.orderDeatiles?.mechanic != nil{
                    self.viewProviderName.isHidden = false
                }
                
                // Show Offers Itmes
                self.offers = self.orderDeatiles?.offers ?? []
                self.btOffers.setTitle("Offers".localized + " " + "(\(self.offers.count))", for: .normal)
                if self.orderDeatiles?.offers?.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "There are no offers to display.".localized
                    
                    self.offers.removeAll()
                    self.tableView.reloadData()
                }else{
                    self.tableView.tableFooterView = nil
                }
                
                self.tableView.reloadData()
                
                
                // Handel Offer and fixedPrice show
                if self.orderDeatiles?.type == "offer"{
                    self.HieghtstackTable.constant = 65
                    self.lblType.text = "Offer price".localized
                    self.lblNamePrice.text = "Lowest Price".localized
                    if self.orderDeatiles?.minOffer == 0.0 || self.orderDeatiles?.minOffer == 0 || self.orderDeatiles?.minOffer == nil {
                        self.lblPrice.text = "No Price".localized
                    }else{
                        self.lblPrice.text = "\(self.orderDeatiles?.minOffer ?? 0.0)" + " " +  "SAR".localized
                    }
                }else{
                    self.lblType.text = "Fixed price".localized
                    self.HieghtstackTable.constant = 0
                    self.lblPrice.text = "\(self.orderDeatiles?.price ?? "0.0")" + " " + "SAR".localized
                    self.lblNamePrice.text = "Price".localized
                }
                
                //Edite price after order is completed
                if self.orderDeatiles?.status == "completed"{
                    self.lblPrice.text =  "\(self.orderDeatiles?.price ?? "0.0")" + " " +  "SAR".localized
                    self.lblNamePrice.text = "price".localized
                    /// - Note :  this All show if status order is completed
                    // show rating deatiles if rating data != nil
                    if self.orderDeatiles?.rating != nil{
                        self.viewRatingDeatiles.isHidden = false
                        self.lblDeatilesDescrebtion.text = self.orderDeatiles?.rating?.comment ?? ""
                        self.RatingDeatiles.rating = Double(self.orderDeatiles?.rating?.rating ?? 0)
                    }else{
                        self.viewRatingDeatiles.isHidden = true
                    }
                    
                    // if isRating == false this meain provider is not submit rate for clinet
                    if self.orderDeatiles?.isRating == false{
                        self.viewComment.isHidden = false
                    }else{
                        self.viewComment.isHidden = true
                    }
                    
                }
                
                //show Map
                let lat  = Double(self.orderDeatiles?.lat ?? 0.0)
                let lng  = Double(self.orderDeatiles?.lng ?? 0.0)
                
//                let testurlStr = "http://maps.google.com/maps/api/staticmap?markers=color:red|\(lat ?? "0.0"),\(lng ?? "0.0")&zoom=10&size=400x300&sensor=true&key=AIzaSyAlRxMCvdl5ipOLuyTiof8c6B-Zvg11eHo"
//                let components = Helper.transformURLString(testurlStr)
//
//                if let url = components?.url {
//                    self.imgMap.sd_setImage(with:url)
//                    print(url)
//                    print("valid")
//                } else {
//                    print("invalid")
//                }
//
                self.mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16)
                
                let myLocationCamera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 14)
                self.mapView.animate(to: myLocationCamera)
                let position = CLLocationCoordinate2DMake(lat,lng)
                let marker = GMSMarker(position: position)
                marker.map = self.mapView

                self.mapView.delegate = self

                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    @IBAction func didTab_Rate(_ sender: UIButton) {
        
        if self.viewCommentsRate.rating <= 3{
            guard let txtrate = self.txtcomment.text, !txtrate.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your comment".localized)
                return
            }
        }
        
        if self.viewCommentsRate.rating <= 3{
            guard self.viewCommentsRate.rating != 0.0 else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your comment".localized)
                return
            }
        }
        
        
        var parameters: [String: Any] = [:]
        parameters["order_id"] =  self.orderDeatiles?.id ?? 0
        parameters["rating"] = "\(self.viewCommentsRate.rating)"
        parameters["comment"] =  self.txtcomment.text ?? ""
        
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.rateUser,parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    self.showAlert(title: "", message: Status.message ?? "")
                    self.txtcomment.text = ""
                    self.getDeatiles()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @objc func didRefersh(sender: UIButton) {
        self.getDeatiles()
    }
    
    func makeAccept(index:Int,id:Int){
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.acceptUser,nestedParams:"\(id)/accept").start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    NotificationCenter.default.post(name: Notification.Name("UpdateOffers"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    func makeRejected(index:Int,id:Int){
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.acceptUser,nestedParams:"\(id)/reject").start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }else if Status.status == 200{
                    NotificationCenter.default.post(name: Notification.Name("UpdateOffers"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    
}


extension UserDeatilesVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if offers.count != 0{
            return offers.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OffersCell", for: indexPath) as! OffersCell
        cell.offers = offers[indexPath.row]
        let id  = offers[indexPath.row].id
        cell.Accepte = { [weak self] in
            self?.makeAccept(index: indexPath.row,id:id ?? 0)
        }
        cell.Close = { [weak self] in
            self?.makeRejected(index: indexPath.row,id:id ?? 0)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
