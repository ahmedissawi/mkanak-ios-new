//
//  OffersCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/18/21.
//

import UIKit
import Cosmos

class OffersCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var lblDescrbtion: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btAccepte: UIButton!
    @IBOutlet weak var btClose: UIButton!
    @IBOutlet weak var stackoffer: UIStackView!

    var Accepte: (() -> ())?
    var Close: (() -> ())?


    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    
    var offers:Offers!{
        didSet{
            self.lblName.text = offers.mechanic?.name ?? ""
            self.lblDescrbtion.text = offers.descriptionValue ?? ""
            self.lblPrice.text = "\(offers.price ?? 0)" + " " + "SAR".localized
            self.viewRate.rating = Double(offers.mechanic?.rating ?? 0)
            self.lblDate.text = "\(offers.createdAt ?? "")"
            if offers.status == 1{
                self.stackoffer.isHidden = true
            }else{
                self.stackoffer.isHidden = false
            }

        }
    }
    

    @IBAction func btAccepte(_ sender: UIButton) {
        Accepte?()
     }
 
    @IBAction func btClose(_ sender: UIButton) {
        Close?()
     }
    
}
