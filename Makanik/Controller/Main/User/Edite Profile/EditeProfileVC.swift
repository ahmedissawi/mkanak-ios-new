//
//  EditeProfileVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21.
//

import UIKit
import FlagPhoneNumber

class EditeProfileVC: SuperViewController {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var tfCodeMobile: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var dialCode = ""
    var Code = ""
    var MobileVaild = false
    var Mobile = ""
    var selectedimage = false
    var imagePicker: ImagePicker!
    var user:UserData?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMobileFileds()
        setUpNav()
        setupdata()
    }
    
    func setUpNav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Edite Profile".localized, sender: self, large: false)
    }
    
    func setupdata(){
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.tfName.text = CurrentUser.userInfo?.name ?? ""
        self.tfEmail.text = CurrentUser.userInfo?.email ?? ""
        self.tfMobile.text = CurrentUser.userInfo?.mobile?.dropFirst(4).description ?? ""
        self.tfCodeMobile.text = CurrentUser.userInfo?.countryCode ?? ""
        self.imgProfile.sd_custom(url: CurrentUser.userInfo?.image ?? "",placeholderImage: UIImage(named: "ic_uplaodprofile"))
    }
    
    func setupMobileFileds(){
        
        tfMobile.keyboardType = .asciiCapableNumberPad
        phoneNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneNumberTextField.countryRepository)
        phoneNumberTextField.setFlag(key: .SA)
        dialCode = phoneNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = phoneNumberTextField.selectedCountry?.code.rawValue ?? ""
        
        listController.didSelect = { [weak self] country in
            self?.phoneNumberTextField.setFlag(countryCode: country.code)
        }
        phoneNumberTextField.flagLbl = tfCodeMobile
        phoneNumberTextField.delegate = self

        tfMobile.text = phoneNumberTextField.selectedCountry?.phoneCode ?? ""

    }

    
    @IBAction func didTab_Edite(_ sender: UIButton) {
        
        guard let fullName = self.tfName.text, !fullName.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your full name".localized)
            
            return
        }
        
        let email = self.tfEmail.text ?? ""
        
        if email != ""{
            if !email.isValidEamil{
                self.showAlert(title: "Alert!".localized, message: "Email Not Vaild".localized)
                
            }
        }
        
        var parameters: [String: String] = [:]
        
        parameters["name"] =  fullName
        parameters["email"] = email
        
        self.showIndicator()
        let imageData = (imgProfile.image) ?? UIImage()
        
        WebRequests.sendPostMultipartRequestWithImgParam(api:APIRouter.updateProfileUser,parameters: parameters, img: imageData, withName: "image", completion: { (response, error) in
            self.hideIndicator()

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ResponseUser.self, from: response.data!)
                self.user = Status.data
                CATransaction.begin()
                self.navigationController?.popViewController(animated: true)
                CATransaction.setCompletionBlock({ [weak self] in
                    self?.showAlert(title: "", message: "Done successfully".localized)
                    CurrentUser.userInfo?.name = self?.user?.name ?? ""
                    CurrentUser.userInfo?.email = self?.user?.email ?? ""
                    CurrentUser.userInfo?.image = self?.user?.image ?? ""
                })
                CATransaction.commit()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
        )
    }
    

}



extension EditeProfileVC: FPNTextFieldDelegate {
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        print("isValid: \(isValid)")
        MobileVaild = isValid
        Mobile = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCode = dialCode.replacingOccurrences(of: "+", with: "")
        self.Code = code
        print(name, dialCode, code)
    }
    
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        listController.title = "Countries".localized
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        
        self.present(navigationViewController, animated: true, completion: nil)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}


extension EditeProfileVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    func didSelect(image: UIImage?, picker: ImagePicker) {
        if picker == imagePicker{
            self.imgProfile.image = image
            self.selectedimage = true
        }
    }
    
}
