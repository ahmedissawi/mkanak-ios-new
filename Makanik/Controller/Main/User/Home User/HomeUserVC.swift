//
//  HomeUserVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/16/21.
//

import UIKit
import FSPagerView
import SKPhotoBrowser
import SwiftEntryKit
import CollectionKit



class HomeUserVC: SuperViewController {
    
    
    @IBOutlet weak var collectionServicesView: UICollectionView!
    @IBOutlet weak var collectionTrindView: UICollectionView!
    @IBOutlet weak var collectionNewView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewSlider: UIView!
    @IBOutlet weak var ServicesProvider: CollectionView!
    @IBOutlet weak var heightProvide: NSLayoutConstraint!

    private var SourceList = ArrayDataSource(data: [])

    
    var categories = [Categories]()
    var mostorders = [MostOrders]()
    var lastadded = [LastAdded]()
    var slider = [Slider]()
    var homedata:HomeUserData?
    
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet{
            self.pagerView.isScrollEnabled = true
            self.pagerView.register(UINib(nibName: "SliderCell", bundle: Bundle.main), forCellWithReuseIdentifier: "SliderCell")
            self.pagerView.itemSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 220)
            self.pagerView.transformer = FSPagerViewTransformer(type: .cubic)
            self.pagerView.automaticSlidingInterval = 2.0
        }
    }
    
    var imagesBrowser = [SKPhoto]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav()
        getHome()
        SetupList()
        collectionServicesView.registerCell(id: "CategoryCell")
        collectionTrindView.registerCell(id: "CategoryCell")
        collectionNewView.registerCell(id: "CategoryCell")
        UserDefaults.standard.removeObject(forKey: "isNotification")
        self.scrollView.es.addPullToRefresh {
            self.getHome()
            self.hideIndicator()
        }
    }
    
    func SetupList() {
        
        let selectionAction = { (context: BasicProvider<Any, HomeCard>.TapContext) -> Void in
            
            let obj = self.SourceList.data[context.index] as? Categories
//            let mainVC = PostInfoVC.loadFromNib()
//            mainVC.idItem = obj?.id ?? 0
//            self.navigationController?.pushViewController(mainVC, animated: true)
            let vc:ServicesUserVC = ServicesUserVC.loadFromNib()
            vc.isFromHome = true
            vc.idsubCategory = obj?.id
            vc.namesubCategorie = obj?.name
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)

        }

        let cellUpdater = { (cell: HomeCard, data: Any, index: Int) in
            cell.obj = self.SourceList.data[index] as? Categories

            if MOLHLanguage.isRTLLanguage() {
                cell.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
        let viewSource = ClosureViewSource(viewUpdater: cellUpdater)
        let sizeSource = { (index: Int, data: Any, collectionSize: CGSize) -> CGSize in
            return CGSize(width: UIScreen.screenWidth - 40, height: 60)
        }

        let provider = BasicProvider<Any, HomeCard>(dataSource: SourceList,
                                                             viewSource: viewSource,
                                                             sizeSource: sizeSource,
                                                             layout: FlowLayout(lineSpacing: 8).inset(by: UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)),
                                                             tapHandler: selectionAction)
        ServicesProvider.delegate = self
        ServicesProvider.showsVerticalScrollIndicator = false
        ServicesProvider.showsHorizontalScrollIndicator = false
        ServicesProvider.provider = provider
        ServicesProvider.isScrollEnabled =  false
        if MOLHLanguage.isRTLLanguage() {
            ServicesProvider.transform = CGAffineTransform(scaleX: -1, y: 1)
        }

    }
    func setupnav(){
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Makank".localized, sender: self, large: false)
    }
    
    func getHome(){
        
        _ = WebRequests.setup(controller: self).prepare(api: APIRouter.homeUser).start(){ (response, error) in
            
            self.scrollView.es.stopLoadingMore()
            self.scrollView.es.stopPullToRefresh()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(HomeUser.self, from: response.data!)
                self.homedata = Status.data
                if self.homedata?.categories?.count != 0{
                    self.categories = self.homedata?.categories ?? []
                    self.SourceList.data = self.homedata?.categories ?? []
                    self.heightProvide.constant = CGFloat(70 * (self.homedata?.categories?.count ?? 5 ))

                }
                if self.homedata?.mostOrders?.count != 0{
                    self.mostorders = self.homedata?.mostOrders ?? []
                }
                if self.homedata?.lastAdded?.count != 0{
                    self.lastadded = self.homedata?.lastAdded ?? []
                }
                if self.homedata?.slider?.count != 0{
                    self.slider = self.homedata?.slider ?? []
                    self.viewSlider.isHidden = false
                }else{
                    self.viewSlider.isHidden = true
                }
                self.collectionNewView.reloadData()
                self.collectionTrindView.reloadData()
                self.collectionServicesView.reloadData()
                self.pagerView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @IBAction func didTab_ShowList(_ sender: UIButton) {
        tabBarController?.selectedIndex = 1
    }
    
 
    
}


extension HomeUserVC:FSPagerViewDelegate,FSPagerViewDataSource{
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return slider.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "SliderCell", at: index) as! SliderCell
        cell.imgSlider.sd_custom(url: slider[index].image ?? "")
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let vc = NewOrderVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)

    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        
    }
    
}

extension HomeUserVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionServicesView{
            return self.categories.count
        }else if collectionView == collectionTrindView{
            return self.mostorders.count
        }else{
            return self.lastadded.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        if collectionView == collectionServicesView{
            cell.categories = categories[indexPath.row]
        }else if collectionView == collectionTrindView{
            cell.mostorders = mostorders[indexPath.row]
        }else{
            cell.lastadded = lastadded[indexPath.row]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionServicesView{
                let vc:ServicesUserVC = ServicesUserVC.loadFromNib()
                vc.isFromHome = true
                vc.idsubCategory = categories[indexPath.row].id
                vc.namesubCategorie = categories[indexPath.row].name
                vc.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collectionTrindView{
                let vc:TypeServicesVC = TypeServicesVC.loadFromNib()
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                UserDefaults.standard.set(mostorders[indexPath.row].name ?? "", forKey: "servicename")
                UserDefaults.standard.set(mostorders[indexPath.row].subCategory?.id  ?? "", forKey: "serviceID")
                vc.hidesBottomBarWhenPushed = true
              self.tabBarController?.present(vc, animated: false)
        }else{
                let vc:TypeServicesVC = TypeServicesVC.loadFromNib()
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                UserDefaults.standard.set(lastadded[indexPath.row].name ?? "", forKey: "servicename")
                UserDefaults.standard.set(lastadded[indexPath.row].id ?? "", forKey: "serviceID")
                vc.hidesBottomBarWhenPushed = true
            self.tabBarController?.present(vc, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 130)
    }
    
    
}
extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}
