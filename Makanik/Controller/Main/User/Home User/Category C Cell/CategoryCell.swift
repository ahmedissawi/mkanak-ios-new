//
//  CategoryCell.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/16/21.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!

    
    
    var categories:Categories!{
        didSet{
            self.imgCategory.sd_custom(url: categories.icon ?? "")
            self.lblCategory.text = categories.name  ?? ""
        }
    }
    
    var categoriesAll:Categories!{
        didSet{
            self.imgCategory.sd_custom(url: categoriesAll.icon ?? "")
            self.lblCategory.text = categoriesAll.name  ?? ""
        }
    }
    
    var subcategoriesAll:subCategoryData!{
        didSet{
            self.imgCategory.sd_custom(url: subcategoriesAll.icon ?? "")
            self.lblCategory.text = subcategoriesAll.name  ?? ""
        }
    }
    
    var mostorders:MostOrders!{
        didSet{
            self.imgCategory.sd_custom(url: mostorders.icon ?? "")
            self.lblCategory.text = mostorders.name  ?? ""
        }
    }
    
    var lastadded:LastAdded!{
        didSet{
            self.imgCategory.sd_custom(url: lastadded.icon ?? "")
            self.lblCategory.text = lastadded.name  ?? ""
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

}
