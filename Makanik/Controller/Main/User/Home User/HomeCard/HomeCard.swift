//
//  ColorsCard.swift
//  senam
//
//  Created by  Ahmed’s MacBook Pro on 3/30/21.
//

import UIKit

class HomeCard: UIView {

    // MARK: - IBOutlet

    @IBOutlet weak var imageColor: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblpriority: UILabel!
    @IBOutlet weak var viewPriority: UIView!

    @IBOutlet weak var viewBoarder: UIView!
    var obj:Categories!{
        didSet{
            self.lblname.text = obj.name ?? ""
            imageColor.sd_custom(url: obj.icon ?? "")
            self.lblDate.text = obj.descriptionValue ?? ""
//            viewPriority.isHidden =  obj.priority == "urgent" ? false : true
             
        }
        
    }
    var hideUrgent:Bool!{
        didSet{
            viewPriority.isHidden =   hideUrgent

        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    init() {
        super.init(frame: .zero)
        fromNib()
    }
    
    
}
extension UIView {
    public func fillSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
        }
    }
    @discardableResult
    func fromNib<T : UIView>() -> T? {
        guard let contentView = Bundle(for: type(of: self)).loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? T else {
            return nil
        }
        addSubview(contentView)
        contentView.fillSuperview()
        return contentView
    }
}
