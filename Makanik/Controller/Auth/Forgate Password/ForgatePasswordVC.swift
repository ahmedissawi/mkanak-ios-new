//
//  ForgatePasswordVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit
import FlagPhoneNumber
import Firebase
import FirebaseMessaging
import FirebaseAuth


class ForgatePasswordVC: SuperViewController {
    
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var tfCodeMobile: UILabel!
    @IBOutlet weak var tfMobile: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMobileFileds()
    }
    
    var dialCode = ""
    var Code = ""
    var MobileVaild = false
    var Mobile = ""
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    func setupMobileFileds(){
        tfMobile.keyboardType = .asciiCapableNumberPad
        phoneNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneNumberTextField.countryRepository)
        phoneNumberTextField.setFlag(key: .SA)
        dialCode = phoneNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = (phoneNumberTextField.selectedCountry?.code.rawValue)!
        listController.didSelect = { [weak self] country in
            self?.phoneNumberTextField.setFlag(countryCode: country.code)
        }
        phoneNumberTextField.flagLbl = tfCodeMobile
        phoneNumberTextField.delegate = self
        tfCodeMobile.text = phoneNumberTextField.selectedCountry?.phoneCode ?? ""
        tfMobile.addTarget(self, action: #selector(didEditText), for: .editingChanged)
    }
    
    @objc private func didEditText(_ textField: UITextField) {
        phoneNumberTextField.text = textField.text
        phoneNumberTextField.setupMyTEXT()
    }
    @IBAction func didTab_MobileCode(_ sender: UIButton) {
        phoneNumberTextField.selectCountries()
    }
    
    @IBAction func didTab_Send(_ sender: UIButton) {
        
        if !MobileVaild {
            self.showAlert(title: "Alert!".localized, message: "Mobile Not Vaild".localized)
            
            return
        }
        
        
        if CurrentUser.typeSelect == "client"{
            
            
            guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your mobile number".localized)
                return
            }
            
            
            var parametersUserCheck: [String: Any] = [:]
            parametersUserCheck["mobile"] = "+" + dialCode + mobile
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.checkUser,parameters: parametersUserCheck).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if !Status.success!{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.success == true,Status.status == 200 {
                        let vc:CheckMobileCodeVC = CheckMobileCodeVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        vc.mobile = "+" + self.dialCode + mobile
                        vc.Typeverification = TypeVer.forgatepassword
                        self.present(vc, animated: true, completion: nil)
                    }
                     
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            
            
            guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your mobile number".localized)
                return
            }
            
                        
            var parametersProviderCheck: [String: Any] = [:]
            parametersProviderCheck["mobile"] = "+" + dialCode + mobile
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.checkProvider,parameters: parametersProviderCheck).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if !Status.success!{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.success == true,Status.status == 200 {
                        let vc:CheckMobileCodeVC = CheckMobileCodeVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        vc.mobile = "+" + self.dialCode + mobile
                        vc.Typeverification = TypeVer.forgatepassword
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
    }
    
    @IBAction func didTab_login(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension ForgatePasswordVC: FPNTextFieldDelegate {
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        print("isValid: \(isValid)")
        MobileVaild = isValid
        Mobile = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCode = dialCode.replacingOccurrences(of: "+", with: "")
        self.Code = code
        print(name, dialCode, code)
    }
    
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        listController.title = "Countries".localized
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        
        self.present(navigationViewController, animated: true, completion: nil)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}
