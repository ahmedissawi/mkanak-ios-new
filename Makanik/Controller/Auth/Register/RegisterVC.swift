//
//  RegisterVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit
import FlagPhoneNumber
import ActionSheetPicker_3_0

class RegisterVC: SuperViewController {
    
    
    @IBOutlet weak var tfUserCodeMobile: UILabel!
    @IBOutlet weak var tfUserMobile: UITextField!
    @IBOutlet weak var tfUserFullName: UITextField!
    @IBOutlet weak var tfUserPassword: UITextField!
    @IBOutlet weak var phoneUserNumberTextField: FPNTextField!
    
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var viewProvider: UIView!
    @IBOutlet weak var lbluser: UILabel!
    @IBOutlet weak var lblProvider: UILabel!
    @IBOutlet weak var imgProvider: UIImageView!
    @IBOutlet weak var imguser: UIImageView!
    
    
    @IBOutlet weak var tfProviderFullName: UITextField!
    @IBOutlet weak var tfProviderEmail: UITextField!
    @IBOutlet weak var tfProviderType: UITextField!

    @IBOutlet weak var tfProviderCodeMobile: UILabel!
    @IBOutlet weak var tfProviderMobile: UITextField!
    @IBOutlet weak var tfProviderPassword: UITextField!
    @IBOutlet weak var tfProviderConfirmPassword: UITextField!
    @IBOutlet weak var tfProviderID:UITextField!
    @IBOutlet weak var phoneProviderNumberTextField: FPNTextField!
    @IBOutlet weak var imgID: UIImageView!
    
    
    @IBOutlet weak var ViewimgID: UIView!
    @IBOutlet weak var imglicence: UIImageView!
    @IBOutlet weak var Viewimglicence: UIView!
    @IBOutlet weak var ViewimgUplaodlicence: UIView!

    
    @IBOutlet weak var stackUser: UIStackView!
    @IBOutlet weak var stackProvider: UIStackView!
    @IBOutlet weak var stackMain: UIStackView!
    
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var imgFemale: UIImageView!

    
    @IBOutlet weak var CheckBox: CheckBox!

    
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var viewID: UIView!
    @IBOutlet weak var viewtfID: UIView!

    
    var selectedimage = false
    var selectedimagelicence = false

    var imagePicker: ImagePicker!
    var imagePickerlicence: ImagePicker!

    var type = ""
    var gendertype = ""
    var typeProvider = [String]()

    
    var dialCode = ""
    var Code = ""
    var MobileVaild = false
    var Mobile = ""
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handelShowIDGander()
        setupMobileFileds()
        self.stackProvider.isHidden = true
        if CurrentUser.typeSelect == "client" {
                self.stackProvider.isHidden = true
            self.stackUser.isHidden = false

                
                self.lbluser.textColor = "EDAE20".color
                self.imguser.image = UIImage(named: "ic_whiteuser")
                
    //            self.viewProvider.backgroundColor = "FAFAFC".color
                self.lblProvider.textColor = "999999".color
                self.imgProvider.image = UIImage(named: "ic_blackprovider")
                CurrentUser.typeSelect = "client"

            }else{
                self.stackProvider.isHidden = false
            self.stackUser.isHidden = true
                self.lbluser.textColor = "999999".color
                self.imguser.image = UIImage(named: "ic_blackuser")
                
    //            self.viewProvider.backgroundColor = "EDAE20".color
                self.lblProvider.textColor = "EDAE20".color
                self.imgProvider.image = UIImage(named: "ic_whiteprovider")
                CurrentUser.typeSelect = "provider"
            
        }
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.imagePickerlicence = ImagePicker(presentationController: self, delegate: self)
        self.ViewimgID.isHidden = true
        self.tfProviderID.keyboardType = .asciiCapableNumberPad
        self.ViewimgUplaodlicence.isHidden = true
        self.Viewimglicence.isHidden = true
        self.ViewimgID.isHidden = true

    }
    
    func handelShowIDGander(){
        if CurrentSettings.settingsInfo?.showInput == 0{
            self.viewGender.isHidden = true
            self.viewID.isHidden = true
            self.viewtfID.isHidden = true
            self.ViewimgUplaodlicence.isHidden = true
        }else{
            self.viewGender.isHidden = false
            self.viewID.isHidden = false
            self.viewtfID.isHidden = false
            self.ViewimgUplaodlicence.isHidden = false
        }
    }
    
    
    func setupMobileFileds(){
        tfUserMobile.keyboardType = .asciiCapableNumberPad
        phoneUserNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneUserNumberTextField.countryRepository)
        phoneUserNumberTextField.setFlag(key: .SA)
        dialCode = phoneUserNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = (phoneUserNumberTextField.selectedCountry?.code.rawValue)!
        listController.didSelect = { [weak self] country in
            self?.phoneUserNumberTextField.setFlag(countryCode: country.code)
        }
        phoneUserNumberTextField.flagLbl = tfUserCodeMobile
        phoneUserNumberTextField.delegate = self
        tfUserCodeMobile.text = phoneUserNumberTextField.selectedCountry?.phoneCode ?? ""
        tfUserMobile.addTarget(self, action: #selector(didEditText), for: .editingChanged)
        
        
        
        tfProviderMobile.keyboardType = .asciiCapableNumberPad
        phoneProviderNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneProviderNumberTextField.countryRepository)
        phoneProviderNumberTextField.setFlag(key: .SA)
        dialCode = phoneProviderNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = (phoneProviderNumberTextField.selectedCountry?.code.rawValue)!
        listController.didSelect = { [weak self] country in
            self?.phoneProviderNumberTextField.setFlag(countryCode: country.code)
        }
        phoneProviderNumberTextField.flagLbl = tfProviderCodeMobile
        phoneProviderNumberTextField.delegate = self
        tfProviderCodeMobile.text = phoneProviderNumberTextField.selectedCountry?.phoneCode ?? ""
        tfProviderMobile.addTarget(self, action: #selector(didEditText), for: .editingChanged)
        
        }
    
    @objc private func didEditText(_ textField: UITextField) {
        if CurrentUser.typeSelect == "client"{
            phoneUserNumberTextField.text = textField.text
            phoneUserNumberTextField.setupMyTEXT()
        }else{
            phoneProviderNumberTextField.text = textField.text
            phoneProviderNumberTextField.setupMyTEXT()
            
        }
    }
    
    @IBAction func didTab_Gender(_ sender: UIButton) {
        if sender.tag == 1{
            self.imgMale.image = UIImage(named: "ic_selectedbutton")
            self.imgFemale.image = UIImage(named: "ic_unselect")
            self.gendertype = "male"

        }else{
            self.imgMale.image = UIImage(named: "ic_unselect")
            self.imgFemale.image = UIImage(named: "ic_selectedbutton")
            self.gendertype = "female"

        }
    }
    
    @IBAction func didTab_Type(_ sender: UIButton) {
        if CurrentSettings.settingsInfo?.showInput == 1{
            self.typeProvider = ["company","person"]
        }else{
            self.typeProvider = ["person"]
        }
        ActionSheetStringPicker.show(withTitle: "Type".localized, rows: self.typeProvider, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.tfProviderType.text = Value as? String
            }
            
            self.type = self.tfProviderType.text ?? ""
            if self.type == "company"{
                self.ViewimgUplaodlicence.isHidden = false
            }else{
                self.ViewimgUplaodlicence.isHidden = true
            }
            
            return
           
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    @IBAction func didTab_MobileCode(_ sender: UIButton) {
        if CurrentUser.typeSelect == "client"{
            phoneUserNumberTextField.selectCountries()
        }else{
            phoneProviderNumberTextField.selectCountries()
        }
    }
    
    
    
    @IBAction func didTab_Switch(_ sender: UIButton) {
        if sender.tag == 0{
            self.stackProvider.hideAnimated(in: self.stackMain)

            self.stackUser.showAnimated(in: self.stackMain)
            
            self.lbluser.textColor = "EDAE20".color
            self.imguser.image = UIImage(named: "ic_whiteuser")
            
//            self.viewProvider.backgroundColor = "FAFAFC".color
            self.lblProvider.textColor = "999999".color
            self.imgProvider.image = UIImage(named: "ic_blackprovider")
            CurrentUser.typeSelect = "client"

        }else{
            self.stackUser.hideAnimated(in: self.stackMain)

            self.stackProvider.showAnimated(in: self.stackMain)
            
            self.lbluser.textColor = "999999".color
            self.imguser.image = UIImage(named: "ic_blackuser")
            
//            self.viewProvider.backgroundColor = "EDAE20".color
            self.lblProvider.textColor = "EDAE20".color
            self.imgProvider.image = UIImage(named: "ic_whiteprovider")
            CurrentUser.typeSelect = "provider"
        }
    }
    
    
    @IBAction func didTab_Login(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTab_Terms(_ sender: Any) {
        let navVC: CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        let vc:InfoVC = InfoVC.loadFromNib()
        navVC.viewControllers = [vc]
        navVC.modalPresentationStyle = .fullScreen
        vc.isFromRegister = true
        vc.keyInfo = "usage_policy"
        self.present(navVC, animated: false, completion: nil)
    }
    
    @IBAction func didTab_Register(_ sender: UIButton) {
        
        
        if !MobileVaild {
            self.showAlert(title: "Alert!".localized, message: "Mobile Not Vaild".localized)
            
            return
        }
        
        guard CheckBox.isChecked == true else{
            showAlert(title: "".localized, message: "You must agree to the terms and conditions.".localized)
            return
        }

        
        
        if CurrentUser.typeSelect == "client"{
            
            
            guard let fullName = self.tfUserFullName.text, !fullName.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your full name".localized)
                
                return
            }
            guard let mobile = self.tfUserMobile.text, !mobile.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your mobile number".localized)
                return
            }
            
            
            guard let password = self.tfUserPassword.text, !password.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your password".localized)
                return
            }
            
            if password.count < 6 {
                showAlert(title: "Alert!".localized, message: "Password must be at least 6 characters".localized)
                return
            }
            
            var parametersUser: [String: Any] = [:]
            parametersUser["name"] = fullName
            parametersUser["mobile"] = "+" + dialCode + mobile
            parametersUser["password"] = password
            parametersUser["country_code"] = dialCode.description
            parametersUser["short_country"] = Code.description

            
            var parametersUserCheck: [String: Any] = [:]
            parametersUserCheck["mobile"] = "+" + dialCode + mobile

            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.checkUser,parameters: parametersUserCheck).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.success == true{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.success == false,Status.status == 200{
                        let vc:CheckMobileCodeVC = CheckMobileCodeVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        vc.parametersUser = parametersUser
                        vc.Typeverification = TypeVer.signUp
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            
            guard let fullName = self.tfProviderFullName.text, !fullName.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your full name".localized)
                
                return
            }
            
            guard let email = self.tfProviderEmail.text, !email.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your email".localized)
                
                return
            }
            
            guard let mobile = self.tfProviderMobile.text, !mobile.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your mobile number".localized)
                return
            }
            
            
            guard let type = self.tfProviderType.text, !type.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter type".localized)
                return
            }
            
            
            guard let password = self.tfProviderPassword.text, !password.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your password".localized)
                return
            }
            
            guard let confirmpassword = self.tfProviderConfirmPassword.text, !confirmpassword.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Please enter your confirm password".localized)
                return
            }
            
            if CurrentSettings.settingsInfo?.showInput == 1{
                guard let id = self.tfProviderID.text, !id.isEmpty else{
                    self.showAlert(title: "Alert!".localized, message: "Please enter your ID Number".localized)
                    return
                }
            }
            
            if password.count < 6 {
                showAlert(title: "Alert!".localized, message: "Password must be at least 6 characters".localized)
                return
            }
            
            guard (self.tfProviderConfirmPassword.text != nil), !confirmpassword.isEmpty else{
                self.showAlert(title: "Alert!".localized, message: "Password not matched".localized)
                return
            }
            
            if CurrentSettings.settingsInfo?.showInput == 1{
                if self.type == "company"{
                    
                    guard selectedimage != false else{
                        self.showAlert(title: "Alert!".localized, message: "Please choose a photo ID".localized)
                        return
                    }
                    guard selectedimagelicence != false else{
                        self.showAlert(title: "Alert!".localized, message: "Please choose image of the business license".localized)
                        return
                    }
                }
            }
            
            let imageID = (imgID.image) ?? UIImage()
            let imagelicence = (imglicence.image) ?? UIImage()

            
            var parametersProvider: [String: String] = [:]
            parametersProvider["name"] = fullName
            parametersProvider["mobile"] = "+" + dialCode + mobile
            parametersProvider["country_code"] = dialCode.description
            parametersProvider["short_country"] = Code.description
            parametersProvider["password"] = password
            parametersProvider["email"] = email
            parametersProvider["type"] = self.type
            parametersProvider["gender"] = self.gendertype
            parametersProvider["identification_number"] = self.tfProviderID.text ?? ""

            
            var parametersProviderCheck: [String: Any] = [:]
            parametersProviderCheck["mobile"] = "+" + dialCode + mobile
            parametersProviderCheck["email"] = email
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.checkProvider,parameters: parametersProviderCheck).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.success == true{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.success == false,Status.status == 200{
                        let vc:CheckMobileCodeVC = CheckMobileCodeVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        vc.parametersProvider = parametersProvider
                        vc.imagelicence = imagelicence
                        vc.imageidentification = imageID
                        vc.Typeverification = TypeVer.signUp
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
        
    }
    
    
    @IBAction func didTab_Close(_ sender: UIButton) {
        if sender.tag == 1{
            self.ViewimgID.isHidden = true
            self.selectedimage = false

        }else{
            self.Viewimglicence.isHidden = true
            self.selectedimagelicence = false

        }
    }
    
}


extension RegisterVC: FPNTextFieldDelegate {
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        print("isValid: \(isValid)")
        MobileVaild = isValid
        Mobile = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCode = dialCode.replacingOccurrences(of: "+", with: "")
        self.Code = code
        print(name, dialCode, code)
    }
    
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        listController.title = "Countries".localized
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        
        self.present(navigationViewController, animated: true, completion: nil)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}


extension RegisterVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        if sender.tag == 1{
            self.imagePicker.present(from: sender)

        }else{
            self.imagePickerlicence.present(from: sender)

        }
        
    }
    func didSelect(image: UIImage?,picker:ImagePicker) {
        if picker == imagePicker{
            self.imgID.image = image
            self.selectedimage = true
            self.ViewimgID.isHidden = false
        }else{
            self.imglicence.image = image
            self.selectedimagelicence = true
            self.Viewimglicence.isHidden = false

        }
        
    }
    
}
