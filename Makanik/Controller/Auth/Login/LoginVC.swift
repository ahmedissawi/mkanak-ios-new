//
//  LoginVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit
import FlagPhoneNumber


class LoginVC: SuperViewController {
    
    @IBOutlet weak var tfCodeMobile: UILabel!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var phoneNumberTextField: FPNTextField!
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var viewProvider: UIView!
    @IBOutlet weak var lbluser: UILabel!
    @IBOutlet weak var lblProvider: UILabel!
    @IBOutlet weak var imgProvider: UIImageView!
    @IBOutlet weak var imguser: UIImageView!
    
    
    var dialCode = ""
    var Code = ""
    var MobileVaild = false
    var Mobile = ""
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var myProviderdeatiles:MyProviderData?
    var isFromSkip = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMobileFileds()
        setupview()
    }
    
    func setupview(){
        if CurrentUser.typeSelect != "provider" {

        self.lbluser.textColor = "EDAE20".color
        self.imguser.image = UIImage(named: "ic_whiteuser")
        
//            self.viewProvider.backgroundColor = "FAFAFC".color
        self.lblProvider.textColor = .white
        self.imgProvider.image = UIImage(named: "ic_blackprovider")
        }else{
            self.lbluser.textColor = .white
            self.imguser.image = UIImage(named: "ic_blackuser")
            
//            self.viewProvider.backgroundColor = "EDAE20".color
            self.lblProvider.textColor = "EDAE20".color
            self.imgProvider.image = UIImage(named: "ic_whiteprovider")
            CurrentUser.typeSelect = "provider"

        }
    }
    
    func setupMobileFileds(){
        tfMobile.keyboardType = .asciiCapableNumberPad
        phoneNumberTextField.displayMode = .list // .picker by default
        listController.setup(repository: phoneNumberTextField.countryRepository)
        phoneNumberTextField.setFlag(key: .SA)
        dialCode = phoneNumberTextField.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "") ?? ""
        Code = (phoneNumberTextField.selectedCountry?.code.rawValue)!
        listController.didSelect = { [weak self] country in
            self?.phoneNumberTextField.setFlag(countryCode: country.code)
        }
        phoneNumberTextField.flagLbl = tfCodeMobile
        phoneNumberTextField.delegate = self
        tfCodeMobile.text = phoneNumberTextField.selectedCountry?.phoneCode ?? ""
        tfMobile.placeholder = phoneNumberTextField.placeholder
        tfMobile.addTarget(self, action: #selector(didEditText), for: .editingChanged)
    }
    
    @objc private func didEditText(_ textField: UITextField) {
        phoneNumberTextField.text = textField.text
        phoneNumberTextField.setupMyTEXT()
    }
    @IBAction func didTab_MobileCode(_ sender: UIButton) {
        phoneNumberTextField.selectCountries()
    }
    
    @IBAction func didTab_Switch(_ sender: UIButton) {
        if sender.tag == 0{
            
//            self.viewUser.backgroundColor = "EDAE20".color
            self.lbluser.textColor = "EDAE20".color
            self.imguser.image = UIImage(named: "ic_whiteuser")
            
//            self.viewProvider.backgroundColor = "FAFAFC".color
            self.lblProvider.textColor = .white
            self.imgProvider.image = UIImage(named: "ic_blackprovider")
            CurrentUser.typeSelect = "client"

        }else{
            
//            self.viewUser.backgroundColor = "FAFAFC".color
            self.lbluser.textColor = .white
            self.imguser.image = UIImage(named: "ic_blackuser")
            
//            self.viewProvider.backgroundColor = "EDAE20".color
            self.lblProvider.textColor = "EDAE20".color
            self.imgProvider.image = UIImage(named: "ic_whiteprovider")
            CurrentUser.typeSelect = "provider"

        }
    }
    
    @IBAction func didTab_Skip(_ sender: UIButton) {
        let vc:TTabBarViewController = TTabBarViewController.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        CurrentUser.typeSelect = ""
    }
    
    @IBAction func didTab_Register(_ sender: UIButton) {
        let vc:RegisterVC = RegisterVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func didTab_Forgate(_ sender: UIButton) {
        let vc:ForgatePasswordVC = ForgatePasswordVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)

    }
    
    func getProfile(){
        if CurrentUser.typeSelect == "client"{
            
            
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showmyProvider).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseMyProvider.self, from: response.data!)
                    CurrentProviderProfile.providerInfo = Status.data
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
   
            
    }
    
    @IBAction func didTab_login(_ sender: UIButton) {
        
        guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your mobile number".localized)
            return
        }
        
        if !MobileVaild {
            self.showAlert(title: "Alert!".localized, message: "Mobile Not Vaild".localized)
            
            return
        }
        
        guard let password = self.tfPassword.text, !password.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your password".localized)
            return
        }
        
//        if CurrentUser.typeSelect == "client"{
//            #if DEBUG
//            mobile = "+970592269898"
//            password = "123456"
//            #endif
//        }else{
//            #if DEBUG
//            mobile =  "970" "597523111"
//            password = "123456"
//            #endif
//        }
        
        var parameters: [String: Any] = [:]
        parameters["mobile"] =   "+" + dialCode + mobile
        parameters["password"] = password
        
        
        if CurrentUser.typeSelect == "client"{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.loginUser,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseUser.self, from: response.data!)
                    CurrentUser.userInfo = Status.data
                    if CurrentUser.userInfo != nil{
                        FCM.handelTopicNotification()
                        self.getProfile()
                        if self.isFromSkip{
                            self.dismiss(animated: true, completion: nil)
                            UserDefaults.standard.set(true, forKey: "isNotification")
                        }else{
                            let vc:TTabBarViewController = TTabBarViewController.loadFromNib()
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.loginProvider,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseProvider.self, from: response.data!)
                    CurrentProvider.providerInfo = Status.data
                    if CurrentProvider.providerInfo != nil{
                        FCM.handelTopicNotification()
                        self.getProfile()
                        let vc:TTabBarViewController = TTabBarViewController.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
        
    }
    
    
}


extension LoginVC: FPNTextFieldDelegate {
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        print("isValid: \(isValid)")
        MobileVaild = isValid
        Mobile = textField.getFormattedPhoneNumber(format: .E164) ?? ""
        print(
            isValid,
            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
            textField.getRawPhoneNumber() ?? "Raw: nil"
        )
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        self.dialCode = dialCode.replacingOccurrences(of: "+", with: "")
        self.Code = code
        print(name, dialCode, code)
    }
    
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        listController.title = "Countries".localized
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))
        
        self.present(navigationViewController, animated: true, completion: nil)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}
