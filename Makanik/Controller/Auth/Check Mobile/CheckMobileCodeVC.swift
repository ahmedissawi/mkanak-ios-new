//
//  CheckMobileCodeVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit
import VKPinCodeView
import Firebase
import FirebaseMessaging
import FirebaseAuth

enum TypeVer : Int {
    case signUp
    case forgatepassword

}

class CheckMobileCodeVC: SuperViewController {
    
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var PinCodeView: VKPinCodeView!
    
    var mobilTxt = ""
    var codeNumber:String?
    var parametersUser: [String: Any] = [:]
    var parametersProvider: [String:String] = [:]
    var mobile = ""
    var Typeverification = TypeVer.signUp
    var imagelicence:UIImage?
    var imageidentification:UIImage?
    var myProviderdeatiles:MyProviderData?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupcodeView()
        if Typeverification == .forgatepassword{
            setCodeForgatePassword()
        }else{
            setupCode()
        }
        setCode()
    }
    
    func setupCode(){
        if CurrentUser.typeSelect == "client"{
            self.mobile = (parametersUser["mobile"] as? String ?? "")
            lblMobile.text =  "\(self.mobile)"

        }else{
            self.mobile = (parametersProvider["mobile"] ?? "")
            lblMobile.text =  "\(self.mobile)"
        }
        
        mobilTxt = self.lblMobile.text ?? ""

        #if DEBUG
        mobilTxt = "+966597523111"
        #endif
        
    }
    
    func setCodeForgatePassword(){
        
        mobilTxt = self.mobile
    }
    
    
    
    func setupcodeView(){
        
        PinCodeView.keyBoardType = .asciiCapableNumberPad
        PinCodeView.becomeFirstResponder()
        let border = VKEntryViewStyle.border(font: UIFont.NeoSansArabicBlack(ofSize: 15), textColor: UIColor.black, errorTextColor: .red, cornerRadius: 5, borderWidth: 1, borderColor: UIColor.clear, selectedBorderColor: UIColor.black, errorBorderColor: UIColor.red, backgroundColor: "F5F5F5".color, selectedBackgroundColor: "F5F5F5".color)
        
        PinCodeView.setStyle(border)
        PinCodeView.shakeOnError = true
        PinCodeView.onCodeDidChange = { code in
            print("code/\(code)")
            self.codeNumber = code
        }
    }
    
    func setCode(){
        PhoneAuthProvider.provider().verifyPhoneNumber(mobilTxt, uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                self.alert(message:error!.localizedDescription)
                return
            }
            print("verificationID:\(verificationID!)")
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
    
    func  goToReset(){
        
        let mainVC:ResetPasswordVC = ResetPasswordVC.loadFromNib()
        mainVC.modalPresentationStyle = .fullScreen
        mainVC.mobileActivie = self.mobile
        self.present(mainVC, animated: true, completion: nil)

    }
    
    func getProfile(){
        if CurrentUser.typeSelect == "client"{
            
            
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.showmyProvider).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseMyProvider.self, from: response.data!)
                    CurrentProviderProfile.providerInfo = Status.data
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
   
            
    }
    
    func  getSignUp(){
        
        
        if CurrentUser.typeSelect == "client"{
            
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.registerUser,parameters: parametersUser).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseUser.self, from: response.data!)
                    CurrentUser.userInfo = Status.data
                    if CurrentUser.userInfo != nil{
                        FCM.handelTopicNotification()
                        self.getProfile()
                        let vc = AlertActivieCodeVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }else{
            
            WebRequests.sendPostMultipartRequestWith2ImgParam(api: APIRouter.registerProvider,parameters: parametersProvider, img: imageidentification ?? UIImage(), withName: "identification_photo", img2: imagelicence ?? UIImage(), withName2: "licence", completion: { (response, error) in
                
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(ResponseProvider.self, from: response.data!)
                    CurrentProvider.providerInfo = Status.data
                    if CurrentProvider.providerInfo != nil{
                        FCM.handelTopicNotification()
                        self.getProfile()
                        let vc = AlertActivieCodeVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            })
            
            
        }
        
    }
    
    
    
    @IBAction func didTab_Check(_ sender: UIButton) {
        
        
        guard let codesms = self.codeNumber, !codesms.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Code is Not Vaild!".localized)
            return
        }
        
        
        
        print(codesms)
        
        self.showIndicator()
        
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "",
                                                                 verificationCode: codesms )
        Auth.auth().signIn(with: credential) { authData, error in
            self.hideIndicator()
            
            if ((error) != nil) {
                self.alert(message:error!.localizedDescription)
                
                return
            }else{
                print("SUCCESS")
                switch self.Typeverification {
                case .signUp:
                    self.getSignUp()
                case .forgatepassword:
                    self.goToReset()
                }
            }
        }
        
    }
    
    @IBAction func didTab_Edite(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
