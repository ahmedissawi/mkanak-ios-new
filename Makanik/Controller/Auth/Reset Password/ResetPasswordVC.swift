//
//  ResetPasswordVC.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit

class ResetPasswordVC: SuperViewController {
    
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    var mobileActivie = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func didTab_Send(_ sender: UIButton) {
        
        
        guard let newpassword = self.tfNewPassword.text, !newpassword.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your new password".localized)
            return
        }
        
        guard let confirampassword = self.tfConfirmPassword.text, !confirampassword.isEmpty else{
            self.showAlert(title: "Alert!".localized, message: "Please enter your confiram password".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        parameters["mobile"] = mobileActivie
        parameters["password"] = newpassword
        parameters["password_confirmation"] = confirampassword
        
        if CurrentUser.typeSelect == "client"{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.resetpasswordUser,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        let vc:LoginVC = LoginVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(api: APIRouter.resetpasswordProvider,parameters: parameters).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "Alert!".localized, message: Status.message ?? "")
                        return
                    }else if Status.status == 200{
                        let vc:LoginVC = LoginVC.loadFromNib()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                
            }
        }
        
        
    }
    
    
    
}
