//
//  MyraidProSemiBold.swift
//  BayanPay
//
//  Created by Ketan Bhangale on 26/09/18.
//  Copyright © 2018 Shaileshk Dodia. All rights reserved.
//

import UIKit


class NeoSansArabic: UILabel {


    @IBInspectable var fontSize: CGFloat {
        get {
            return 26.0
        }
        set {
            self.font = UIFont.NeoSansArabic(ofSize: newValue)

        }
    }
    @IBInspectable var numberLines: Int {
        get {
            return 0
        }
        set {
            self.numberOfLines = newValue
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        allocInit()
    }
    
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        allocInit()
    }
    
    func allocInit(){
        self.font = UIFont.NeoSansArabic(ofSize: self.fontSize)
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
  
}
class FFShamelMediumLbl: UILabel {


    @IBInspectable var fontSize: CGFloat {
        get {
            return 26.0
        }
        set {
            self.font = UIFont.FFShamelFamilySemiRoundMedium(ofSize: newValue)

        }
    }
    @IBInspectable var numberLines: Int {
        get {
            return 0
        }
        set {
            self.numberOfLines = newValue
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        allocInit()
    }
    
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        allocInit()
    }
    
    func allocInit(){
        self.font = UIFont.FFShamelFamilySemiRoundMedium(ofSize: self.fontSize)
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
  
}
//class GEFlowBold: UILabel {
//
//
//    @IBInspectable var fontSize: CGFloat {
//        get {
//            return 26.0
//        }
//        set {
//            self.font = UIFont.GEFlow.Bold.of(size: newValue)
//        }
//    }
//    @IBInspectable var numberLines: Int {
//        get {
//            return 0
//        }
//        set {
//            self.numberOfLines = newValue
//        }
//    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        allocInit()
//    }
//
//
//    required init?(coder aDecoder: NSCoder)
//    {
//        super.init(coder: aDecoder)
//        allocInit()
//    }
//
//    func allocInit(){
//        self.font = UIFont.GEFlow.Bold.of(size: self.fontSize)
//        self.numberOfLines = 0
//        self.lineBreakMode = .byWordWrapping
//    }
//
//}
class GEFlowEnBold: UILabel {


    @IBInspectable var fontSize: CGFloat {
        get {
            return 26.0
        }
        set {
            self.font = UIFont.boldSystemFont(ofSize: newValue)

        }
    }
    @IBInspectable var numberLines: Int {
        get {
            return 0
        }
        set {
            self.numberOfLines = newValue
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        allocInit()
    }
    
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        allocInit()
    }
    
    func allocInit(){
        self.font = UIFont.boldSystemFont(ofSize: self.fontSize)
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
  
}
//class GEFlowItalic: UILabel {
//
//
//    @IBInspectable var fontSize: CGFloat {
//        get {
//            return 26.0
//        }
//        set {
//            self.font = UIFont.GEFlow.Italic.of(size: newValue)
//        }
//    }
//    @IBInspectable var numberLines: Int {
//        get {
//            return 0
//        }
//        set {
//            self.numberOfLines = newValue
//        }
//    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        allocInit()
//    }
//
//
//    required init?(coder aDecoder: NSCoder)
//    {
//        super.init(coder: aDecoder)
//        allocInit()
//    }
//
//    func allocInit(){
//        self.font = UIFont.GEFlow.Italic.of(size: self.fontSize)
//        self.numberOfLines = 0
//        self.lineBreakMode = .byWordWrapping
//    }
//
//}



//class GEFlowRegular: UILabel {
//
//
//    @IBInspectable var fontSize: CGFloat {
//        get {
//            return 15.0
//        }
//        set {
//            self.font = UIFont.GEFlow.regular.of(size: newValue)
//        }
//    }
//    @IBInspectable var numberLines: Int {
//        get {
//            return 0
//        }
//        set {
//            self.numberOfLines = newValue
//        }
//    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        allocInit()
//    }
//
//
//    required init?(coder aDecoder: NSCoder)
//    {
//        super.init(coder: aDecoder)
//        allocInit()
//    }
//
//    func allocInit(){
//        self.font = UIFont.GEFlow.regular.of(size: self.fontSize)
//        self.numberOfLines = 0
//        self.lineBreakMode = .byWordWrapping
//    }
//
//}




extension UILabel{
    func localisedText(text: String){
        self.text = NSLocalizedString(text, comment: "")
    }
    
    func setLocalisedFormattedText(arabicText: String!, engText: String!){
        
        self.text = NSLocalizedString(arabicText, comment: engText)
        self.text = "\(self.text!), \(engText!) "

//        if Utility.isArabicLanguage(){
//            self.text = NSLocalizedString(arabicText, comment: "")
//            self.text = "\(self.text!), \(engText!)"
//        }else{
//            self.text = NSLocalizedString(arabicText, comment: "")
//            self.text = "\(self.text!), \(engText!)"
//        }
    }
}
  
//@IBDesignable
//class DoneButton: UIButton {
//
//    @IBInspectable var backGroundClr : UIColor? = .greenPublic {
//        didSet{
//            self.backgroundColor = backGroundClr
//        }
//    }
//
//    @IBInspectable var titleColour : UIColor? = .white {
//
//        didSet{
//            self.setTitleColor(titleColour, for: .normal)
//        }
//
//    }
//
//
//    @IBInspectable var fontSize: CGFloat {
//          get {
//            return 15.0
//          }
//          set {
//            self.titleLabel?.font = UIFont.TajawalFont.Regular.of(size: newValue)
//
//          }
//      }
//
//
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        allocInit()
//
//    }
//
//    override init(frame: CGRect) {
//         super.init(frame: frame)
//         allocInit()
//     }
//
//    func allocInit()  {
//        self.titleLabel?.font = UIFont.GEFlow.Bold.of(size: fontSize)
//        self.setTitleColor(.white, for: .normal)
//        self.backgroundColor = .greenPublic
//        self.layer.cornerRadius = 10
//
//
//    }
//
//}
