//
//  UIFont.swift
//  Alerts
//
//  Created by  Ahmed’s MacBook Pro on 11/10/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func NeoSansArabicBlack(ofSize: CGFloat) -> UIFont {
        
        return UIFont(name: "NeoSansArabic-Black", size: ofSize)!
    }
    
    static func NeoSansArabicLight(ofSize: CGFloat) -> UIFont {
        
        return UIFont(name: "NeoSansArabic-Light", size: ofSize)!
    }
    static func NeoSansArabicMedium(ofSize: CGFloat) -> UIFont {
        
        return UIFont(name: "NeoSansArabic-Medium", size: ofSize)!
    }
    static func NeoSansArabicUltra(ofSize: CGFloat) -> UIFont {
        
        return UIFont(name: "NeoSansArabic-Ultra", size: ofSize)!
    }
    
    static func NeoSansArabicBold(ofSize: CGFloat) -> UIFont {
        
        return UIFont(name: "NeoSansArabic-Bold", size: ofSize)!
    }
    static func NeoSansArabic(ofSize: CGFloat) -> UIFont {
        
        return UIFont(name: "NeoSansArabic", size: ofSize)!
    }
    
    static func FFShamelFamilySemiRoundBook(ofSize: CGFloat) -> UIFont {
  
        return UIFont(name: "FFShamelFamilySemiRoundBook-S", size: ofSize)!
    }
    
    static func FFShamelFamilySemiRoundMedium(ofSize: CGFloat) -> UIFont {
    
          return UIFont(name: "FFShamelFamily-SemiRoundMedium", size: ofSize)!
      }
}

struct AppFontName {
    static let regular = "CourierNewPSMT"
    static let bold = "CourierNewPS-BoldMT"
    static let italic = "CourierNewPS-ItalicMT"
}

extension UIFontDescriptor.AttributeName {
    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}

extension UIFont {
    static var isOverrided: Bool = false
    
    @objc class func mySystemFont(ofSize: CGFloat, weight: UIFont.Weight) -> UIFont {
        switch weight {
        case .semibold, .bold, .heavy, .black:
            return UIFont(name: AppFontName.italic, size: ofSize)!
            
        case .medium, .regular:
            return UIFont(name: AppFontName.italic, size: ofSize)!
            
        default:
            return UIFont(name: AppFontName.italic, size: ofSize)!
        }
    }
    
    
    
    @objc convenience init(myCoder aDecoder: NSCoder) {
        guard
            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
            self.init(myCoder: aDecoder)
            return
        }
        var fontName = ""
        switch fontAttribute {
        case "CTFontRegularUsage":
            fontName = AppFontName.regular
        case "CTFontEmphasizedUsage", "CTFontBoldUsage":
            fontName = AppFontName.bold
        case "CTFontObliqueUsage":
            fontName = AppFontName.italic
        default:
            fontName = AppFontName.regular
        }
        self.init(name: fontName, size: fontDescriptor.pointSize)!
    }
    
    class func overrideInitialize() {
        guard self == UIFont.self, !isOverrided else { return }
        
        // Avoid method swizzling run twice and revert to original initialize function
        isOverrided = true
        
        if let systemFontMethodWithWeight = class_getClassMethod(self, #selector(systemFont(ofSize: weight:))),
           let mySystemFontMethodWithWeight = class_getClassMethod(self, #selector(mySystemFont(ofSize: weight:))) {
            method_exchangeImplementations(systemFontMethodWithWeight, mySystemFontMethodWithWeight)
        }
        
        
        
        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))), // Trick to get over the lack of UIFont.init(coder:))
           let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
        }
    }
}
