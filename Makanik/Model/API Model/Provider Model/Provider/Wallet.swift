//
//  Wallet.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Wallet: Codable {

  enum CodingKeys: String, CodingKey {
    case outstandingBalance = "outstanding_balance"
    case id
    case availableBalance = "available_balance"
  }

  var outstandingBalance: Int?
  var id: Int?
  var availableBalance: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.outstandingBalance) {
 outstandingBalance = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.outstandingBalance) {
outstandingBalance = value 
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(String.self, forKey:.availableBalance) {
 availableBalance = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.availableBalance) {
availableBalance = value 
}
  }

}
