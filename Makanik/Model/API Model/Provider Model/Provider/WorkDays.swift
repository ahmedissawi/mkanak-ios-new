//
//  WorkDays.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct WorkDays: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case fromTime = "from_time"
    case day
    case toTime = "to_time"
  }

  var id: Int?
  var fromTime: String?
  var day: Int?
  var toTime: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.fromTime) {                       
fromTime = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.fromTime) {
 fromTime = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.day) {
 day = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.day) {
day = value 
}
    if let value = try? container.decode(Int.self, forKey:.toTime) {                       
toTime = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.toTime) {
 toTime = value                                                                                     
}
  }

}
