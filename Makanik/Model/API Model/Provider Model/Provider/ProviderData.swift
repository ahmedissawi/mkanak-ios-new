//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProviderData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case address
        case lng
        case image
        case local
        case shortCountry = "short_country"
        case type
        case name
        case id
        case rating
        case notification
        case lat
        case email
        case identificationNumber = "identification_number"
        case accessToken = "access_token"
        case active
        case wallet
        case mobile
        case licence
        case identificationPhoto = "identification_photo"
        case workDays = "work_days"
        case countryCode = "country_code"
        case gender
    }
    
    var address: String?
    var lng: Float?
    var image: String?
    var local: String?
    var shortCountry: String?
    var type: String?
    var name: String?
    var id: Int?
    var rating: Int?
    var notification: Int?
    var lat: Float?
    var email: String?
    var identificationNumber: String?
    var accessToken: String?
    var active: Int?
    var wallet: Wallet?
    var mobile: String?
    var licence: String?
    var identificationPhoto: String?
    var workDays: [WorkDays]?
    var countryCode: String?
    var gender: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(Int.self, forKey:.lng){
            lng = Float(value)
        }else if let value = try? container.decode(String.self, forKey:.lng){
            lng = Float(value)
        }else{
            if let value = try? container.decode(Float.self, forKey:.lng){
                lng = value
            }
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(Int.self, forKey:.local) {
            local = String(value)
        }else if let value = try? container.decode(String.self, forKey:.local) {
            local = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortCountry) {
            shortCountry = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortCountry) {
            shortCountry = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.rating) {
            rating = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.rating) {
            rating = value
        }
        if let value = try? container.decode(String.self, forKey:.notification) {
            notification = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.notification) {
            notification = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.lat){
            lat = Float(value)
        }else if let value = try? container.decode(String.self, forKey:.lat){
            lat = Float(value)
        }else{
            if let value = try? container.decode(Float.self, forKey:.lat){
                lat = value
            }
        }
        
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationNumber) {
            identificationNumber = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationNumber) {
            identificationNumber = value
        }
        if let value = try? container.decode(Int.self, forKey:.accessToken) {
            accessToken = String(value)
        }else if let value = try? container.decode(String.self, forKey:.accessToken) {
            accessToken = value
        }
        if let value = try? container.decode(String.self, forKey:.active) {
            active = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.active) {
            active = value
        }
        wallet = try container.decodeIfPresent(Wallet.self, forKey: .wallet)
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.licence) {
            licence = String(value)
        }else if let value = try? container.decode(String.self, forKey:.licence) {
            licence = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationPhoto) {
            identificationPhoto = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationPhoto) {
            identificationPhoto = value
        }
        workDays = try container.decodeIfPresent([WorkDays].self, forKey: .workDays)
        if let value = try? container.decode(Int.self, forKey:.countryCode) {
            countryCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.countryCode) {
            countryCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.gender) {
            gender = String(value)
        }else if let value = try? container.decode(String.self, forKey:.gender) {
            gender = value
        }
    }
    
}
