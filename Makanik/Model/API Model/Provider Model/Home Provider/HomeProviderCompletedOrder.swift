//
//  CompletedOrder.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct HomeProviderCompletedOrder: Codable {
    
    enum CodingKeys: String, CodingKey {
        case priority
        case type
        case createdAt = "created_at"
        case lng
        case id
        case lat
        case paymentType = "payment_type"
        case paid
        case price
        case service
        case user
        case descriptionValue = "description"
        case mechanic
        case images
        case uuid
        case status
        case minOffer

    }
    
    var price: Int?
    var priority: String?
    var type: String?
    var createdAt: String?
    var lng: String?
    var id: Int?
    var lat: String?
    var paymentType: String?
    var paid: String?
    var service: String?
    var user: ProviderUser?
    var descriptionValue: String?
    var mechanic: Mechanic?
    var images: [Images]?
    var uuid: String?
    var status: String?
    var minOffer: Double?

    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.priority) {
            priority = String(value)
        }else if let value = try? container.decode(String.self, forKey:.priority) {
            priority = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lng) {
            lng = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lat) {
            lat = value
        }
        if let value = try? container.decode(Int.self, forKey:.paymentType) {
            paymentType = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paymentType) {
            paymentType = value
        }
        if let value = try? container.decode(Int.self, forKey:.paid) {
            paid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paid) {
            paid = value
        }
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(Int.self, forKey:.service) {
            service = String(value)
        }else if let value = try? container.decode(String.self, forKey:.service) {
            service = value
        }
        user = try container.decodeIfPresent(ProviderUser.self, forKey: .user)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        images = try container.decodeIfPresent([Images].self, forKey: .images)
        if let value = try? container.decode(Int.self, forKey:.uuid) {
            uuid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.uuid) {
            uuid = value
        }
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(String.self, forKey:.minOffer) {
            minOffer = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.minOffer) {
            minOffer = value
        }else{
            let value = try? container.decode(Int.self, forKey:.minOffer)
            minOffer = Double(value ?? 0)
        }
    }
    
}
