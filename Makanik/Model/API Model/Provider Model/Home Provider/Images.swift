//
//  Images.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Images: Codable {

  enum CodingKeys: String, CodingKey {
    case image
    case id
  }

  var image: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
