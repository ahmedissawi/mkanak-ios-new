//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct HomeProviderResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case success
        case message
        case data
    }
    
    var status: Int?
    var success: Bool?
    var message: String?
    var data: HomeProviderData?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        data = try container.decodeIfPresent(HomeProviderData.self, forKey: .data)
    }
    
}
