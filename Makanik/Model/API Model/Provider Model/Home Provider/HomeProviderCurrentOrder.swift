//
//  CurrentOrder.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct HomeProviderCurrentOrder: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case id
        case images
        case priority
        case user
        case mechanic
        case schedulingDate = "scheduling_date"
        case paymentType = "payment_type"
        case uuid
        case minOffer
        case service
        case descriptionValue = "description"
        case createdAt = "created_at"
        case lat
        case paid
        case lng
        case type
        case price
    }
    
    var status: String?
    var id: Int?
    var images: [Images]?
    var priority: String?
    var user: ProviderUser?
    var mechanic: Mechanic?
    var schedulingDate: String?
    var paymentType: String?
    var uuid: String?
    var minOffer: Double?
    var service: String?
    var descriptionValue: String?
    var createdAt: String?
    var lat: String?
    var paid: String?
    var lng: String?
    var type: String?
    var price: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        images = try container.decodeIfPresent([Images].self, forKey: .images)
        if let value = try? container.decode(Int.self, forKey:.priority) {
            priority = String(value)
        }else if let value = try? container.decode(String.self, forKey:.priority) {
            priority = value
        }
        user = try container.decodeIfPresent(ProviderUser.self, forKey: .user)
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        if let value = try? container.decode(Int.self, forKey:.schedulingDate) {
            schedulingDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.schedulingDate) {
            schedulingDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.paymentType) {
            paymentType = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paymentType) {
            paymentType = value
        }
        if let value = try? container.decode(Int.self, forKey:.uuid) {
            uuid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.uuid) {
            uuid = value
        }
        if let value = try? container.decode(String.self, forKey:.minOffer) {
            minOffer = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.minOffer) {
            minOffer = value
        }else{
            let value = try? container.decode(Int.self, forKey:.minOffer)
            minOffer = Double(value ?? 0)
        }
        if let value = try? container.decode(Int.self, forKey:.service) {
            service = String(value)
        }else if let value = try? container.decode(String.self, forKey:.service) {
            service = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lat) {
            lat = value
        }
        if let value = try? container.decode(Int.self, forKey:.paid) {
            paid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paid) {
            paid = value
        }
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lng) {
            lng = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.price) {
            price = String(value)
        }else if let value = try? container.decode(String.self, forKey:.price) {
            price = value
        }
    }
    
}
