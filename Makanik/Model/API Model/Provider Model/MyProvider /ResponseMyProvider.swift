//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResponseMyProvider: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case data
        case success
        case message
    }
    
    var status: Int?
    var data: MyProviderData?
    var success: Bool?
    var message: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        data = try container.decodeIfPresent(MyProviderData.self, forKey: .data)
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
    }
    
}
