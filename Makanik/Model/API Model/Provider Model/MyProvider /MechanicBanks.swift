//
//  MechanicBanks.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MechanicBanks: Codable {

  enum CodingKeys: String, CodingKey {
    case bankId = "bank_id"
    case deletedAt = "deleted_at"
    case mechanicId = "mechanic_id"
    case ownerName = "owner_name"
    case id
    case updatedAt = "updated_at"
    case accountNumber = "account_number"
    case createdAt = "created_at"
    case iBan = "i_ban"
  }

  var bankId: Int?
  var deletedAt: String?
  var mechanicId: Int?
  var ownerName: String?
  var id: Int?
  var updatedAt: String?
  var accountNumber: String?
  var createdAt: String?
  var iBan: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.bankId) {
 bankId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.bankId) {
bankId = value 
}
    if let value = try? container.decode(Int.self, forKey:.deletedAt) {                       
deletedAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.deletedAt) {
 deletedAt = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.mechanicId) {
 mechanicId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.mechanicId) {
mechanicId = value 
}
    if let value = try? container.decode(Int.self, forKey:.ownerName) {                       
ownerName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.ownerName) {
 ownerName = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.updatedAt) {                       
updatedAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.updatedAt) {
 updatedAt = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.accountNumber) {                       
accountNumber = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.accountNumber) {
 accountNumber = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.createdAt) {                       
createdAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.createdAt) {
 createdAt = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.iBan) {                       
iBan = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.iBan) {
 iBan = value                                                                                     
}
  }

}
