//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MyProviderData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case rating
        case tradeId = "trade_id"
        case lat
        case identificationNumber = "identification_number"
        case email
        case type
        case workDays = "work_days"
        case mechanicBanks = "mechanic_banks"
        case lng
        case address
        case name
        case image
        case licence
        case timezone
        case countryCode = "country_code"
        case identificationPhoto = "identification_photo"
        case local
        case mobile
        case notification
        case id
        case shortCountry = "short_country"
        case gender
        case accessToken = "access_token"
        case active
        case medals
    }
    
    var rating: Int?
    var tradeId: String?
    var lat: String?
    var identificationNumber: String?
    var email: String?
    var type: String?
    var workDays: [WorkDaysDeatilesProvider]?
    var mechanicBanks: [MechanicBanks]?
    var medals: Medals?
    var lng: String?
    var address: String?
    var name: String?
    var image: String?
    var licence: String?
    var timezone: String?
    var countryCode: String?
    var identificationPhoto: String?
    var local: String?
    var mobile: String?
    var notification: Int?
    var id: Int?
    var shortCountry: String?
    var gender: String?
    var accessToken: String?
    var active: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.rating) {
            rating = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.rating) {
            rating = value
        }
        if let value = try? container.decode(Int.self, forKey:.tradeId) {
            tradeId = String(value)
        }else if let value = try? container.decode(String.self, forKey:.tradeId) {
            tradeId = value
        }
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lat) {
            lat = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationNumber) {
            identificationNumber = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationNumber) {
            identificationNumber = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        workDays = try container.decodeIfPresent([WorkDaysDeatilesProvider].self, forKey: .workDays)
        mechanicBanks = try container.decodeIfPresent([MechanicBanks].self, forKey: .mechanicBanks)
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lng) {
            lng = value
        }
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(Int.self, forKey:.licence) {
            licence = String(value)
        }else if let value = try? container.decode(String.self, forKey:.licence) {
            licence = value
        }
        if let value = try? container.decode(Int.self, forKey:.timezone) {
            timezone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.timezone) {
            timezone = value
        }
        if let value = try? container.decode(Int.self, forKey:.countryCode) {
            countryCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.countryCode) {
            countryCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationPhoto) {
            identificationPhoto = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationPhoto) {
            identificationPhoto = value
        }
        if let value = try? container.decode(Int.self, forKey:.local) {
            local = String(value)
        }else if let value = try? container.decode(String.self, forKey:.local) {
            local = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(String.self, forKey:.notification) {
            notification = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.notification) {
            notification = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortCountry) {
            shortCountry = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortCountry) {
            shortCountry = value
        }
        if let value = try? container.decode(Int.self, forKey:.gender) {
            gender = String(value)
        }else if let value = try? container.decode(String.self, forKey:.gender) {
            gender = value
        }
        if let value = try? container.decode(Int.self, forKey:.accessToken) {
            accessToken = String(value)
        }else if let value = try? container.decode(String.self, forKey:.accessToken) {
            accessToken = value
        }
        if let value = try? container.decode(String.self, forKey:.active) {
            active = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.active) {
            active = value
        }
        medals = try container.decodeIfPresent(Medals.self, forKey: .medals)
    }
    
}
