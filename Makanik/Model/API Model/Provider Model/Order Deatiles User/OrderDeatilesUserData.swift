//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderDeatilesUserData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case user
        case service
        case uuid
        case offers
        case paymentType = "payment_type"
        case lng
        case id
        case priority
        case createdAt = "created_at"
        case descriptionValue = "description"
        case mechanic
        case lat
        case type
        case paid
        case status
        case serviceIcon = "service_icon"
        case minOffer
        case price
        case address
        case images
        case isRating = "is_rating"
        case rating
        case myOffer = "my_offer"

    }
    
    var user: ProviderUser?
    var service: String?
    var uuid: String?
    var offers: [Offers]?
    var paymentType: String?
    var lng: Float?
    var id: Int?
    var priority: String?
    var createdAt: String?
    var descriptionValue: String?
    var mechanic: Mechanic?
    var lat: Float?
    var type: String?
    var paid: Int?
    var status: String?
    var serviceIcon: String?
    var minOffer: Double?
    var price: String?
    var address: String?
    var images: [Images]?
    var isRating: Bool?
    var rating: Rating?
    var myOffer: MyOffer?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        user = try container.decodeIfPresent(ProviderUser.self, forKey: .user)
        images = try container.decodeIfPresent([Images].self, forKey: .images)
        if let value = try? container.decode(Int.self, forKey:.service) {
            service = String(value)
        }else if let value = try? container.decode(String.self, forKey:.service) {
            service = value
        }
        if let value = try? container.decode(Int.self, forKey:.uuid) {
            uuid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.uuid) {
            uuid = value
        }
        offers = try container.decodeIfPresent([Offers].self, forKey: .offers)
        if let value = try? container.decode(Int.self, forKey:.paymentType) {
            paymentType = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paymentType) {
            paymentType = value
        }
        if let value = try? container.decodeIfPresent(Float.self, forKey: .lng){
            lng = value
            
        }else if let value = try? container.decodeIfPresent(String.self, forKey: .lng){
            lng = Float(value)
        }else{
            if let value = try? container.decodeIfPresent(Int.self, forKey: .lng){
                lng = Float(value)
            }
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.priority) {
            priority = String(value)
        }else if let value = try? container.decode(String.self, forKey:.priority) {
            priority = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        myOffer = try container.decodeIfPresent(MyOffer.self, forKey: .myOffer)
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        if let value = try? container.decodeIfPresent(Float.self, forKey: .lat){
            lat = value
            
        }else if let value = try? container.decodeIfPresent(String.self, forKey: .lat){
            lat = Float(value)
        }else{
            if let value = try? container.decodeIfPresent(Int.self, forKey: .lat){
                lat = Float(value)
            }
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(String.self, forKey:.paid) {
            paid = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.paid) {
            paid = value
        }
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.serviceIcon) {
            serviceIcon = String(value)
        }else if let value = try? container.decode(String.self, forKey:.serviceIcon) {
            serviceIcon = value
        }
        if let value = try? container.decode(String.self, forKey:.minOffer) {
            minOffer = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.minOffer) {
            minOffer = value
        }else{
            let value = try? container.decode(Int.self, forKey:.minOffer)
            minOffer = Double(value ?? 0)
        }
        if let value = try? container.decode(Int.self, forKey:.price) {
            price = String(value)
        }else if let value = try? container.decode(String.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        isRating = try container.decodeIfPresent(Bool.self, forKey: .isRating)
        rating = try container.decodeIfPresent(Rating.self, forKey: .rating)
    }
    
}
