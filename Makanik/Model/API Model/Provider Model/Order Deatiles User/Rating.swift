//
//  Rating.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Rating: Codable {
    
    enum CodingKeys: String, CodingKey {
        case comment
        case rating
        case createdAt = "created_at"
        case mechanic
        case id
        case user
    }
    
    var comment: String?
    var rating: Int?
    var createdAt: String?
    var mechanic: Mechanic?
    var id: Int?
    var user: ProviderUser?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.comment) {                       
            comment = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.comment) {
            comment = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.rating) {
            rating = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.rating) {
            rating = value 
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {                       
            createdAt = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value                                                                                     
        }
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
        user = try container.decodeIfPresent(ProviderUser.self, forKey: .user)
    }
    
}
