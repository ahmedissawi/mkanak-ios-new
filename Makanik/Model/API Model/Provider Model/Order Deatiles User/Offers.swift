//
//  Offers.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Offers: Codable {
    
    enum CodingKeys: String, CodingKey {
        case descriptionValue = "description"
        case status
        case mechanic
        case price
        case id
        case orderId = "order_id"
        case createdAt = "created_at"
    }
    
    var descriptionValue: String?
    var status: Int?
    var mechanic: Mechanic?
    var price: Double?
    var id: Int?
    var orderId: Int?
    var createdAt: String?

    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Double(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = Double(value)
        }else{
            let value = try? container.decode(Double.self, forKey:.price)
                price = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.orderId) {
            orderId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.orderId) {
            orderId = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
    }
    
}
