//
//  MyOffer.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/28/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MyOffer: Codable {
    
    enum CodingKeys: String, CodingKey {
        case orderId = "order_id"
        case mechanic
        case price
        case createdAt = "created_at"
        case id
        case status
        case descriptionValue = "description"
        case canEdit = "can_edit"
        
    }
    
    var orderId: Int?
    var mechanic: Mechanic?
    var price: Int?
    var createdAt: String?
    var id: Int?
    var status: Int?
    var descriptionValue: String?
    var canEdit: Bool?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.orderId) {
            orderId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.orderId) {
            orderId = value
        }
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        canEdit = try container.decodeIfPresent(Bool.self, forKey: .canEdit)
        
    }
    
}
