//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderDeatilesUserResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case data
        case status
        case message
        case success
    }
    
    var data: OrderDeatilesUserData?
    var status: Int?
    var message: String?
    var success: Bool?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decodeIfPresent(OrderDeatilesUserData.self, forKey: .data)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
    }
    
}
