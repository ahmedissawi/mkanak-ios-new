//
//  Items.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderProviderItems: Codable {
    
    enum CodingKeys: String, CodingKey {
        case schedulingDate = "scheduling_date"
        case createdAt = "created_at"
        case serviceIcon = "service_icon"
        case minOffer
        case service
        case priority
        case offers
        case id
        case uuid
        case lat
        case paymentType = "payment_type"
        case status
        case images
        case mechanic
        case price
        case user
        case lng
        case descriptionValue = "description"
        case paid
        case type
    }
    
    var schedulingDate: String?
    var createdAt: String?
    var minOffer: Double?
    var service: String?
    var priority: String?
    var id: Int?
    var uuid: String?
    var lat: String?
    var paymentType: String?
    var status: String?
    var images: [Images]?
    var mechanic: Mechanic?
    var price: Int?
    var user: ProviderUser?
    var lng: String?
    var descriptionValue: String?
    var paid: String?
    var type: String?
    var offers: [Offers]?
    var serviceIcon: String?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.schedulingDate) {
            schedulingDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.schedulingDate) {
            schedulingDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.service) {
            service = String(value)
        }else if let value = try? container.decode(String.self, forKey:.service) {
            service = value
        }
        if let value = try? container.decode(Int.self, forKey:.priority) {
            priority = String(value)
        }else if let value = try? container.decode(String.self, forKey:.priority) {
            priority = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.uuid) {
            uuid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.uuid) {
            uuid = value
        }
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lat) {
            lat = value
        }
        if let value = try? container.decode(Int.self, forKey:.paymentType) {
            paymentType = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paymentType) {
            paymentType = value
        }
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value
        }
        offers = try container.decodeIfPresent([Offers].self, forKey: .offers)
        images = try container.decodeIfPresent([Images].self, forKey: .images)
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        user = try container.decodeIfPresent(ProviderUser.self, forKey: .user)
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lng) {
            lng = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(Int.self, forKey:.paid) {
            paid = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paid) {
            paid = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(String.self, forKey:.minOffer) {
            minOffer = Double(value)
        } else if let value = try? container.decode(Double.self, forKey:.minOffer) {
            minOffer = value
        }else{
            let value = try? container.decode(Int.self, forKey:.minOffer)
            minOffer = Double(value ?? 0)
        }
        if let value = try? container.decode(Int.self, forKey:.serviceIcon) {
            serviceIcon = String(value)
        }else if let value = try? container.decode(String.self, forKey:.serviceIcon) {
            serviceIcon = value
        }
    }
    
}
