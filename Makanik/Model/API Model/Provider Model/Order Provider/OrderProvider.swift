//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderProvider: Codable {

  enum CodingKeys: String, CodingKey {
    case items
    case paginate
    case orderCount = "order_count"
  }

  var items: [OrderProviderItems]?
  var paginate: Paginate?
  var orderCount: OrderCount?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    items = try container.decodeIfPresent([OrderProviderItems].self, forKey: .items)
    paginate = try container.decodeIfPresent(Paginate.self, forKey: .paginate)
    orderCount = try container.decodeIfPresent(OrderCount.self, forKey: .orderCount)
  }

}
