//
//  OrderCount.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderCount: Codable {

  enum CodingKeys: String, CodingKey {
    case currentOrderCount = "current_order_count"
    case completedOrderCount = "completed_order_count"
    case pendingOrderCount = "pending_order_count"
    case rejectedOrderCount = "rejected_order_count"
  }

  var currentOrderCount: Int?
  var completedOrderCount: Int?
  var pendingOrderCount: Int?
  var rejectedOrderCount: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.currentOrderCount) {
 currentOrderCount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.currentOrderCount) {
currentOrderCount = value 
}
    if let value = try? container.decode(String.self, forKey:.completedOrderCount) {
 completedOrderCount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.completedOrderCount) {
completedOrderCount = value 
}
    if let value = try? container.decode(String.self, forKey:.pendingOrderCount) {
 pendingOrderCount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.pendingOrderCount) {
pendingOrderCount = value 
}
    if let value = try? container.decode(String.self, forKey:.rejectedOrderCount) {
 rejectedOrderCount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.rejectedOrderCount) {
rejectedOrderCount = value 
}
  }

}
