//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/26/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ServicesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case price
        case id
        case service
        case type
    }
    
    var price: Int?
    var id: Int?
    var service: Service?
    var type: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        service = try container.decodeIfPresent(Service.self, forKey: .service)
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
    }
    
}
