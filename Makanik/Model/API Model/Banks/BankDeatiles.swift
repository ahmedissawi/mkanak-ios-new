//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/26/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BankDeatiles: Codable {
    
    enum CodingKeys: String, CodingKey {
        case accountNumber = "account_number"
        case mechanicId = "mechanic_id"
        case bank
        case iBan = "i_ban"
        case ownerName = "owner_name"
        case id
    }
    
    var accountNumber: String?
    var mechanicId: Int?
    var bank: Bank?
    var iBan: String?
    var ownerName: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.accountNumber) {
            accountNumber = String(value)
        }else if let value = try? container.decode(String.self, forKey:.accountNumber) {
            accountNumber = value
        }
        if let value = try? container.decode(String.self, forKey:.mechanicId) {
            mechanicId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.mechanicId) {
            mechanicId = value
        }
        bank = try container.decodeIfPresent(Bank.self, forKey: .bank)
        if let value = try? container.decode(Int.self, forKey:.iBan) {
            iBan = String(value)
        }else if let value = try? container.decode(String.self, forKey:.iBan) {
            iBan = value
        }
        if let value = try? container.decode(Int.self, forKey:.ownerName) {
            ownerName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.ownerName) {
            ownerName = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    }
