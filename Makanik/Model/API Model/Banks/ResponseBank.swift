//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/26/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResponseBank: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case data
        case success
        case message
    }
    
    var status: Int?
    var data: [BankData]?
    var success: Bool?
    var message: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        data = try container.decodeIfPresent([BankData].self, forKey: .data)
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
    }
    
}
