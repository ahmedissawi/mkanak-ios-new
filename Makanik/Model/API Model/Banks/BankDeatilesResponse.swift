//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/26/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BankDeatilesResponse: Codable {

  enum CodingKeys: String, CodingKey {
    case message
    case status
    case success
    case data
  }

  var message: String?
  var status: Int?
  var success: Bool?
  var data: BankDeatiles?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    success = try container.decodeIfPresent(Bool.self, forKey: .success)
    data = try container.decodeIfPresent(BankDeatiles.self, forKey: .data)
  }

}
