//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 2/9/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Pages: Codable {
    
    enum CodingKeys: String, CodingKey {
        case key
        case id
        case content
        case name
    }
    
    var key: String?
    var id: Int?
    var content: String?
    var name: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.key) {
            key = String(value)
        }else if let value = try? container.decode(String.self, forKey:.key) {
            key = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.content) {
            content = String(value)
        }else if let value = try? container.decode(String.self, forKey:.content) {
            content = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
    }
    
}
