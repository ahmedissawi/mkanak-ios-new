//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 2/9/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PagesResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case data
        case success
        case status
        case message
    }
    
    var data: Pages?
    var success: Bool?
    var status: Int?
    var message: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decodeIfPresent(Pages.self, forKey: .data)
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value 
        }
        if let value = try? container.decode(Int.self, forKey:.message) {                       
            message = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
    }
    
}
