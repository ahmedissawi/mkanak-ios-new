//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/23/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResponseResones: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case success
        case data
    }
    
    var status: Int?
    var message: String?
    var success: Bool?
    var data: [Resones]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        data = try container.decodeIfPresent([Resones].self, forKey: .data)
    }
    
}
