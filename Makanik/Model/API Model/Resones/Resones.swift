//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/23/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Resones: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case reason
    }
    
    var id: Int?
    var reason: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.reason) {
            reason = String(value)
        }else if let value = try? container.decode(String.self, forKey:.reason) {
            reason = value
        }
    }
    
}
