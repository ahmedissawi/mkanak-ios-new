//
//  Paginate.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/6/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Paginate: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lastPageUrl = "last_page_url"
        case total
        case perPage = "per_page"
        case from
        case firstPageUrl = "first_page_url"
        case nextPageUrl = "next_page_url"
        case lastPage = "last_page"
        case currentPage = "current_page"
        case to
    }
    
    var lastPageUrl: String?
    var total: Int?
    var perPage: Int?
    var from: Int?
    var firstPageUrl: String?
    var lastPage: Int?
    var currentPage: Int?
    var to: Int?
    var nextPageUrl: String?

    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {
            lastPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
            lastPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
        if let value = try? container.decode(String.self, forKey:.from) {
            from = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.from) {
            from = value
        }
        if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {
            firstPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
            firstPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.lastPage) {
            lastPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.lastPage) {
            lastPage = value
        }
        if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {
            nextPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
            nextPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.currentPage) {
            currentPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.currentPage) {
            currentPage = value
        }
        if let value = try? container.decode(String.self, forKey:.to) {
            to = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.to) {
            to = value
        }
    }
    
}
