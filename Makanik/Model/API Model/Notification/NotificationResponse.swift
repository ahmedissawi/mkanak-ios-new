//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/28/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationResponse: Codable {

  enum CodingKeys: String, CodingKey {
    case status
    case success
    case data
    case message
  }

  var status: Int?
  var success: Bool?
  var data: NotificationData?
  var message: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    success = try container.decodeIfPresent(Bool.self, forKey: .success)
    data = try container.decodeIfPresent(NotificationData.self, forKey: .data)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
  }

}
