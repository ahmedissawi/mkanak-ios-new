//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/28/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationData: Codable {

  enum CodingKeys: String, CodingKey {
    case items
    case paginate
  }

  var items: [NotificationItmes]?
  var paginate: Paginate?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    items = try container.decodeIfPresent([NotificationItmes].self, forKey: .items)
    paginate = try container.decodeIfPresent(Paginate.self, forKey: .paginate)
  }

}
