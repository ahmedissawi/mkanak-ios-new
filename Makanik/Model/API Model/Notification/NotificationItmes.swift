//
//  Items.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/28/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationItmes: Codable {
    
    enum CodingKeys: String, CodingKey {
        case message
        case id
        case title
        case type
        case createdAt = "created_at"
        case others
        case seen
    }
    
    var message: String?
    var id: String?
    var title: String?
    var type: String?
    var createdAt: String?
    var others: NotificationOthers?
    var seen: Bool?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(Int.self, forKey:.id) {
            id = String(value)
        }else if let value = try? container.decode(String.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        others = try container.decodeIfPresent(NotificationOthers.self, forKey: .others)
        seen = try container.decodeIfPresent(Bool.self, forKey: .seen)
    }
    
}
