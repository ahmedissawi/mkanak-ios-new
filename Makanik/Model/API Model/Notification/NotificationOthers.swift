//
//  Others.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/28/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationOthers: Codable {

  enum CodingKeys: String, CodingKey {
    case orderId = "order_id"
  }

  var orderId: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.orderId) {
 orderId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.orderId) {
orderId = value 
}
  }

}
