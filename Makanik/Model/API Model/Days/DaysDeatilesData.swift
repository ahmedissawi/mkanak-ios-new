//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DaysDeatilesData: Codable {

  enum CodingKeys: String, CodingKey {
    case day
    case id
    case toTime = "to_time"
    case fromTime = "from_time"
    case dayName = "day_name"
  }

  var day: Int?
  var id: Int?
  var toTime: String?
  var fromTime: String?
  var dayName: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.day) {
 day = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.day) {
day = value 
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.toTime) {                       
toTime = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.toTime) {
 toTime = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.fromTime) {                       
fromTime = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.fromTime) {
 fromTime = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.dayName) {                       
dayName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.dayName) {
 dayName = value                                                                                     
}
  }

}
