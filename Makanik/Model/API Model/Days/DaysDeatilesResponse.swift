//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DaysDeatilesResponse: Codable {

  enum CodingKeys: String, CodingKey {
    case status
    case message
    case data
    case success
  }

  var status: Int?
  var message: String?
  var data: DaysDeatilesData?
  var success: Bool?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    data = try container.decodeIfPresent(DaysDeatilesData.self, forKey: .data)
    success = try container.decodeIfPresent(Bool.self, forKey: .success)
  }

}
