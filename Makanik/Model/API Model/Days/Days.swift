//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Days: Codable {

  enum CodingKeys: String, CodingKey {
    case num
    case day
  }

  var num: Int?
  var day: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.num) {
 num = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.num) {
num = value 
}
    if let value = try? container.decode(Int.self, forKey:.day) {                       
day = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.day) {
 day = value                                                                                     
}
  }

}
