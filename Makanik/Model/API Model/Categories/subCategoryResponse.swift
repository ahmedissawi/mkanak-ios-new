//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct subCategoryResponse: Codable {

  enum CodingKeys: String, CodingKey {
    case message
    case data
    case success
    case status
  }

  var message: String?
  var data: [subCategoryData]?
  var success: Bool?
  var status: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    data = try container.decodeIfPresent([subCategoryData].self, forKey: .data)
    success = try container.decodeIfPresent(Bool.self, forKey: .success)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
  }

}
