//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/27/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CategoriesResponse: Codable {

  enum CodingKeys: String, CodingKey {
    case message
    case data
    case status
    case success
  }

  var message: String?
  var data: [Categories]?
  var status: Int?
  var success: Bool?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    data = try container.decodeIfPresent([Categories].self, forKey: .data)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    success = try container.decodeIfPresent(Bool.self, forKey: .success)
  }

}
