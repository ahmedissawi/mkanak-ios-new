//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Settings: Codable {
    
    enum CodingKeys: String, CodingKey {
        case searchDistance = "search_distance"
        case tax
        case adsPrice = "ads_price"
        case walletPay = "wallet_pay"
        case youtube
        case logoMin = "logo_min"
        case logo
        case distinctionPrice = "distinction_price"
        case iosUrl = "ios_url"
        case facebook
        case timeToCancelOrder = "time_to_cancel_order"
        case cachePay = "cache_pay"
        case email
        case onlinePay = "online_pay"
        case name
        case twitter
        case instagram
        case androidUrl = "android_url"
        case mobile
        case whatsApp
        case mechanicOffer = "mechanic_offer"
        case showInput = "show_input"

    }
    
    var searchDistance: Int?
    var tax: Int?
    var adsPrice: Int?
    var walletPay: Int?
    var youtube: String?
    var logoMin: String?
    var logo: String?
    var distinctionPrice: Int?
    var iosUrl: String?
    var facebook: String?
    var timeToCancelOrder: Int?
    var cachePay: Int?
    var email: String?
    var onlinePay: Int?
    var name: String?
    var twitter: String?
    var instagram: String?
    var androidUrl: String?
    var mobile: String?
    var whatsApp: String?
    var mechanicOffer: Int?
    var showInput: Int?

    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.searchDistance) {
            searchDistance = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.searchDistance) {
            searchDistance = value
        }
        if let value = try? container.decode(String.self, forKey:.tax) {
            tax = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.tax) {
            tax = value
        }
        if let value = try? container.decode(String.self, forKey:.adsPrice) {
            adsPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.adsPrice) {
            adsPrice = value
        }
        if let value = try? container.decode(String.self, forKey:.walletPay) {
            walletPay = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.walletPay) {
            walletPay = value
        }
        if let value = try? container.decode(Int.self, forKey:.youtube) {
            youtube = String(value)
        }else if let value = try? container.decode(String.self, forKey:.youtube) {
            youtube = value
        }
        if let value = try? container.decode(Int.self, forKey:.logoMin) {
            logoMin = String(value)
        }else if let value = try? container.decode(String.self, forKey:.logoMin) {
            logoMin = value
        }
        if let value = try? container.decode(Int.self, forKey:.logo) {
            logo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.logo) {
            logo = value
        }
        if let value = try? container.decode(String.self, forKey:.distinctionPrice) {
            distinctionPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.distinctionPrice) {
            distinctionPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.iosUrl) {
            iosUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.iosUrl) {
            iosUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.facebook) {
            facebook = String(value)
        }else if let value = try? container.decode(String.self, forKey:.facebook) {
            facebook = value
        }
        if let value = try? container.decode(String.self, forKey:.timeToCancelOrder) {
            timeToCancelOrder = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.timeToCancelOrder) {
            timeToCancelOrder = value
        }
        if let value = try? container.decode(String.self, forKey:.cachePay) {
            cachePay = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.cachePay) {
            cachePay = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(String.self, forKey:.onlinePay) {
            onlinePay = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.onlinePay) {
            onlinePay = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.twitter) {
            twitter = String(value)
        }else if let value = try? container.decode(String.self, forKey:.twitter) {
            twitter = value
        }
        if let value = try? container.decode(Int.self, forKey:.instagram) {
            instagram = String(value)
        }else if let value = try? container.decode(String.self, forKey:.instagram) {
            instagram = value
        }
        if let value = try? container.decode(Int.self, forKey:.androidUrl) {
            androidUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.androidUrl) {
            androidUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.whatsApp) {
            whatsApp = String(value)
        }else if let value = try? container.decode(String.self, forKey:.whatsApp) {
            whatsApp = value
        }
        if let value = try? container.decode(String.self, forKey:.mechanicOffer) {
            mechanicOffer = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.mechanicOffer) {
            mechanicOffer = value
        }
        if let value = try? container.decode(String.self, forKey:.showInput) {
            showInput = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.showInput) {
            showInput = value
        }
    }
    

    
}
