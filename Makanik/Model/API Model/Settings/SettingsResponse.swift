//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SettingsResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case success
        case message
        case status
        case data
    }
    
    var success: Bool?
    var message: String?
    var status: Int?
    var data: Settings?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(Int.self, forKey:.message) {                       
            message = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value 
        }
        data = try container.decodeIfPresent(Settings.self, forKey: .data)
    }
    
}
