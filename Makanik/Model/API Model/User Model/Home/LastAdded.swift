//
//  LastAdded.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct LastAdded: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case id
    case subCategory = "sub_category"
    case icon
  }

  var name: String?
  var id: Int?
  var subCategory: SubCategory?
  var icon: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    subCategory = try container.decodeIfPresent(SubCategory.self, forKey: .subCategory)
    if let value = try? container.decode(Int.self, forKey:.icon) {                       
icon = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.icon) {
 icon = value                                                                                     
}
  }

}
