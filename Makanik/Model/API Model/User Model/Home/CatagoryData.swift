//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CatagoryData: Codable {

  enum CodingKeys: String, CodingKey {
    case paginate
    case items
  }

  var paginate: Paginate?
  var items: [CatagoryItems]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    paginate = try container.decodeIfPresent(Paginate.self, forKey: .paginate)
    items = try container.decodeIfPresent([CatagoryItems].self, forKey: .items)
  }

}
