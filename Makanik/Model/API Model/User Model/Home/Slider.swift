//
//  Slider.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Slider: Codable {

  enum CodingKeys: String, CodingKey {
    case startAt = "start_at"
    case image
    case id
    case endAt = "end_at"
  }

  var startAt: String?
  var image: String?
  var id: Int?
  var endAt: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.startAt) {                       
startAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.startAt) {
 startAt = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.endAt) {                       
endAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.endAt) {
 endAt = value                                                                                     
}
  }

}
