//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct HomeUserData: Codable {

  enum CodingKeys: String, CodingKey {
    case mostOrders = "most_orders"
    case categories
    case lastAdded = "last_added"
    case slider
  }

  var mostOrders: [MostOrders]?
  var categories: [Categories]?
  var lastAdded: [LastAdded]?
  var slider: [Slider]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    mostOrders = try container.decodeIfPresent([MostOrders].self, forKey: .mostOrders)
    categories = try container.decodeIfPresent([Categories].self, forKey: .categories)
    lastAdded = try container.decodeIfPresent([LastAdded].self, forKey: .lastAdded)
    slider = try container.decodeIfPresent([Slider].self, forKey: .slider)
  }

}
