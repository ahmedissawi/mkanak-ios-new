//
//  Items.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CatagoryItems: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case descriptionValue = "description"
    case name
    case icon
  }

  var id: Int?
  var descriptionValue: String?
  var name: String?
  var icon: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.icon) {                       
icon = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.icon) {
 icon = value                                                                                     
}
  }

}
