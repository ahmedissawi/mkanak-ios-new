//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResponseDeatilesProvider: Codable {
    
    enum CodingKeys: String, CodingKey {
        case success
        case status
        case message
        case data
    }
    
    var success: Bool?
    var status: Int?
    var message: String?
    var data: DeatilesProviderData?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        data = try container.decodeIfPresent(DeatilesProviderData.self, forKey: .data)
    }
    
}
