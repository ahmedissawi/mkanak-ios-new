//
//  User.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserDeatilesProvider: Codable {
    
    enum CodingKeys: String, CodingKey {
        case mobile
        case image
        case id
        case shortCountry = "short_country"
        case email
        case countryCode = "country_code"
        case name
    }
    
    var mobile: String?
    var image: String?
    var id: Int?
    var shortCountry: String?
    var email: String?
    var countryCode: String?
    var name: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortCountry) {
            shortCountry = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortCountry) {
            shortCountry = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.countryCode) {
            countryCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.countryCode) {
            countryCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
    }
    
}
