//
//  Medals.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Medals: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case icon
  }

  var name: String?
  var icon: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.icon) {                       
icon = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.icon) {
 icon = value                                                                                     
}
  }

}
