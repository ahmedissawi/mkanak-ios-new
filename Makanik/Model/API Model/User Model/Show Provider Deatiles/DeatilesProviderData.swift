//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DeatilesProviderData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lng
        case workDays = "work_days"
        case lat
        case name
        case mechanicService = "mechanic_service"
        case mobile
        case shortCountry = "short_country"
        case rating
        case gender
        case email
        case address
        case countryCode = "country_code"
        case ratingAverage = "rating_average"
        case id
        case image
        case medals
        case type
        case licence
        case identificationNumber = "identification_number"
        
    }
    
    var lng: Float?
    var workDays: [WorkDaysDeatilesProvider]?
    var lat: Float?
    var name: String?
    var mechanicService: [MechanicService]?
    var mobile: String?
    var shortCountry: String?
    var rating: [RatingDeatilesProvider]?
    var gender: String?
    var email: String?
    var address: String?
    var countryCode: String?
    var id: Int?
    var image: String?
    var medals: Medals?
    var type: String?
    var ratingAverage: Int?
    var licence: String?
    var identificationNumber: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        lng = try container.decodeIfPresent(Float.self, forKey: .lng)
        workDays = try container.decodeIfPresent([WorkDaysDeatilesProvider].self, forKey: .workDays)
        lat = try container.decodeIfPresent(Float.self, forKey: .lat)
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        medals = try container.decodeIfPresent(Medals.self, forKey: .medals)
        mechanicService = try container.decodeIfPresent([MechanicService].self, forKey: .mechanicService)
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortCountry) {
            shortCountry = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortCountry) {
            shortCountry = value
        }
        rating = try container.decodeIfPresent([RatingDeatilesProvider].self, forKey: .rating)
        if let value = try? container.decode(Int.self, forKey:.gender) {
            gender = String(value)
        }else if let value = try? container.decode(String.self, forKey:.gender) {
            gender = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(Int.self, forKey:.countryCode) {
            countryCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.countryCode) {
            countryCode = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(String.self, forKey:.ratingAverage) {
            ratingAverage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.ratingAverage) {
            ratingAverage = value
        }
        if let value = try? container.decode(Int.self, forKey:.licence) {
            licence = String(value)
        }else if let value = try? container.decode(String.self, forKey:.licence) {
            licence = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationNumber) {
            identificationNumber = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationNumber) {
            identificationNumber = value
        }
        
    }
    
}
