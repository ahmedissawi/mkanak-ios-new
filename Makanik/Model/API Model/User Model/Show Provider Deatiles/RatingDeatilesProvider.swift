//
//  Rating.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct RatingDeatilesProvider: Codable {
    
    enum CodingKeys: String, CodingKey {
        case comment
        case rating
        case id
        case mechanic
        case user
        case createdAt = "created_at"
    }
    
    var comment: String?
    var rating: Int?
    var id: Int?
    var mechanic: Mechanic?
    var user: UserDeatilesProvider?
    var createdAt: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.comment) {
            comment = String(value)
        }else if let value = try? container.decode(String.self, forKey:.comment) {
            comment = value
        }
        if let value = try? container.decode(String.self, forKey:.rating) {
            rating = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.rating) {
            rating = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        mechanic = try container.decodeIfPresent(Mechanic.self, forKey: .mechanic)
        user = try container.decodeIfPresent(UserDeatilesProvider.self, forKey: .user)
    }
    
}
