//
//  Category.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Category: Codable {

  enum CodingKeys: String, CodingKey {
    case icon
    case descriptionValue = "description"
    case name
    case id
  }

  var icon: String?
  var descriptionValue: String?
  var name: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.icon) {                       
icon = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.icon) {
 icon = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
