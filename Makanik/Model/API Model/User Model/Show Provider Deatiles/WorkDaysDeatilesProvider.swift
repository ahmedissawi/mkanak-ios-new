//
//  WorkDays.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct WorkDaysDeatilesProvider: Codable {
    
    enum CodingKeys: String, CodingKey {
        case day
        case id
        case dayName = "day_name"
        case fromTime = "from_time"
        case toTime = "to_time"
    }
    
    var day: Int?
    var id: Int?
    var dayName: String?
    var fromTime: String?
    var toTime: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.day) {
            day = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.day) {
            day = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.dayName) {
            dayName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.dayName) {
            dayName = value
        }
        if let value = try? container.decode(Int.self, forKey:.fromTime) {
            fromTime = String(value)
        }else if let value = try? container.decode(String.self, forKey:.fromTime) {
            fromTime = value
        }
        if let value = try? container.decode(Int.self, forKey:.toTime) {
            toTime = String(value)
        }else if let value = try? container.decode(String.self, forKey:.toTime) {
            toTime = value
        }
    }
    
}
