//
//  Service.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ServiceDeatilesProvider: Codable {
    
    enum CodingKeys: String, CodingKey {
        case subCategoryId = "sub_category_id"
        case createdAt = "created_at"
        case subCategory = "sub_category"
        case icon
        case name
        case id
        case updatedAt = "updated_at"
    }
    
    var subCategoryId: Int?
    var createdAt: String?
    var icon: String?
    var name: String?
    var id: Int?
    var updatedAt: String?
    var subCategory: SubCategory?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.subCategoryId) {
            subCategoryId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.subCategoryId) {
            subCategoryId = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.icon) {
            icon = String(value)
        }else if let value = try? container.decode(String.self, forKey:.icon) {
            icon = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.updatedAt) {
            updatedAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.updatedAt) {
            updatedAt = value
        }
        subCategory = try container.decodeIfPresent(SubCategory.self, forKey: .subCategory)
    }
    
}
