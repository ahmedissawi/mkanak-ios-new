//
//  SubCategory.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SubCategory: Codable {

  enum CodingKeys: String, CodingKey {
    case category
    case name
    case icon
    case descriptionValue = "description"
    case id
  }

  var category: Category?
  var name: String?
  var icon: String?
  var descriptionValue: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    category = try container.decodeIfPresent(Category.self, forKey: .category)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.icon) {                       
icon = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.icon) {
 icon = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.descriptionValue) {                       
descriptionValue = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
 descriptionValue = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
