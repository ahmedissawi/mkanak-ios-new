//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResponseUser: Codable {
    
    enum CodingKeys: String, CodingKey {
        case data
        case message
        case success
        case status
    }
    
    var data: UserData?
    var message: String?
    var success: Bool?
    var status: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decodeIfPresent(UserData.self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        success = try container.decodeIfPresent(Bool.self, forKey: .success)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
    }
    
}
