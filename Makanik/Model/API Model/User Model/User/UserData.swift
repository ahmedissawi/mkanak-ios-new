//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/19/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case timezone
        case mobile
        case address
        case image
        case lng
        case name
        case identificationPhoto = "identification_photo"
        case active
        case countryCode = "country_code"
        case identificationNumber = "identification_number"
        case shortCountry = "short_country"
        case email
        case accessToken = "access_token"
        case id
        case local
        case lat
        case notification
        case gender
    }
    
    var timezone: String?
    var mobile: String?
    var address: String?
    var image: String?
    var lng: Float?
    var name: String?
    var identificationPhoto: String?
    var active: Int?
    var countryCode: String?
    var identificationNumber: String?
    var shortCountry: String?
    var email: String?
    var accessToken: String?
    var id: Int?
    var local: String?
    var lat: Float?
    var notification: Int?
    var gender: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.timezone) {
            timezone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.timezone) {
            timezone = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        lng = try container.decodeIfPresent(Float.self, forKey: .lng)
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationPhoto) {
            identificationPhoto = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationPhoto) {
            identificationPhoto = value
        }
        if let value = try? container.decode(String.self, forKey:.active) {
            active = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.active) {
            active = value
        }
        if let value = try? container.decode(Int.self, forKey:.countryCode) {
            countryCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.countryCode) {
            countryCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.identificationNumber) {
            identificationNumber = String(value)
        }else if let value = try? container.decode(String.self, forKey:.identificationNumber) {
            identificationNumber = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortCountry) {
            shortCountry = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortCountry) {
            shortCountry = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.accessToken) {
            accessToken = String(value)
        }else if let value = try? container.decode(String.self, forKey:.accessToken) {
            accessToken = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.local) {
            local = String(value)
        }else if let value = try? container.decode(String.self, forKey:.local) {
            local = value
        }
        lat = try container.decodeIfPresent(Float.self, forKey: .lat)
        if let value = try? container.decode(String.self, forKey:.notification) {
            notification = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.notification) {
            notification = value
        }
        if let value = try? container.decode(Int.self, forKey:.gender) {
            gender = String(value)
        }else if let value = try? container.decode(String.self, forKey:.gender) {
            gender = value
        }
    }
    
}
