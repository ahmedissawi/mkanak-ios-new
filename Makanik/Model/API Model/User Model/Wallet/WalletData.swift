//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct WalletData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case items
        case paginate
        case availableBalance = "available_balance"
        case outstandingBalance = "outstanding_balance"
    }
    
    var items: [WalletItems]?
    var paginate: Paginate?
    var availableBalance: Int?
    var outstandingBalance: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        items = try container.decodeIfPresent([WalletItems].self, forKey: .items)
        paginate = try container.decodeIfPresent(Paginate.self, forKey: .paginate)
        if let value = try? container.decode(String.self, forKey:.availableBalance) {
            availableBalance = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.availableBalance) {
            availableBalance = value
        }
        if let value = try? container.decode(String.self, forKey:.outstandingBalance) {
            outstandingBalance = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.outstandingBalance) {
            outstandingBalance = value
        }
    }
    
}
