//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 1/25/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct subCategoryData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case name
        case subCategory = "sub_category"
        case id
        case icon
    }
    
    var name: String?
    var subCategory: SubCategory?
    var id: Int?
    var icon: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        subCategory = try container.decodeIfPresent(SubCategory.self, forKey: .subCategory)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.icon) {
            icon = String(value)
        }else if let value = try? container.decode(String.self, forKey:.icon) {
            icon = value
        }
    }
    
}
