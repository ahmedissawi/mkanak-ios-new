//
//  AppDelegate.swift
//  Makanik
//
//  Created by  Ahmed’s MacBook Pro on 1/14/21.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseCore
import GoogleMaps
import GooglePlaces

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let sb_main = UIStoryboard.init(name: "Main", bundle: nil)
    
    static var fcm:FCM!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Helper.EnableKeyboardManager(TintColor: "FFFFFF".color, BarTintColor: "2476B8".color, placeholderColor: "FFFFFF".color)

       // MOLHLanguage.setDefaultLanguage("ar")
        MOLH.shared.activate(true)
        
        FirebaseApp.configure()

        //AIzaSyAlRxMCvdl5ipOLuyTiof8c6B-Zvg11eHo
        GMSServices.provideAPIKey("AIzaSyAgLfq54h5DuzBwBYYg0V5QkVXq4m36yYQ")
        GMSPlacesClient.provideAPIKey("AIzaSyAgLfq54h5DuzBwBYYg0V5QkVXq4m36yYQ")
        
        AppDelegate.fcm = FCM(withApplication: application)
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

